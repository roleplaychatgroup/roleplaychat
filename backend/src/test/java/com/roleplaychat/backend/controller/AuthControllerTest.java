package com.roleplaychat.backend.controller;

import com.roleplaychat.backend.auth.payload.request.EditRequest;
import com.roleplaychat.backend.auth.payload.request.LoginRequest;
import com.roleplaychat.backend.auth.payload.request.SignupRequest;
import com.roleplaychat.backend.auth.payload.response.JwtResponse;
import com.roleplaychat.backend.auth.payload.response.UserInfoResponse;
import com.roleplaychat.backend.services.AuthService;
import com.roleplaychat.backend.services.UserService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;

@ExtendWith(MockitoExtension.class)
class AuthControllerTest {
    @Mock private SignupRequest signUpRequest;
    @Mock private LoginRequest loginRequest;
    @Mock private EditRequest editRequest;
    @Mock private JwtResponse jwtResponse;
    @Mock private UserInfoResponse userInfoResponse;

    @Mock private UserService userService;
    @Mock private AuthService authService;
    @Mock private Authentication authentication;

    private AuthController controller;

    @BeforeEach
    private void initAuthController() {
        controller = new AuthController(userService, authService);
    }

    @Test
    @DisplayName("AuthControllerTest:authenticateUser()")
    void authenticateUser() {
        Mockito.when(authService.createJwtResponse(Mockito.any()))
                .thenReturn(jwtResponse);

        Assertions.assertEquals(HttpStatus.OK, controller.authenticateUser(loginRequest).getStatusCode());

        Mockito.verify(authService, Mockito.times(1))
                .createJwtResponse(Mockito.any());
    }

    @Test
    @DisplayName("AuthControllerTest:editUserInfo()")
    void editUserInfo() {
        ResponseEntity expectedResponse = new ResponseEntity<>("", HttpStatus.OK);
        Mockito.when(authService.editUserInfo(authentication, editRequest))
                .thenReturn(expectedResponse);

        ResponseEntity<?> response = controller.editUserInfo(authentication, editRequest);

        Mockito.verify(authService, Mockito.times(1))
                .editUserInfo(authentication, editRequest);
        Assertions.assertEquals(expectedResponse, response);
        Assertions.assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    @DisplayName("AuthControllerTest:getUserInfo()")
    void getUserInfo() {
        long userId = 1;

        ResponseEntity expectedResponse = new ResponseEntity<>(userInfoResponse, HttpStatus.OK);
        Mockito.when(authService.getUserInfo(userId))
                .thenReturn(expectedResponse);

        ResponseEntity<?> response = controller.getUserInfo(userId);

        Mockito.verify(authService, Mockito.times(1))
                .getUserInfo(userId);
        Assertions.assertEquals(expectedResponse, response);
    }

    @Test
    @DisplayName("AuthControllerTest:registerUserReturnsBadRequest()")
    void registerUserReturnsBadRequest() {
        Mockito.when(userService.existsByEmail(Mockito.any()))
                .thenReturn(true);

        Assertions.assertEquals(HttpStatus.BAD_REQUEST, controller.registerUser(signUpRequest).getStatusCode());

        Mockito.verify(signUpRequest, Mockito.times(1))
                .getEmail();
        Mockito.verify(userService, Mockito.times(1))
                .existsByEmail(Mockito.any());
    }

    @Test
    @DisplayName("AuthControllerTest:registerUserReturnsOk()")
    void registerUserReturnsOk() {
        Mockito.when(userService.existsByEmail(Mockito.any()))
                .thenReturn(false);

        Assertions.assertEquals(HttpStatus.OK, controller.registerUser(signUpRequest).getStatusCode());

        Mockito.verify(signUpRequest, Mockito.times(1))
                .getEmail();
        Mockito.verify(userService, Mockito.times(1))
                .existsByEmail(Mockito.any());
    }
}