package com.roleplaychat.backend.controller;

import com.roleplaychat.backend.request.CharacterCreationRequest;
import com.roleplaychat.backend.response.CharacterResponse;
import com.roleplaychat.backend.response.UserCharacterResponse;
import com.roleplaychat.backend.services.CharacterService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.security.Principal;

@ExtendWith(MockitoExtension.class)
class CharacterControllerTest {
    private final long characterId = 1L;

    @Mock private Principal principal;
    @Mock private CharacterService characterService;
    @Mock private CharacterCreationRequest characterCreationRequest;
    @Mock private CharacterResponse characterResponse;
    @Mock private UserCharacterResponse userCharacterResponse;

    private CharacterController controller;

    @BeforeEach
    void initCharacterController() {
        controller = new CharacterController(characterService);
    }

    @Test
    @DisplayName("CharacterControllerTest:addNewCharacter()")
    void addNewCharacter() {
        ResponseEntity expectedResponse = new ResponseEntity<>(characterResponse, HttpStatus.OK);

        Mockito.when(characterService.createCharacter(principal, characterCreationRequest))
                .thenReturn(expectedResponse);

        ResponseEntity<?> response = controller.addNewCharacter(principal, characterCreationRequest);

        Mockito.verify(characterService, Mockito.times(1))
                .createCharacter(principal, characterCreationRequest);
        Assertions.assertEquals(expectedResponse, response);
    }

    @Test
    @DisplayName("CharacterControllerTest:deleteCharacter()")
    void deleteCharacter() {
        ResponseEntity expectedResponse = new ResponseEntity<>(HttpStatus.OK);

        Mockito.when(characterService.deleteCharacter(characterId))
                .thenReturn(expectedResponse);

        ResponseEntity<?> response = controller.deleteCharacter(characterId);

        Mockito.verify(characterService, Mockito.times(1))
                .deleteCharacter(characterId);
        Assertions.assertEquals(expectedResponse, response);
    }

    @Test
    @DisplayName("CharacterControllerTest:addCharacterLife()")
    void addCharacterLife() {
        ResponseEntity expectedResponse = new ResponseEntity<>("Success!", HttpStatus.OK);

        Mockito.when(characterService.addLife(characterId))
                .thenReturn(expectedResponse);

        ResponseEntity<?> response = controller.addCharacterLife(characterId);

        Mockito.verify(characterService, Mockito.times(1))
                .addLife(characterId);
        Assertions.assertEquals(expectedResponse, response);
    }

    @Test
    @DisplayName("CharacterControllerTest:deleteCharacterLife()")
    void deleteCharacterLife() {
        ResponseEntity expectedResponse = new ResponseEntity<>("Success!", HttpStatus.OK);

        Mockito.when(characterService.deleteLife(characterId))
                .thenReturn(expectedResponse);

        ResponseEntity<?> response = controller.deleteCharacterLife(characterId);

        Mockito.verify(characterService, Mockito.times(1))
                .deleteLife(characterId);
        Assertions.assertEquals(expectedResponse, response);
    }

    @Test
    @DisplayName("CharacterControllerTest:getListCharacterByEmail()")
    void getListCharacterByEmail() {
        String email = "email@email.com";
        ResponseEntity expectedResponse = new ResponseEntity<>(userCharacterResponse, HttpStatus.OK);

        Mockito.when(characterService.getListCharactersByEmail(email))
                .thenReturn(expectedResponse);

        ResponseEntity<?> response = controller.getListCharacterByEmail(email);

        Mockito.verify(characterService, Mockito.times(1))
                .getListCharactersByEmail(email);
        Assertions.assertEquals(expectedResponse, response);
    }


    @Test
    @DisplayName("CharacterControllerTest:getListCharacter()")
    void getListCharacter() {
        ResponseEntity expectedResponse = new ResponseEntity<>(characterResponse, HttpStatus.OK);

        Mockito.when(characterService.getListCharacters(principal))
                .thenReturn(expectedResponse);

        ResponseEntity<?> response = controller.getListCharacter(principal);

        Mockito.verify(characterService, Mockito.times(1))
                .getListCharacters(principal);
        Assertions.assertEquals(expectedResponse, response);
    }
}