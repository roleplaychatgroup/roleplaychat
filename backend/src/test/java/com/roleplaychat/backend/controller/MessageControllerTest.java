package com.roleplaychat.backend.controller;

import com.roleplaychat.backend.request.SendMessageRequest;
import com.roleplaychat.backend.response.MessageInfoResponse;
import com.roleplaychat.backend.services.MessageService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;

@ExtendWith(MockitoExtension.class)
class MessageControllerTest {
    @Mock private MessageService messageService;
    @Mock private SendMessageRequest sendMessageRequest;
    private final long groupId = 1L;
    private MessageController controller;

    @BeforeEach
    void initCharacterController() {
        controller = new MessageController(messageService);
    }

    @Test
    @DisplayName("MessageControllerTest:getAllMessages()")
    void getAllMessages() {
        List<MessageInfoResponse> messagesResponses = new ArrayList<>();
        ResponseEntity expectedResponse = new ResponseEntity<>(messagesResponses, HttpStatus.OK);

        Mockito.when(messageService.getAllMessagesByGroupId(groupId))
                .thenReturn(expectedResponse);

        ResponseEntity<?> response = controller.getAllMessages(groupId);

        Mockito.verify(messageService, Mockito.times(1))
                .getAllMessagesByGroupId(groupId);
        Assertions.assertEquals(expectedResponse, response);
    }

    @Test
    @DisplayName("MessageControllerTest:sendMessage()")
    void sendMessage() {
        ResponseEntity expectedResponse = new ResponseEntity<>(HttpStatus.OK);

        Mockito.when(messageService.addNewMessage(sendMessageRequest, groupId))
                .thenReturn(expectedResponse);

        ResponseEntity<?> response = controller.sendMessage(sendMessageRequest, groupId);

        Mockito.verify(messageService, Mockito.times(1))
                .addNewMessage(sendMessageRequest, groupId);
        Assertions.assertEquals(expectedResponse, response);
    }
}