package com.roleplaychat.backend.controller;

import com.roleplaychat.backend.request.AddOrEditGroupRequest;
import com.roleplaychat.backend.response.GroupFullInfoResponse;
import com.roleplaychat.backend.response.GroupInfoResponse;
import com.roleplaychat.backend.services.GroupService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;

@ExtendWith(MockitoExtension.class)
class GroupControllerTest {
    private final Long userId = 1L;
    private final Long groupId = 1L;
    @Mock private AddOrEditGroupRequest addRequest;
    @Mock private GroupService groupService;

    private GroupController groupController;

    @BeforeEach
    void initCharacterController() {
        groupController = new GroupController(groupService);
    }

    @Test
    @DisplayName("GroupControllerTest:getAllGroups()")
    void getAllGroups() {
        List<GroupInfoResponse> groupInfoResponses = new ArrayList<>();
        ResponseEntity expectedResponse = new ResponseEntity<>(groupInfoResponses, HttpStatus.OK);

        Mockito.when(groupService.getAllGroups(userId)).thenReturn(expectedResponse);

        ResponseEntity<?> response = groupController.getAllGroups(userId);

        Mockito.verify(groupService, Mockito.times(1))
                .getAllGroups(userId);
        Assertions.assertEquals(expectedResponse, response);
        Assertions.assertEquals(expectedResponse.getStatusCode(), response.getStatusCode());
    }

    @Test
    @DisplayName("GroupControllerTest:getGroupInfo()")
    void getGroupInfo() {
        GroupFullInfoResponse groupFullInfoResponse = new GroupFullInfoResponse(
                groupId, "testGroupName", new ArrayList<>(), userId
        );
        ResponseEntity expectedResponse = new ResponseEntity<>(groupFullInfoResponse, HttpStatus.OK);

        Mockito.when(groupService.getGroupInfo(groupId)).thenReturn(expectedResponse);

        ResponseEntity<?> response = groupController.getGroupInfo(groupId);

        Mockito.verify(groupService, Mockito.times(1))
                .getGroupInfo(groupId);
        Assertions.assertEquals(expectedResponse, response);
        Assertions.assertEquals(expectedResponse.getStatusCode(), response.getStatusCode());
    }

    @Test
    @DisplayName("GroupControllerTest:addNewGroup()")
    void addNewGroup() {
        GroupInfoResponse groupInfoResponse = new GroupInfoResponse(groupId, "testGroupName");
        ResponseEntity expectedResponse = new ResponseEntity<>(groupInfoResponse, HttpStatus.OK);

        Mockito.when(groupService.addNewGroup(addRequest)).thenReturn(expectedResponse);

        ResponseEntity<?> response = groupController.addNewGroup(addRequest);

        Mockito.verify(groupService, Mockito.times(1))
                .addNewGroup(addRequest);
        Assertions.assertEquals(expectedResponse, response);
        Assertions.assertEquals(expectedResponse.getStatusCode(), response.getStatusCode());
    }

    @Test
    @DisplayName("GroupControllerTest:editGroup()")
    void editGroup() {
        ResponseEntity expectedResponse = new ResponseEntity<>(HttpStatus.OK);
        Mockito.when(groupService.editGroup(groupId, addRequest)).thenReturn(expectedResponse);

        ResponseEntity<?> response = groupController.editGroup(groupId, addRequest);

        Mockito.verify(groupService, Mockito.times(1))
                .editGroup(groupId, addRequest);
        Assertions.assertEquals(expectedResponse, response);
        Assertions.assertEquals(expectedResponse.getStatusCode(), response.getStatusCode());
    }
}