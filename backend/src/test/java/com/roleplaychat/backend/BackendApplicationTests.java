package com.roleplaychat.backend;

import lombok.Generated;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class BackendApplicationTests {

    @Test
    @Generated
    @DisplayName("BackendApplicationTests:runApplication()")
    void runApplication() {
        BackendApplication.main(new String[]{});
    }
}
