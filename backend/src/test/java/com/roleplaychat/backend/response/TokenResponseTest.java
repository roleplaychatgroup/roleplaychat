package com.roleplaychat.backend.response;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class TokenResponseTest {
    private final String token = "sDdv65FddDS545dABCss54Y6sd12PO3z54";

    @Test
    @DisplayName("TokenResponseTest:getToken()")
    void getToken() {
        TokenResponse response = new TokenResponse(token);

        Assertions.assertNotNull(response);
        Assertions.assertNotNull(response.getToken());
        Assertions.assertEquals(token, response.getToken());
    }

    @Test
    @DisplayName("TokenResponseTest:setToken()")
    void setToken() {
        TokenResponse response = new TokenResponse(token);
        Assertions.assertEquals(token, response.getToken());

        String newToken = "sD8dv66uk5OASOA64Dss01s54Y6sd12PO3z54";
        response.setToken(newToken);
        Assertions.assertNotNull(response.getToken());
        Assertions.assertEquals(newToken, response.getToken());
    }
}