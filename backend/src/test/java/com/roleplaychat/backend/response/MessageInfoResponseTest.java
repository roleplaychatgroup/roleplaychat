package com.roleplaychat.backend.response;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Date;

class MessageInfoResponseTest {
    private final String text = "messageText";
    private final Date sendTime = new Date();
    private final Long userId = 1L;
    private final String name = "senderName";
    private final String gender = "male";

    private final Long characterId = 1L;
    private final String characterName = "characterName";
    private final String characterGender = "male";

    private MessageInfoResponse response;

    @BeforeEach
    private void initMessageInfoResponse() {
        response = new MessageInfoResponse(
                text, sendTime, userId, name, gender,
                characterId, characterName, characterGender
        );
    }

    @Test
    @DisplayName("MessageInfoResponseTest:allArgsConstructor()")
    void allArgsConstructor() {
        response = new MessageInfoResponse(
                text, sendTime, userId, name, gender,
                characterId, characterName, characterGender
        );

        Assertions.assertEquals(text, response.getText());
        Assertions.assertEquals(sendTime, response.getSendTime());
        Assertions.assertEquals(userId, response.getUserId());
        Assertions.assertEquals(name, response.getName());
        Assertions.assertEquals(gender, response.getGender());
        Assertions.assertEquals(characterId, response.getCharacterId());
        Assertions.assertEquals(characterName, response.getCharacterName());
        Assertions.assertEquals(characterGender, response.getCharacterGender());
    }

    @Test
    @DisplayName("MessageInfoResponseTest:getText()")
    void getText() {
        Assertions.assertEquals(text, response.getText());
    }

    @Test
    @DisplayName("MessageInfoResponseTest:getSendTime()")
    void getSendTime() {
        Assertions.assertEquals(sendTime, response.getSendTime());
    }

    @Test
    @DisplayName("MessageInfoResponseTest:getUserId()")
    void getUserId() {
        Assertions.assertEquals(userId, response.getUserId());
    }

    @Test
    @DisplayName("MessageInfoResponseTest:getName()")
    void getName() {
        Assertions.assertEquals(name, response.getName());
    }

    @Test
    @DisplayName("MessageInfoResponseTest:getGender()")
    void getGender() {
        Assertions.assertEquals(gender, response.getGender());
    }

    @Test
    @DisplayName("MessageInfoResponseTest:getCharacterId()")
    void getCharacterId() {
        Assertions.assertEquals(characterId, response.getCharacterId());
    }

    @Test
    @DisplayName("MessageInfoResponseTest:getCharacterName()")
    void getCharacterName() {
        Assertions.assertEquals(characterName, response.getCharacterName());
    }

    @Test
    @DisplayName("MessageInfoResponseTest:getCharacterGender()")
    void getCharacterGender() {
        Assertions.assertEquals(characterGender, response.getCharacterGender());
    }

    @Test
    @DisplayName("MessageInfoResponseTest:setText()")
    void setText() {
        String newText = "newMessageText";
        response.setText(newText);
        Assertions.assertEquals(newText, response.getText());
    }

    @Test
    @DisplayName("MessageInfoResponseTest:setSendTime()")
    void setSendTime() {
        Date newDate = new Date();
        response.setSendTime(newDate);
        Assertions.assertEquals(newDate, response.getSendTime());
    }

    @Test
    @DisplayName("MessageInfoResponseTest:setUserId()")
    void setUserId() {
        Long newUserId = 2L;
        response.setUserId(newUserId);
        Assertions.assertEquals(newUserId, response.getUserId());
    }

    @Test
    @DisplayName("MessageInfoResponseTest:setName()")
    void setName() {
        String newName = "newName";
        response.setName(newName);
        Assertions.assertEquals(newName, response.getName());
    }

    @Test
    @DisplayName("MessageInfoResponseTest:setGender()")
    void setGender() {
        String newGender = "female";
        response.setGender(newGender);
        Assertions.assertEquals(newGender, response.getGender());
    }

    @Test
    @DisplayName("MessageInfoResponseTest:setCharacterId()")
    void setCharacterId() {
        Assertions.assertEquals(characterId, response.getCharacterId());
        Long newCharacterId = 2L;
        response.setCharacterId(newCharacterId);
        Assertions.assertEquals(newCharacterId, response.getCharacterId());
    }

    @Test
    @DisplayName("MessageInfoResponseTest:setCharacterName()")
    void setCharacterName() {
        Assertions.assertEquals(characterName, response.getCharacterName());
        String newCharacterName = "newCharacterName";
        response.setCharacterName(newCharacterName);
        Assertions.assertEquals(newCharacterName, response.getCharacterName());
    }

    @Test
    @DisplayName("MessageInfoResponseTest:setCharacterGender()")
    void setCharacterGender() {
        Assertions.assertEquals(characterGender, response.getCharacterGender());
        String newCharacterGender = "female";
        response.setCharacterGender(newCharacterGender);
        Assertions.assertEquals(newCharacterGender, response.getCharacterGender());
    }
}