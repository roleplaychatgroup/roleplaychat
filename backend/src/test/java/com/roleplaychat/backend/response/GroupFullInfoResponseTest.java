package com.roleplaychat.backend.response;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

class GroupFullInfoResponseTest {
    private final Long groupId = 1L;
    private final String groupName = "groupName";
    private final List<UserInfo> userList = new ArrayList<>();
    private final Long adminId = 1L;

    private GroupFullInfoResponse response;

    @BeforeEach
    private void initGroupFullInfoResponse() {
        response = new GroupFullInfoResponse(
                groupId,
                groupName,
                userList,
                adminId);
    }

    @Test
    @DisplayName("GroupFullInfoResponseTest:getGroupId()")
    void getGroupId() {
        Assertions.assertNotNull(response.getGroupId());
        Assertions.assertEquals(groupId, response.getGroupId());
    }

    @Test
    @DisplayName("GroupFullInfoResponseTest:getGroupName()")
    void getGroupName() {
        Assertions.assertNotNull(response.getGroupName());
        Assertions.assertEquals(groupName, response.getGroupName());
    }

    @Test
    @DisplayName("GroupFullInfoResponseTest:getUserList()")
    void getUserList() {
        Assertions.assertNotNull(response.getUserList());
        Assertions.assertEquals(userList, response.getUserList());
    }

    @Test
    @DisplayName("GroupFullInfoResponseTest:getAdminId()")
    void getAdminId() {
        Assertions.assertNotNull(response.getAdminId());
        Assertions.assertEquals(adminId, response.getAdminId());
    }

    @Test
    @DisplayName("GroupFullInfoResponseTest:setGroupId()")
    void setGroupId() {
        Assertions.assertEquals(groupId, response.getGroupId());
        Long newGroupId = 2L;
        response.setGroupId(newGroupId);
        Assertions.assertEquals(newGroupId, response.getGroupId());
    }

    @Test
    @DisplayName("GroupFullInfoResponseTest:setGroupName()")
    void setGroupName() {
        Assertions.assertEquals(groupName, response.getGroupName());
        String newGroupName = "newGroupName";
        response.setGroupName(newGroupName);
        Assertions.assertEquals(newGroupName, response.getGroupName());
    }

    @Test
    @DisplayName("GroupFullInfoResponseTest:setUserList()")
    void setUserList() {
        Assertions.assertEquals(userList, response.getUserList());
        List<UserInfo> newUserList = new ArrayList<>();
        response.setUserList(newUserList);
        Assertions.assertEquals(newUserList, response.getUserList());
    }

    @Test
    @DisplayName("GroupFullInfoResponseTest:setAdminId()")
    void setAdminId() {
        Assertions.assertEquals(adminId, response.getAdminId());
        Long newAdminId = 2L;
        response.setAdminId(newAdminId);
        Assertions.assertEquals(newAdminId, response.getAdminId());
    }
}
