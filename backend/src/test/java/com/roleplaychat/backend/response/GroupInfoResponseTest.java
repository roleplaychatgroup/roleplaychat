package com.roleplaychat.backend.response;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class GroupInfoResponseTest {
    private final Long groupId = 1L;
    private final String groupName = "groupName";
    GroupInfoResponse response;

    @BeforeEach
    private void initGroupInfoResponse() {
        response = new GroupInfoResponse(
                groupId, groupName
        );
    }

    @Test
    @DisplayName("GroupInfoResponseTest:getGroupId()")
    void getGroupId() {
        Assertions.assertEquals(groupId, response.getGroupId());
    }

    @Test
    @DisplayName("GroupInfoResponseTest:getGroupName()")
    void getGroupName() {
        Assertions.assertEquals(groupName, response.getGroupName());
    }

    @Test
    @DisplayName("GroupInfoResponseTest:setGroupId()")
    void setGroupId() {
        Long newGroupId = 2L;
        response.setGroupId(newGroupId);
        Assertions.assertEquals(newGroupId, response.getGroupId());
    }

    @Test
    @DisplayName("GroupInfoResponseTest:setGroupName()")
    void setGroupName() {
        String newGroupName = "newGroupName";
        response.setGroupName(newGroupName);
        Assertions.assertEquals(newGroupName, response.getGroupName());
    }
}