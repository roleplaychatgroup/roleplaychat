package com.roleplaychat.backend.response;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class CharacterResponseTest {
    private final Long characterId = 1L;
    private final String characterName = "characterName";
    private final String characterGender = "male";
    private final String note = "characterNote";
    private final long lifeCount = 10;

    private CharacterResponse response;

    @BeforeEach
    private void initCharacterResponse() {
        response = new CharacterResponse(
                characterId, characterName, characterGender, note, lifeCount
        );
    }

    @Test
    @DisplayName("CharacterResponseTest:allArgsConstructor()")
    void allArgsConstructor() {
        response = new CharacterResponse(
                characterId, characterName, characterGender, note, lifeCount
        );

        Assertions.assertNotNull(response);
        Assertions.assertEquals(characterId, response.getCharacterId());
        Assertions.assertEquals(characterName, response.getCharacterName());
        Assertions.assertEquals(characterGender, response.getCharacterGender());
        Assertions.assertEquals(note, response.getNote());
        Assertions.assertEquals(lifeCount, response.getLifeCount());
    }

    @Test
    @DisplayName("CharacterResponseTest:getCharacterId()")
    void getCharacterId() {
        Assertions.assertEquals(characterId, response.getCharacterId());
    }

    @Test
    @DisplayName("CharacterResponseTest:getCharacterName()")
    void getCharacterName() {
        Assertions.assertEquals(characterName, response.getCharacterName());
    }

    @Test
    @DisplayName("CharacterResponseTest:getCharacterGender()")
    void getCharacterGender() {
        Assertions.assertEquals(characterGender, response.getCharacterGender());
    }

    @Test
    @DisplayName("CharacterResponseTest:getNote()")
    void getNote() {
        Assertions.assertEquals(note, response.getNote());
    }

    @Test
    @DisplayName("CharacterResponseTest:getLifeCount()")
    void getLifeCount() {
        Assertions.assertEquals(lifeCount, response.getLifeCount());
    }

    @Test
    @DisplayName("CharacterResponseTest:setCharacterId()")
    void setCharacterId() {
        Assertions.assertEquals(characterId, response.getCharacterId());
        Long newCharacterId = 2L;
        response.setCharacterId(newCharacterId);
        Assertions.assertEquals(newCharacterId, response.getCharacterId());
    }

    @Test
    @DisplayName("CharacterResponseTest:setCharacterName()")
    void setCharacterName() {
        Assertions.assertEquals(characterName, response.getCharacterName());
        String newCharacterName = "newCharacterName";
        response.setCharacterName(newCharacterName);
        Assertions.assertEquals(newCharacterName, response.getCharacterName());
    }

    @Test
    @DisplayName("CharacterResponseTest:setCharacterGender()")
    void setCharacterGender() {
        Assertions.assertEquals(characterGender, response.getCharacterGender());
        String newCharacterGender = "female";
        response.setCharacterGender(newCharacterGender);
        Assertions.assertEquals(newCharacterGender, response.getCharacterGender());
    }

    @Test
    @DisplayName("CharacterResponseTest:setNote()")
    void setNote() {
        Assertions.assertEquals(note, response.getNote());
        String newNote = "newCharacterNote";
        response.setNote(newNote);
        Assertions.assertEquals(newNote, response.getNote());
    }

    @Test
    @DisplayName("CharacterResponseTest:setLifeCount()")
    void setLifeCount() {
        Assertions.assertEquals(lifeCount, response.getLifeCount());
        long newLifeCount = 11;
        response.setLifeCount(newLifeCount);
        Assertions.assertEquals(newLifeCount, response.getLifeCount());
    }
}