package com.roleplaychat.backend.response;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.Set;

class UserInfoTest {
    private final String email = "email@email.com";
    private final Set<CharacterResponse> characters = new HashSet<>();

    private final String newEmail = "new_email@email.com";
    private final Set<CharacterResponse> newCharacters = new HashSet<>();

    private UserInfo userInfo;

    @BeforeEach
    private void initUserInfo() {
        userInfo = new UserInfo(email, characters);
    }

    @Test
    @DisplayName("UserInfoTest:getName()")
    void getUserEmail() {
        Assertions.assertEquals(email, userInfo.getUserEmail());
    }

    @Test
    @DisplayName("UserInfoTest:getCharacters()")
    void getCharacters() {
        Assertions.assertEquals(characters, userInfo.getCharacters());
    }

    @Test
    @DisplayName("UserInfoTest:setUserEmail()")
    void setUserEmail() {
        Assertions.assertEquals(email, userInfo.getUserEmail());
        userInfo.setUserEmail(newEmail);
        Assertions.assertEquals(newEmail, userInfo.getUserEmail());
    }

    @Test
    @DisplayName("UserInfoTest:setCharacters()")
    void setCharacters() {
        Assertions.assertEquals(characters, userInfo.getCharacters());
        userInfo.setCharacters(newCharacters);
        Assertions.assertEquals(newCharacters, userInfo.getCharacters());
    }
}