package com.roleplaychat.backend.response;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.Set;

class UserCharacterResponseTest {
    private final String username = "username";
    private final Set<CharacterResponse> characters = new HashSet<>();
    private UserCharacterResponse response;

    @BeforeEach
    private void initUserCharacterResponse() {
        response = new UserCharacterResponse(username, characters);
    }

    @Test
    @DisplayName("UserCharacterResponseTest:allArgsConstructor()")
    void allArgsConstructor() {
        response = new UserCharacterResponse(username, characters);

        Assertions.assertNotNull(response);
        Assertions.assertEquals(username, response.getUsername());
        Assertions.assertEquals(characters, response.getCharacters());
    }

    @Test
    @DisplayName("UserCharacterResponseTest:getUsername()")
    void getUsername() {
        Assertions.assertEquals(username, response.getUsername());
    }

    @Test
    @DisplayName("UserCharacterResponseTest:getCharacters()")
    void getCharacters() {
        Assertions.assertEquals(characters, response.getCharacters());
    }

    @Test
    @DisplayName("UserCharacterResponseTest:setUsername()")
    void setUsername() {
        Assertions.assertEquals(username, response.getUsername());
        String newUsername = "newUsername";
        response.setUsername(newUsername);
        Assertions.assertEquals(newUsername, response.getUsername());
    }

    @Test
    @DisplayName("UserCharacterResponseTest:setCharacters()")
    void setCharacters() {
        Assertions.assertEquals(characters, response.getCharacters());
        Set<CharacterResponse> newCharacters = new HashSet<>();
        response.setCharacters(newCharacters);
        Assertions.assertEquals(newCharacters, response.getCharacters());
    }
}