package com.roleplaychat.backend.model;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Objects;

class GroupUserKeyTest {
    private final long userId  = 1L;
    private final long groupId = 2L;

    @Test
    @DisplayName("GroupUserKeyTest:allArgsConstructor()")
    void allArgsConstructor() {
        GroupUserKey key = new GroupUserKey(userId, groupId);
        Assertions.assertEquals(1L, key.getUserId());
        Assertions.assertEquals(2L, key.getGroupId());
    }

    @Test
    @DisplayName("GroupUserKeyTest:noArgsConstructor()")
    void noArgsConstructor() {
        GroupUserKey key = new GroupUserKey();
        Assertions.assertEquals(0, key.getUserId());
        Assertions.assertEquals(0, key.getGroupId());
    }

    @Test
    @DisplayName("GroupUserKeyTest:testEqualsTrue()")
    void testEqualsTrue() {
        GroupUserKey key1 = new GroupUserKey(userId, groupId);
        GroupUserKey key2 = new GroupUserKey(userId, groupId);

        Assertions.assertEquals(key1, key2);
        Assertions.assertEquals(key2, key1);
    }

    @Test
    @DisplayName("GroupUserKeyTest:testEqualsFalse()")
    void testEqualsFalse() {
        GroupUserKey key1 = new GroupUserKey(userId, groupId);
        GroupUserKey key2 = new GroupUserKey(userId + 1, groupId + 1);

        Assertions.assertNotEquals(key1, key2);
        Assertions.assertNotEquals(key2, key1);
    }

    @Test
    @DisplayName("GroupUserKeyTest:testEqualsDifferentUserId()")
    void testEqualsDifferentUserId() {
        GroupUserKey key1 = new GroupUserKey(userId, groupId);
        GroupUserKey key2 = new GroupUserKey(userId + 1, groupId);

        Assertions.assertNotEquals(key1, key2);
    }

    @Test
    @DisplayName("GroupUserKeyTest:testEqualsDifferentGroupId()")
    void testEqualsDifferentGroupId() {
        GroupUserKey key1 = new GroupUserKey(userId, groupId);
        GroupUserKey key2 = new GroupUserKey(userId, groupId + 1);

        Assertions.assertNotEquals(key1, key2);
    }

    @Test
    @DisplayName("GroupUserKeyTest:testEqualsSame()")
    void testEqualsSame() {
        GroupUserKey key1 = new GroupUserKey(userId, groupId);
        Assertions.assertEquals(key1, key1);
    }

    @Test
    @DisplayName("GroupUserKeyTest:testEqualsNull()")
    void testEqualsNull() {
        GroupUserKey key1 = new GroupUserKey(userId, groupId);
        GroupUserKey key2 = null;
        Assertions.assertFalse(key1.equals(key2));
    }

    @Test
    @DisplayName("GroupUserKeyTest:testEqualsOtherClass()")
    void testEqualsOtherClass() {
        GroupUserKey key1 = new GroupUserKey(userId, groupId);
        String key2 = "";
        Assertions.assertFalse(key1.equals(key2));
    }

    @Test
    @DisplayName("GroupUserKeyTest:testHashCode()")
    void testHashCode() {
        int hash = Objects.hash(0, 0);
        GroupUserKey key = new GroupUserKey();
        Assertions.assertEquals(hash, key.hashCode());

        hash = Objects.hash(userId, groupId);
        key = new GroupUserKey(userId, groupId);
        Assertions.assertEquals(hash, key.hashCode());
    }

    @Test
    @DisplayName("GroupUserKeyTest:getUserId()")
    void getUserId() {
        GroupUserKey key = new GroupUserKey(userId, groupId);
        Assertions.assertEquals(userId, key.getUserId());
    }

    @Test
    @DisplayName("GroupUserKeyTest:getGroupId()")
    void getGroupId() {
        GroupUserKey key = new GroupUserKey(userId, groupId);
        Assertions.assertEquals(groupId, key.getGroupId());
    }

    @Test
    @DisplayName("GroupUserKeyTest:setUserId()")
    void setUserId() {
        GroupUserKey key = new GroupUserKey(userId, groupId);
        Assertions.assertEquals(userId, key.getUserId());

        int newUserId = 0;
        key.setUserId(newUserId);
        Assertions.assertEquals(newUserId, key.getUserId());
    }

    @Test
    @DisplayName("GroupUserKeyTest:setGroupId()")
    void setGroupId() {
        GroupUserKey key = new GroupUserKey(userId, groupId);
        Assertions.assertEquals(groupId, key.getGroupId());

        int newGroupId = 0;
        key.setGroupId(newGroupId);
        Assertions.assertEquals(newGroupId, key.getGroupId());
    }
}