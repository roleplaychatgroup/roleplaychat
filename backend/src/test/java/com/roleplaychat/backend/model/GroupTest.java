package com.roleplaychat.backend.model;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class GroupTest {
    private String groupName = "groupName";
    private Group group;

    @BeforeEach
    private void initGroup() {
        group = new Group(
                groupName
        );
    }

    @Test
    @DisplayName("GroupTest:builder()")
    void builder() {
        Group builtGroup = Group.builder()
                .groupName(groupName).build();

        Group group = new Group();
        group.setGroupName(groupName);

        Assertions.assertEquals(group.getGroupName(), builtGroup.getGroupName());
    }

    @Test
    @DisplayName("GroupTest:builderToString()")
    void builderToString() {
        Group.GroupBuilder builder = Group.builder()
                .groupName(groupName);

        Assertions.assertEquals("Group.GroupBuilder(groupName=" + groupName + ")",
                builder.toString());
    }

    @Test
    @DisplayName("GroupTest:getGroupId()")
    void getGroupId() {
        // До добавления в базу данных значение должно быть null
        Assertions.assertNull(group.getGroupId());
    }

    @Test
    @DisplayName("GroupTest:setGroupId()")
    void setGroupId() {
        Long newId = 2L;
        group.setGroupId(newId);
        Assertions.assertEquals(newId, group.getGroupId());
    }

    @Test
    @DisplayName("GroupTest:getGroupName()")
    void getGroupName() {
        Assertions.assertEquals(groupName, group.getGroupName());
    }

    @Test
    @DisplayName("GroupTest:setGroupName()")
    void setGroupName() {
        String newGroupName = "newGroupName";
        group.setGroupName(newGroupName);
        Assertions.assertEquals(newGroupName, group.getGroupName());
    }
}