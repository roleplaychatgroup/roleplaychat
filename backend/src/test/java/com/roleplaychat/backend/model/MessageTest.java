package com.roleplaychat.backend.model;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.util.Date;

class MessageTest {
    private final Long messageId = 1L;
    private final String messageText = "messageText";
    private final Date sentTime = new Date();

    private final Long characterId = 1L;
    private final String characterName = "characterName";
    private final String characterGender = "male";

    @Mock private User sender    = Mockito.mock(User.class);
    @Mock private User newSender = Mockito.mock(User.class);
    @Mock private Group group    = Mockito.mock(Group.class);
    @Mock private Group newGroup = Mockito.mock(Group.class);

    private Message message;

    @BeforeEach
    void initMessage() {
        message = new Message(messageText, sentTime, sender, group,
                characterId, characterName, characterGender);
        message.setMessageId(messageId);
    }

    @Test
    @DisplayName("MessageTest:builder()")
    void builder() {
        Message message = Message.builder()
                .messageText(messageText)
                .sentTime(sentTime)
                .sender(sender)
                .group(group)
                .build();

        Assertions.assertEquals(messageText, message.getMessageText());
        Assertions.assertEquals(sentTime, message.getSentTime());
        Assertions.assertEquals(sender, message.getSender());
        Assertions.assertEquals(group, message.getGroup());
    }

    @Test
    @DisplayName("MessageTest:builderToString()")
    void builderToString() {
        Message.MessageBuilder builder = Message.builder()
                .messageText(messageText)
                .sentTime(sentTime)
                .sender(sender)
                .group(group)
                .characterId(characterId)
                .characterName(characterName)
                .characterGender(characterGender);

        Assertions.assertEquals(
                "Message.MessageBuilder(messageText=" + messageText + ", " +
                        "sentTime=" + sentTime + ", " +
                        "sender=" + sender + ", " +
                        "group=" + group + ", " +
                        "characterId=" + characterId + ", " +
                        "characterName=" + characterName + ", " +
                        "characterGender=" + characterGender + ")",
                builder.toString()
        );
    }

    @Test
    @DisplayName("MessageTest:noArgsConstructor()")
    void noArgsConstructor() {
        Message message = new Message();

        Assertions.assertNull(message.getMessageId());
        Assertions.assertNull(message.getMessageText());
        Assertions.assertNull(message.getSentTime());
        Assertions.assertNull(message.getSender());
        Assertions.assertNull(message.getGroup());
    }

    @Test
    @DisplayName("MessageTest:getMessageId()")
    void getMessageId() {
        Assertions.assertEquals(messageId, message.getMessageId());
    }

    @Test
    @DisplayName("MessageTest:getMessageText()")
    void getMessageText() {
        Assertions.assertEquals(messageText, message.getMessageText());
    }

    @Test
    @DisplayName("MessageTest:getSentTime()")
    void getSentTime() {
        Assertions.assertEquals(sentTime, message.getSentTime());
    }

    @Test
    @DisplayName("MessageTest:getSender()")
    void getSender() {
        Assertions.assertEquals(sender, message.getSender());
    }

    @Test
    @DisplayName("MessageTest:getGroup()")
    void getGroup() {
        Assertions.assertEquals(group, message.getGroup());
    }

    @Test
    @DisplayName("MessageTest:getCharacterId()")
    void getCharacterId() {
        Assertions.assertEquals(characterId, message.getCharacterId());
    }

    @Test
    @DisplayName("MessageTest:getCharacterName()")
    void getCharacterName() {
        Assertions.assertEquals(characterName, message.getCharacterName());
    }

    @Test
    @DisplayName("MessageTest:getCharacterGender()")
    void getCharacterGender() {
        Assertions.assertEquals(characterGender, message.getCharacterGender());
    }

    @Test
    @DisplayName("MessageTest:setMessageId()")
    void setMessageId() {
        Assertions.assertEquals(messageId, message.getMessageId());
        Long newMessageId = 2L;
        message.setMessageId(newMessageId);
        Assertions.assertEquals(newMessageId, message.getMessageId());
    }

    @Test
    @DisplayName("MessageTest:setMessageText()")
    void setMessageText() {
        String newMessageText = "newMessageText";
        Assertions.assertEquals(messageText, message.getMessageText());
        message.setMessageText(newMessageText);
        Assertions.assertEquals(newMessageText, message.getMessageText());
    }

    @Test
    @DisplayName("MessageTest:setSentTime()")
    void setSentTime() {
        Date newSentTime = new Date();
        Assertions.assertEquals(sentTime, message.getSentTime());
        message.setSentTime(newSentTime);
        Assertions.assertEquals(newSentTime, message.getSentTime());
    }

    @Test
    @DisplayName("MessageTest:setSender()")
    void setSender() {
        Assertions.assertEquals(sender, message.getSender());
        message.setSender(newSender);
        Assertions.assertEquals(newSender, message.getSender());
    }

    @Test
    @DisplayName("MessageTest:setGroup()")
    void setGroup() {
        Assertions.assertEquals(group, message.getGroup());
        message.setGroup(newGroup);
        Assertions.assertEquals(newGroup, message.getGroup());
    }

    @Test
    @DisplayName("MessageTest:setCharacterId()")
    void setCharacterId() {
        Assertions.assertEquals(characterId, message.getCharacterId());
        Long newCharacterId = 2L;
        message.setCharacterId(newCharacterId);
        Assertions.assertEquals(newCharacterId, message.getCharacterId());
    }

    @Test
    @DisplayName("MessageTest:setCharacterName()")
    void setCharacterName() {
        Assertions.assertEquals(characterName, message.getCharacterName());
        String newCharacterName = "newCharacterName";
        message.setCharacterName(newCharacterName);
        Assertions.assertEquals(newCharacterName, message.getCharacterName());
    }

    @Test
    @DisplayName("MessageTest:setCharacterGender()")
    void setCharacterGender() {
        Assertions.assertEquals(characterGender, message.getCharacterGender());
        String newCharacterGender = "female";
        message.setCharacterGender(newCharacterGender);
        Assertions.assertEquals(newCharacterGender, message.getCharacterGender());
    }
}