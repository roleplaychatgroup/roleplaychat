package com.roleplaychat.backend.model;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class RoleTest {
    private final Integer id = 1;
    private final RoleName roleName = RoleName.ROLE_USER;
    private Role role;

    @Test
    @DisplayName("RoleTest:builder()")
    void builder() {
        role = Role.builder()
                .id(id)
                .roleName(roleName).build();
        Assertions.assertNotNull(role);
        Assertions.assertEquals(id, role.getId());
        Assertions.assertEquals(roleName, role.getRoleName());
    }

    @Test
    @DisplayName("RoleTest:builderToString()")
    void builderToString() {
        Role.RoleBuilder builder = Role.builder()
                .id(id)
                .roleName(roleName);
        Assertions.assertEquals(
                "Role.RoleBuilder(id=" + id + ", roleName=" + roleName + ")",
                builder.toString());
    }

    @Test
    @DisplayName("RoleTest:allArgsConstructor()")
    void allArgsConstructor() {
        role = new Role(id, roleName);
        Assertions.assertNotNull(role);
        Assertions.assertEquals(id, role.getId());
        Assertions.assertEquals(roleName, role.getRoleName());
    }

    @Test
    @DisplayName("RoleTest:noArgsConstructor()")
    void noArgsConstructor() {
        role = new Role();
        Assertions.assertNotNull(role);
        Assertions.assertNull(role.getId());
        Assertions.assertNull(role.getRoleName());
    }

    @Test
    @DisplayName("RoleTest:getId()")
    void getId() {
        role = new Role(id, roleName);
        Assertions.assertEquals(id, role.getId());
    }

    @Test
    @DisplayName("RoleTest:getRoleName()")
    void getRoleName() {
        role = new Role(id, roleName);
        Assertions.assertEquals(roleName, role.getRoleName());
    }

    @Test
    @DisplayName("RoleTest:setRoleName()")
    void setRoleName() {
        role = new Role();
        Assertions.assertNull(role.getRoleName());

        role.setRoleName(roleName);
        Assertions.assertEquals(roleName, role.getRoleName());

    }
}