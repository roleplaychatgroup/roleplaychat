package com.roleplaychat.backend.model;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
class CharacterTest {
    private final String name = "characterName";
    private final String gender = "male";
    private final String note = "characterNote";
    private final long lifeCount = 10L;
    @Mock private User user = Mockito.mock(User.class);
    @Mock private User newUser = Mockito.mock(User.class);

    private Character character;

    @BeforeEach
    private void initCharacter() {
        character = new Character(
                name, user, gender, note, lifeCount
        );
    }

    @Test
    @DisplayName("CharacterTest:builder()")
    void builder() {
        Character.CharacterBuilder builder = Character.builder()
                .name(name)
                .user(user)
                .gender(gender)
                .note(note)
                .lifeCount(lifeCount);
        Character builtCharacter = builder.build();

        Character character = new Character();
        character.setName(name);
        character.setUser(user);
        character.setGender(gender);
        character.setNote(note);
        character.setLifeCount(lifeCount);

        Assertions.assertEquals(character.getName(), builtCharacter.getName());
        Assertions.assertEquals(character.getUser(), builtCharacter.getUser());
        Assertions.assertEquals(character.getGender(), builtCharacter.getGender());
        Assertions.assertEquals(character.getNote(), builtCharacter.getNote());
        Assertions.assertEquals(character.getLifeCount(), builtCharacter.getLifeCount());
    }

    @Test
    @DisplayName("CharacterTest:builderToString()")
    void builderToString() {
        Character.CharacterBuilder builder = Character.builder()
                .name(name)
                .user(user)
                .gender(gender)
                .note(note)
                .lifeCount(lifeCount);

        Assertions.assertEquals(
                "Character.CharacterBuilder(name=" + name + ", " +
                        "user=" + user + ", " +
                        "gender=" + gender + ", " +
                        "note=" + note + ", " +
                        "lifeCount=" + lifeCount + ")",
                builder.toString());
    }

    @Test
    @DisplayName("CharacterTest:getCharacterId()")
    void getCharacterId() {
        // До добавления в базу данных значение должно быть null
        Assertions.assertNull(character.getCharacterId());
    }

    @Test
    @DisplayName("CharacterTest:getName()")
    void getName() {
        Assertions.assertEquals(name, character.getName());
    }

    @Test
    @DisplayName("CharacterTest:getGender()")
    void getGender() {
        Assertions.assertEquals(gender, character.getGender());
    }

    @Test
    @DisplayName("CharacterTest:getNote()")
    void getNote() {
        Assertions.assertEquals(note, character.getNote());
    }

    @Test
    @DisplayName("CharacterTest:getUser()")
    void getUser() {
        Assertions.assertEquals(user, character.getUser());
    }

    @Test
    @DisplayName("CharacterTest:getLifeCount()")
    void getLifeCount() {
        Assertions.assertEquals(lifeCount, character.getLifeCount());
    }

    @Test
    @DisplayName("CharacterTest:setCharacterId()")
    void setCharacterId() {
        // До добавления в базу данных значение должно быть null
        Assertions.assertNull(character.getCharacterId());
        Long newId = 2L;
        character.setCharacterId(newId);
        Assertions.assertEquals(newId, character.getCharacterId());
    }

    @Test
    @DisplayName("CharacterTest:setName()")
    void setName() {
        String newCharacterName = "newCharacterName";
        character.setName(newCharacterName);
        Assertions.assertEquals(newCharacterName, character.getName());
    }

    @Test
    @DisplayName("CharacterTest:setGender()")
    void setGender() {
        String newGender = "female";
        character.setGender(newGender);
        Assertions.assertEquals(newGender, character.getGender());
    }

    @Test
    @DisplayName("CharacterTest:setNote()")
    void setNote() {
        String newNote = "newNote";
        character.setNote(newNote);
        Assertions.assertEquals(newNote, character.getNote());
    }

    @Test
    @DisplayName("CharacterTest:setUser()")
    void setUser() {
        character.setUser(newUser);
        Assertions.assertEquals(newUser, character.getUser());
    }

    @Test
    @DisplayName("CharacterTest:setLifeCount()")
    void setLifeCount() {
        long newLifeCount = lifeCount + 1;
        character.setLifeCount(newLifeCount);
        Assertions.assertEquals(newLifeCount, character.getLifeCount());
    }
}