package com.roleplaychat.backend.model;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
class GroupUserTest {
    @Mock private GroupUserKey id    = Mockito.mock(GroupUserKey.class);
    @Mock private GroupUserKey newId = Mockito.mock(GroupUserKey.class);
    @Mock private User user      = Mockito.mock(User.class);
    @Mock private User newUser   = Mockito.mock(User.class);
    @Mock private Group group    = Mockito.mock(Group.class);
    @Mock private Group newGroup = Mockito.mock(Group.class);
    private final boolean isItAdmin = false;
    private GroupUser groupUser;

    @BeforeEach
    private void initGroupUser() {
        groupUser = new GroupUser(
                id, user, group, isItAdmin
        );
    }

    @Test
    @DisplayName("GroupUserTest:allArgsConstructor()")
    void allArgsConstructor() {
        GroupUser groupUser = new GroupUser(
                id, user, group, isItAdmin
        );

        Assertions.assertEquals(id, groupUser.getId());
        Assertions.assertEquals(user, groupUser.getUser());
        Assertions.assertEquals(group, groupUser.getGroup());
        Assertions.assertFalse(groupUser.isItAdmin());
    }

    @Test
    @DisplayName("GroupUserTest:noArgsConstructor()")
    void noArgsConstructor() {
        GroupUser groupUser = new GroupUser();

        Assertions.assertNull(groupUser.getId());
        Assertions.assertNull(groupUser.getUser());
        Assertions.assertNull(groupUser.getGroup());
    }

    @Test
    @DisplayName("GroupUserTest:getId()")
    void getId() {
        Assertions.assertEquals(id, groupUser.getId());
    }

    @Test
    @DisplayName("GroupUserTest:getUser()")
    void getUser() {
        Assertions.assertEquals(user, groupUser.getUser());
    }

    @Test
    @DisplayName("GroupUserTest:getGroup()")
    void getGroup() {
        Assertions.assertEquals(group, groupUser.getGroup());
    }

    @Test
    @DisplayName("GroupUserTest:isItAdmin()")
    void isItAdmin() {
        groupUser.setItAdmin(false);
        Assertions.assertFalse(groupUser.isItAdmin());

        groupUser.setItAdmin(true);
        Assertions.assertTrue(groupUser.isItAdmin());
    }

    @Test
    @DisplayName("GroupUserTest:setId()")
    void setId() {
        Assertions.assertEquals(id, groupUser.getId());
        groupUser.setId(newId);
        Assertions.assertEquals(newId, groupUser.getId());
    }

    @Test
    @DisplayName("GroupUserTest:setUser()")
    void setUser() {
        Assertions.assertEquals(user, groupUser.getUser());
        groupUser.setUser(newUser);
        Assertions.assertEquals(newUser, groupUser.getUser());
    }

    @Test
    @DisplayName("GroupUserTest:setGroup()")
    void setGroup() {
        Assertions.assertEquals(group, groupUser.getGroup());
        groupUser.setGroup(newGroup);
        Assertions.assertEquals(newGroup, groupUser.getGroup());
    }

    @Test
    @DisplayName("GroupUserTest:setItAdmin()")
    void setItAdmin() {
        Assertions.assertFalse(groupUser.isItAdmin());
        groupUser.setItAdmin(true);
        Assertions.assertTrue(groupUser.isItAdmin());
    }
}