package com.roleplaychat.backend.model;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
class UserTest {
    private final Long userId     = 1L;
    private final String name     = "Name";
    private final String email    = "email@email.com";
    private final String password = "password";
    private final String gender   = "male";

    @Mock private Role role    = Mockito.mock(Role.class);
    @Mock private Role newRole = Mockito.mock(Role.class);
    private User user;

    @BeforeEach
    private void initUser() {
        user = new User(
                userId,
                name,
                email,
                password,
                gender,
                role
        );
    }

    @Test
    @DisplayName("UserTest:builder()")
    void builder() {
        user = User.builder()
                .name(name)
                .email(email)
                .password(password)
                .gender(gender)
                .build();

        Assertions.assertNotNull(user);
        Assertions.assertEquals(name, user.getName());
        Assertions.assertEquals(email, user.getEmail());
        Assertions.assertEquals(password, user.getPassword());
        Assertions.assertEquals(gender, user.getGender());
    }

    @Test
    @DisplayName("UserTest:allArgsConstructor()")
    void allArgsConstructor() {
        user = new User(userId, name, email, password, gender, role);
        Assertions.assertEquals(userId, user.getUserId());
        Assertions.assertEquals(name, user.getName());
        Assertions.assertEquals(email, user.getEmail());
        Assertions.assertEquals(password, user.getPassword());
        Assertions.assertEquals(gender, user.getGender());
        Assertions.assertEquals(role, user.getRole());
    }

    @Test
    @DisplayName("UserTest:noArgsConstructor()")
    void noArgsConstructor() {
        user = new User();
        Assertions.assertNull(user.getUserId());
        Assertions.assertNull(user.getName());
        Assertions.assertNull(user.getEmail());
        Assertions.assertNull(user.getPassword());
        Assertions.assertNull(user.getGender());
        Assertions.assertNull(user.getRole());
    }

    @Test
    @DisplayName("UserTest:constructorWithRole()")
    void constructorWithRole() {
        user = new User(name, password, gender, email, role);

        Assertions.assertNotNull(user);
        Assertions.assertEquals(name, user.getName());
        Assertions.assertEquals(email, user.getEmail());
        Assertions.assertEquals(password, user.getPassword());
        Assertions.assertEquals(gender, user.getGender());
        Assertions.assertEquals(role, user.getRole());
    }

    @Test
    @DisplayName("UserTest:builderToString()")
    void builderToString() {
        User.UserBuilder builder = User.builder()
                .name(name)
                .email(email)
                .password(password)
                .gender(gender);
        Assertions.assertEquals(
                "User.UserBuilder(name=" + name + ", " +
                        "password=" + password + ", " +
                        "gender=" + gender + ", " +
                        "email=" + email + ")",
                builder.toString());
    }

    @Test
    @DisplayName("UserTest:getUserId()")
    void getUserId() {
        Assertions.assertEquals(userId, user.getUserId());
    }

    @Test
    @DisplayName("UserTest:getName()")
    void getName() {
        Assertions.assertEquals(name, user.getName());
    }

    @Test
    @DisplayName("UserTest:getEmail()")
    void getEmail() {
        Assertions.assertEquals(email, user.getEmail());
    }

    @Test
    @DisplayName("UserTest:getPassword()")
    void getPassword() {
        Assertions.assertEquals(password, user.getPassword());
    }

    @Test
    @DisplayName("UserTest:getGender()")
    void getGender() {
        Assertions.assertEquals(gender, user.getGender());
    }

    @Test
    @DisplayName("UserTest:getRole()")
    void getRole() {
        Assertions.assertEquals(role, user.getRole());
    }

    @Test
    @DisplayName("UserTest:setName()")
    void setName() {
        Assertions.assertEquals(name, user.getName());
        String newName = "NewName";
        user.setName(newName);
        Assertions.assertEquals(newName, user.getName());
    }

    @Test
    @DisplayName("UserTest:setEmail()")
    void setEmail() {
        Assertions.assertEquals(email, user.getEmail());
        String newEmail = "new@email.com";
        user.setEmail(newEmail);
        Assertions.assertEquals(newEmail, user.getEmail());
    }

    @Test
    @DisplayName("UserTest:setPassword()")
    void setPassword() {
        Assertions.assertEquals(password, user.getPassword());
        String newPassword = "newPassword";
        user.setPassword(newPassword);
        Assertions.assertEquals(newPassword, user.getPassword());
    }

    @Test
    @DisplayName("UserTest:setGender()")
    void setGender() {
        Assertions.assertEquals(gender, user.getGender());
        String newGender = "female";
        user.setGender(newGender);
        Assertions.assertEquals(newGender, user.getGender());
    }

    @Test
    @DisplayName("UserTest:setRole()")
    void setRole() {
        Assertions.assertEquals(role, user.getRole());
        user.setRole(newRole);
        Assertions.assertEquals(newRole, user.getRole());
    }
}