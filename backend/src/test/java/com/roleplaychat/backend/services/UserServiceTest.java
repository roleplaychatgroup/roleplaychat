package com.roleplaychat.backend.services;

import com.roleplaychat.backend.dto.UserDto;
import com.roleplaychat.backend.model.RoleName;
import com.roleplaychat.backend.model.Role;
import com.roleplaychat.backend.model.User;
import com.roleplaychat.backend.repo.UserRepository;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

@ExtendWith(MockitoExtension.class)
class UserServiceTest {
    @Mock private UserRepository userRepository;
    @Mock private RoleService roleService;

    private UserService userService;

    @BeforeEach
    void initUserService() {
        this.userService = new UserService(userRepository, roleService);
    }

    @Test
    @DisplayName("UserServiceTest:getUserById()")
    void getUserById() {
        User mockedUser = User.builder()
                .name("Пастилова Полина")
                .build();
        Mockito.when(userRepository.findById(35L)).thenReturn(Optional.of(mockedUser));

        Optional<User> user = userService.getUserById(35L);

        Assertions.assertTrue(user.isPresent());
        Assertions.assertEquals(mockedUser.getName(), user.get().getName());

        /*
         * Проверяем, что метод findById вызывается ровно один раз во время
         * выполнения теста
         */
        Mockito.verify(userRepository, Mockito.times(1))
                .findById(Mockito.any());
    }

    @Test
    @DisplayName("UserServiceTest:getUserByEmail()")
    void getUserByEmail() {
        String email = "pastilova@email.com";
        User mockedUser = User.builder()
                .name("Пастилова Полина")
                .email(email)
                .build();
        Mockito.when(userRepository.findByEmail(email)).thenReturn(Optional.of(mockedUser));

        Optional<User> user = userService.getUserByEmail(email);

        Assertions.assertTrue(user.isPresent());
        Assertions.assertEquals(mockedUser.getName(), user.get().getName());
        Assertions.assertEquals(mockedUser.getEmail(), user.get().getEmail());

        /*
         * Проверяем, что метод findById вызывается ровно один раз во время
         * выполнения теста
         */
        Mockito.verify(userRepository, Mockito.times(1))
                .findByEmail(Mockito.any());
    }

    @Test
    @DisplayName("UserServiceTest:getUserByIdFailed()")
    void getUserByIdFailed() {
        Mockito.when(userRepository.findById(35L)).thenReturn(Optional.empty());

        Optional<User> user = userService.getUserById(35L);

        Assertions.assertFalse(user.isPresent());

        /*
         * Проверяем, что метод findById вызывается ровно один раз во время
         * выполнения теста
         */
        Mockito.verify(userRepository, Mockito.times(1))
                .findById(Mockito.any());
    }

    @Test
    @DisplayName("UserServiceTest:save()")
    void save() {
        User mockedUser = User.builder()
                .name("Пастилова Полина")
                .build();
        Mockito.when(userRepository.save(mockedUser)).thenReturn(mockedUser);

        userService.save(mockedUser);

        Mockito.verify(userRepository, Mockito.times(1))
                .save(Mockito.any());
    }

    @Test
    @DisplayName("UserServiceTest:existsByEmail()")
    void existsByEmail() {
        User mockedUser = User.builder()
                .name("Пастилова Полина")
                .email("pastilova@email.com")
                .build();
        Mockito.when(userRepository.existsByEmail(mockedUser.getEmail()))
                .thenReturn(true);

        Boolean userExists = userService.existsByEmail(mockedUser.getEmail());

        Assertions.assertTrue(userExists);
        Mockito.verify(userRepository, Mockito.times(1))
                .existsByEmail(Mockito.any());
    }

    @Test
    @DisplayName("UserServiceTest:existsByEmailFailed()")
    void existsByEmailFailed() {
        User mockedUser = User.builder()
                .email("pastilova@email.com")
                .build();
        Mockito.when(userRepository.existsByEmail(mockedUser.getEmail()))
                .thenReturn(false);

        Boolean userExists = userService.existsByEmail(mockedUser.getEmail());

        Assertions.assertFalse(userExists);
        Mockito.verify(userRepository, Mockito.times(1))
                .existsByEmail(Mockito.any());
    }

    @Test
    @DisplayName("UserServiceTest:setRoles()")
    void setRoles() {
        Role role = new Role();
        role.setRoleName(RoleName.ROLE_USER);
        Mockito.when(roleService.findByRoleName(RoleName.ROLE_USER))
                .thenReturn(Optional.of(role));

        // UserDto creation
        UserDto userDto = new UserDto();
        userService.setRoles(null, userDto);

        Assertions.assertNotNull(userDto.getRole());
        Assertions.assertEquals(role.getRoleName(), userDto.getRole().getRoleName());

        Mockito.verify(roleService, Mockito.times(1))
                .findByRoleName(Mockito.any());
    }

    @Test
    @DisplayName("UserServiceTest:setRolesThrowsRuntimeException()")
    void setRolesThrowsRuntimeException() {
        Mockito.when(roleService.findByRoleName(RoleName.ROLE_USER))
                .thenReturn(Optional.empty());

        Assertions.assertThrows(RuntimeException.class, () -> {
            userService.setRoles(null, new UserDto());
        });

        Mockito.verify(roleService, Mockito.times(1))
                .findByRoleName(Mockito.any());
    }
}