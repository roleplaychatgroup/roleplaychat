package com.roleplaychat.backend.services;

import com.roleplaychat.backend.exceptions.UserNotFoundException;
import com.roleplaychat.backend.model.Group;
import com.roleplaychat.backend.model.GroupUser;
import com.roleplaychat.backend.model.GroupUserKey;
import com.roleplaychat.backend.model.User;
import com.roleplaychat.backend.repo.GroupRepository;
import com.roleplaychat.backend.repo.GroupUserRepository;
import com.roleplaychat.backend.request.AddOrEditGroupRequest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;

@ExtendWith(MockitoExtension.class)
class GroupServiceTest {
    private final Long userId = 1L;
    private final Long groupId = 1L;
    private final String groupName = "groupName";

    @Mock private GroupRepository groupRepository;
    @Mock private GroupUserRepository groupUserRepository;
    @Mock private UserService userService;
    @Mock private CharacterService characterService;

    private GroupService groupService;

    @BeforeEach
    private void initGroupService() {
        groupService = new GroupService(groupRepository, groupUserRepository, userService, characterService);
    }

    @Test
    @DisplayName("GroupServiceTest:getGroupById()")
    void getGroupById() {
        Group group = new Group(groupName);
        Mockito.when(groupRepository.findById(groupId)).thenReturn(Optional.of(group));

        Assertions.assertTrue(groupService.getGroupById(groupId).isPresent());
        Assertions.assertEquals(group.getGroupName(), groupService.getGroupById(groupId).get().getGroupName());
    }

    @Test
    @DisplayName("GroupServiceTest:getGroupByIdEmptyGroup()")
    void getGroupByIdEmptyGroup() {
        Optional<Group> emptyGroup = Optional.empty();
        Mockito.when(groupRepository.findById(groupId)).thenReturn(emptyGroup);

        Assertions.assertEquals(emptyGroup, groupService.getGroupById(groupId));
    }

    @Test
    @DisplayName("GroupServiceTest:getAllGroups()")
    void getAllGroups() {
        Group group = new Group(groupName);
        group.setGroupId(groupId);

        List<GroupUser> groups = new ArrayList<>();
        groups.add(new GroupUser(
                new GroupUserKey(), new User(), group, true
        ));

        Mockito.when(groupUserRepository.findGroupUsersByUser_UserId(userId))
                .thenReturn(groups);

        ResponseEntity<?> response = groupService.getAllGroups(userId);
        Assertions.assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    @DisplayName("GroupServiceTest:getAllGroups()")
    void getAllGroupsWhenNoGroups() {
        Mockito.when(groupUserRepository.findGroupUsersByUser_UserId(userId))
                .thenReturn(new ArrayList<>());

        ResponseEntity<?> response = groupService.getAllGroups(userId);
        Assertions.assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
    }

    @Test
    @DisplayName("GroupServiceTest:getGroupInfo()")
    void getGroupInfo() {
        User user = new User("username", "password", "male", "user@email.com");
        user.setUserId(userId);

        Group group = new Group("groupName");
        group.setGroupId(1L);

        List<GroupUser> groupUsers = new ArrayList<>();
        groupUsers.add(new GroupUser(
                new GroupUserKey(), user, group, true
        ));

        Mockito.when(groupRepository.findById(groupId)).thenReturn(Optional.of(group));

        Mockito.when(groupUserRepository.findGroupUsersByGroup_GroupId(groupId))
                .thenReturn(groupUsers);

        Mockito.when(characterService.getCharactersByUser(Mockito.any())).thenReturn(new HashSet<>());

        ResponseEntity<?> response = groupService.getGroupInfo(groupId);
        Assertions.assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    @DisplayName("GroupServiceTest:getGroupInfoReturnsBadRequest()")
    void getGroupInfoReturnsBadRequest() {
        Mockito.when(groupRepository.findById(groupId)).thenReturn(Optional.empty());

        ResponseEntity<?> response = groupService.getGroupInfo(groupId);
        Assertions.assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    @Test
    @DisplayName("GroupServiceTest:editGroup()")
    void editGroup() {
        AddOrEditGroupRequest request = new AddOrEditGroupRequest(
                new ArrayList<>(), "groupName", userId
        );
        User user = new User("username", "password", "male", "user@email.com");
        user.setUserId(userId);

        Group group = new Group("groupName");
        group.setGroupId(1L);

        Mockito.when(groupRepository.findById(groupId)).thenReturn(Optional.of(group));

        Mockito.when(userService.getUserById(userId)).thenReturn(Optional.of(user));

        ResponseEntity<?> response = groupService.editGroup(groupId, request);
        Assertions.assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    @DisplayName("GroupServiceTest:editGroupThrowsGroupNotFoundException()")
    void editGroupThrowsGroupNotFoundException() {
        AddOrEditGroupRequest request = new AddOrEditGroupRequest(
                new ArrayList<>(), "groupName", userId
        );

        Mockito.when(groupRepository.findById(groupId)).thenReturn(Optional.empty());

        ResponseEntity<?> response = groupService.editGroup(groupId, request);
        Assertions.assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    @Test
    @DisplayName("GroupServiceTest:editGroupThrowsUserNotFoundException()")
    void editGroupThrowsUserNotFoundException() {
        AddOrEditGroupRequest request = new AddOrEditGroupRequest(
                new ArrayList<>(), "groupName", userId
        );

        Group group = new Group("groupName");

        Mockito.when(groupRepository.findById(groupId)).thenReturn(Optional.of(group));

        Mockito.when(userService.getUserById(userId)).thenReturn(Optional.empty());

        ResponseEntity<?> response = groupService.editGroup(groupId, request);
        Assertions.assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    @Test
    @DisplayName("GroupServiceTest:updateGroupUserRepositoryThrowsUserNotFoundException()")
    void updateGroupUserRepositoryThrowsUserNotFoundException() {
        Group group = new Group(groupName);
        group.setGroupId(groupId);

        AddOrEditGroupRequest request = new AddOrEditGroupRequest(
                new ArrayList<>(), "groupName", userId
        );

        Mockito.when(userService.getUserById(userId)).thenReturn(Optional.empty());

        Assertions.assertThrows(UserNotFoundException.class, () ->
                groupService.updateGroupUserRepository(request, group)
        );
    }
}