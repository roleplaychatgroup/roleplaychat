package com.roleplaychat.backend.services;

import com.roleplaychat.backend.dto.*;
import com.roleplaychat.backend.exceptions.CharacterNotFoundException;
import com.roleplaychat.backend.model.Character;
import com.roleplaychat.backend.model.User;
import com.roleplaychat.backend.repo.CharacterRepository;
import com.roleplaychat.backend.request.CharacterCreationRequest;
import com.roleplaychat.backend.response.CharacterResponse;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.security.Principal;
import java.util.*;
import java.util.stream.Collectors;

@ExtendWith(MockitoExtension.class)
class CharacterServiceTest {
    private final static int CHARACTER_LIMIT = 5;
    private final Long characterId = 1L;

    @Mock private Character characterA;
    @Mock private Character characterB;
    @Mock private User user;

    @Mock private UserService userService;
    @Mock private CharacterRepository characterRepository;
    @Mock private UserDtoConverter userDtoConverter;
    @Mock private Principal principal;

    private CharacterService characterService;

    @BeforeEach
    private void initCharacterService() {
        characterService = new CharacterService(userService, characterRepository);
    }

    @Test
    @DisplayName("CharacterServiceTest:getCharactersByUser()")
    void getCharactersByUser() {
        Mockito.when(characterA.getCharacterId()).thenReturn(1L);
        Mockito.when(characterA.getName()).thenReturn("characterA");
        Mockito.when(characterA.getGender()).thenReturn("male");
        Mockito.when(characterA.getNote()).thenReturn("characterANotes");

        Mockito.when(characterB.getCharacterId()).thenReturn(2L);
        Mockito.when(characterB.getName()).thenReturn("characterB");
        Mockito.when(characterB.getGender()).thenReturn("female");
        Mockito.when(characterB.getNote()).thenReturn("characterBNotes");

        Set<Character> characters = new HashSet<>();
        characters.add(characterA);
        characters.add(characterB);

        Mockito.when(characterRepository.findCharactersByUser(user)).thenReturn(characters);

        Set<CharacterResponse> responseSet = characterService.getCharactersByUser(user);
        Map<String, CharacterResponse> responseList = responseSet.stream()
                .collect(Collectors.toMap(CharacterResponse::getCharacterName, e -> e));

        CharacterResponse responseA = responseList.get("characterA");
        Assertions.assertEquals(characterA.getCharacterId(), responseA.getCharacterId());
        Assertions.assertEquals(characterA.getGender(),      responseA.getCharacterGender());
        Assertions.assertEquals(characterA.getNote(),        responseA.getNote());
        Assertions.assertEquals(characterA.getLifeCount(),   responseA.getLifeCount());

        CharacterResponse responseB = responseList.get("characterB");
        Assertions.assertEquals(characterB.getCharacterId(), responseB.getCharacterId());
        Assertions.assertEquals(characterB.getGender(),      responseB.getCharacterGender());
        Assertions.assertEquals(characterB.getNote(),        responseB.getNote());
        Assertions.assertEquals(characterB.getLifeCount(),   responseB.getLifeCount());
    }

    @Test
    @DisplayName("CharacterServiceTest:getCharacterById()")
    void getCharacterById() throws CharacterNotFoundException {
        Mockito.when(characterA.getName()).thenReturn("characterA");
        Mockito.when(characterA.getGender()).thenReturn("male");
        Mockito.when(characterA.getNote()).thenReturn("characterANotes");

        Mockito.when(characterRepository.findById(characterId)).thenReturn(Optional.of(characterA));

        Character character = characterService.getCharacterById(characterId);

        Assertions.assertEquals(characterA.getName(), character.getName());
        Assertions.assertEquals(characterA.getGender(), character.getGender());
        Assertions.assertEquals(characterA.getNote(), character.getNote());

        Mockito.verify(characterRepository, Mockito.times(1))
                .findById(Mockito.any());
    }

    @Test
    @DisplayName("UserServiceTest:getCharacterByIdFailed()")
    void getCharacterByIdFailed() {
        Mockito.when(characterRepository.findById(characterId)).thenReturn(Optional.empty());

        Assertions.assertThrows(CharacterNotFoundException.class, () ->
                characterService.getCharacterById(characterId));

        Mockito.verify(characterRepository, Mockito.times(1))
                .findById(Mockito.any());
    }

    @Test
    @DisplayName("CharacterServiceTest:getListCharacters()")
    void getListCharacters() {
        Mockito.when(principal.getName()).thenReturn("testName");

        Mockito.when(userService.getUserByEmail(Mockito.any())).
                thenReturn(Optional.of(user));

        Mockito.when(characterRepository.findCharactersByUser(user)).thenReturn(new HashSet<>());

        ResponseEntity<?> response = characterService.getListCharacters(principal);
        Assertions.assertEquals(HttpStatus.OK, response.getStatusCode());

        Mockito.verify(principal, Mockito.times(1))
                .getName();
        Mockito.verify(userService, Mockito.times(1))
                .getUserByEmail(Mockito.any());
        Mockito.verify(characterRepository, Mockito.times(1))
                .findCharactersByUser(user);
    }

    @Test
    @DisplayName("CharacterServiceTest:getListCharactersThrowsUserNotFoundException()")
    void getListCharactersThrowsUserNotFoundException() {
        Mockito.when(principal.getName()).thenReturn("testName");

        Mockito.when(userService.getUserByEmail(Mockito.any())).
                thenReturn(Optional.empty());

        ResponseEntity<?> response = characterService.getListCharacters(principal);
        Assertions.assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    @Test
    @DisplayName("CharacterServiceTest:getListCharactersByEmail()")
    void getListCharactersByEmail() {
        String email = "email@email.com";

        Mockito.when(userService.getUserByEmail(Mockito.any())).
                thenReturn(Optional.of(user));

        ResponseEntity<?> response = characterService.getListCharactersByEmail(email);
        Assertions.assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    @DisplayName("CharacterServiceTest:getListCharactersByEmailThrowsUserNotFoundException()")
    void getListCharactersByEmailThrowsUserNotFoundException() {
        String email = "email@email.com";

        Mockito.when(userService.getUserByEmail(Mockito.any())).
                thenReturn(Optional.empty());

        ResponseEntity<?> response = characterService.getListCharactersByEmail(email);
        Assertions.assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    @Test
    @DisplayName("CharacterServiceTest:createCharacter()")
    void createCharacter() {
        // Data for the test
        Character character = new Character();
        CharacterCreationRequest request = new CharacterCreationRequest(
                "testName", "male"
        );

        Mockito.when(principal.getName()).thenReturn("testName");

        Mockito.when(userService.getUserByEmail(Mockito.any())).
                thenReturn(Optional.of(user));

        Mockito.when(characterRepository.findCharactersByUser(user)).thenReturn(new HashSet<>());
        Mockito.when(characterRepository.save(Mockito.any())).thenReturn(character);

        ResponseEntity<?> response = characterService.createCharacter(principal, request);
        Assertions.assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    @DisplayName("CharacterServiceTest:createCharacterReachedCharacterLimit()")
    void createCharacterReachedCharacterLimit() {
        CharacterCreationRequest request = new CharacterCreationRequest(
                "testName", "male"
        );
        Mockito.when(principal.getName()).thenReturn("testName");

        Mockito.when(userService.getUserByEmail(Mockito.any())).
                thenReturn(Optional.of(user));

        Set<Character> characters = new HashSet<>();
        for (int i = 0; i < CHARACTER_LIMIT; ++i) {
            characters.add(new Character());
        }
        Mockito.when(characterRepository.findCharactersByUser(user)).thenReturn(characters);

        ResponseEntity<?> response = characterService.createCharacter(principal, request);
        Assertions.assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    @Test
    @DisplayName("CharacterServiceTest:createCharacterThrowsUserNotFoundException()")
    void createCharacterThrowsUserNotFoundException() {
        CharacterCreationRequest request = new CharacterCreationRequest(
                "testName", "male"
        );
        Mockito.when(principal.getName()).thenReturn("testName");

        Mockito.when(userService.getUserByEmail(Mockito.any())).
                thenReturn(Optional.empty());

        ResponseEntity<?> response = characterService.createCharacter(principal, request);
        Assertions.assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
    }

    @Test
    @DisplayName("CharacterServiceTest:deleteCharacter()")
    void deleteCharacter() {
        Mockito.doNothing().when(characterRepository).deleteById(characterId);

        ResponseEntity<?> response = characterService.deleteCharacter(characterId);
        Assertions.assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    @DisplayName("CharacterServiceTest:addLife()")
    void addLife() {
        Mockito.when(characterA.getCharacterId()).thenReturn(1L);
        Mockito.when(characterA.getName()).thenReturn("characterA");
        Mockito.when(characterA.getGender()).thenReturn("male");
        Mockito.when(characterA.getNote()).thenReturn("characterANotes");

        Mockito.when(characterRepository.findById(characterId)).thenReturn(Optional.of(characterA));

        ResponseEntity<?> response = characterService.addLife(characterId);
        Assertions.assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    @DisplayName("CharacterServiceTest:addLifeThrowsCharacterNotFoundException()")
    void addLifeThrowsCharacterNotFoundException() {
        Mockito.when(characterRepository.findById(characterId)).thenReturn(Optional.empty());

        ResponseEntity<?> response = characterService.addLife(characterId);
        Assertions.assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    @Test
    @DisplayName("CharacterServiceTest:deleteLife()")
    void deleteLife() {
        Mockito.when(characterA.getCharacterId()).thenReturn(1L);
        Mockito.when(characterA.getName()).thenReturn("characterA");
        Mockito.when(characterA.getGender()).thenReturn("male");
        Mockito.when(characterA.getNote()).thenReturn("characterANotes");

        Mockito.when(characterRepository.findById(characterId)).thenReturn(Optional.of(characterA));

        ResponseEntity<?> response = characterService.deleteLife(characterId);
        Assertions.assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    @DisplayName("CharacterServiceTest:deleteLifeThrowsCharacterNotFoundException()")
    void deleteLifeThrowsCharacterNotFoundException() {
        Mockito.when(characterRepository.findById(characterId)).thenReturn(Optional.empty());

        ResponseEntity<?> response = characterService.deleteLife(characterId);
        Assertions.assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    @Test
    @DisplayName("CharacterServiceTest:save()")
    void save() {
        Character mockedCharacter = Character.builder()
                .name("characterName")
                .build();
        Mockito.when(characterRepository.save(Mockito.any())).thenReturn(mockedCharacter);

        CharacterDto mockedDto = new CharacterDto();
        mockedDto.setName(mockedCharacter.getName());

        characterService.save(mockedDto);

        Mockito.verify(characterRepository, Mockito.times(1))
                .save(Mockito.any());
    }
}