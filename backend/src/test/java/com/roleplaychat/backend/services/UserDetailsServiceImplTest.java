package com.roleplaychat.backend.services;

import com.roleplaychat.backend.model.Role;
import com.roleplaychat.backend.model.RoleName;
import com.roleplaychat.backend.model.User;
import com.roleplaychat.backend.repo.UserRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.Optional;

@ExtendWith(MockitoExtension.class)
class UserDetailsServiceImplTest {

    @Mock private UserRepository userRepository;

    private UserDetailsServiceImpl service;

    @BeforeEach
    private void initService() {
        service = new UserDetailsServiceImpl(userRepository);
    }

    @Test
    @DisplayName("UserDetailsServiceImplTest:loadUserByUsername()")
    void loadUserByUsername() {
        String username = "username";
        String password = "password";
        String gender = "male";
        String email = "email@email.com";

        User user = new User(
                username, password, gender, email
        );
        user.setRole(new Role(1, RoleName.ROLE_USER));
        Mockito.when(userRepository.findByEmail(Mockito.any())).thenReturn(Optional.of(user));

        UserDetails details = service.loadUserByUsername("");

        Assertions.assertEquals(email, details.getUsername());
        Assertions.assertEquals(password, details.getPassword());
    }

    @Test
    @DisplayName("UserDetailsServiceImplTest:loadUserByUsernameThrowsUsernameNotFoundException()")
    void loadUserByUsernameThrowsUsernameNotFoundException() {
        Mockito.when(userRepository.findByEmail(Mockito.any())).thenReturn(Optional.empty());

        Assertions.assertThrows(UsernameNotFoundException.class, () ->
                service.loadUserByUsername(""));
    }
}