package com.roleplaychat.backend.services;

import com.roleplaychat.backend.model.Role;
import com.roleplaychat.backend.model.RoleName;
import com.roleplaychat.backend.repo.RoleRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

@ExtendWith(MockitoExtension.class)
class RoleServiceTest {

    @Mock private RoleRepository roleRepository;

    private RoleService roleService;

    @BeforeEach
    void initRoleService() {
        roleService = new RoleService(roleRepository);
    }

    @Test
    void findByRoleName() {
        Role mockedRole = Role.builder()
                .roleName(RoleName.ROLE_USER).build();
        Mockito.when(roleRepository.findByRoleName(RoleName.ROLE_USER))
                .thenReturn(Optional.of(mockedRole));

        Optional<Role> role = roleService.findByRoleName(RoleName.ROLE_USER);

        Assertions.assertTrue(role.isPresent());
        Assertions.assertEquals(mockedRole.getRoleName(), role.get().getRoleName());

        /*
         * Проверяем, что метод findByRoleName вызывается ровно один раз во время
         * выполнения теста
         */
        Mockito.verify(roleRepository, Mockito.times(1))
                .findByRoleName(Mockito.any());
    }

    @Test
    void findByRoleNameFailed() {
        Mockito.when(roleRepository.findByRoleName(RoleName.ROLE_USER))
                .thenReturn(Optional.empty());

        Optional<Role> role = roleService.findByRoleName(RoleName.ROLE_USER);
        Assertions.assertFalse(role.isPresent());

        /*
         * Проверяем, что метод findByRoleName вызывается ровно один раз во время
         * выполнения теста
         */
        Mockito.verify(roleRepository, Mockito.times(1))
                .findByRoleName(Mockito.any());
    }
}