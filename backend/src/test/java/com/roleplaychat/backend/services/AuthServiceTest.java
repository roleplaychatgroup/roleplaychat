package com.roleplaychat.backend.services;

import com.roleplaychat.backend.auth.payload.request.EditRequest;
import com.roleplaychat.backend.auth.payload.request.LoginRequest;
import com.roleplaychat.backend.auth.payload.request.SignupRequest;
import com.roleplaychat.backend.auth.payload.response.JwtResponse;
import com.roleplaychat.backend.auth.user_info.UserDetailsImpl;
import com.roleplaychat.backend.dto.UserDto;
import com.roleplaychat.backend.exceptions.EmailInUseException;
import com.roleplaychat.backend.exceptions.PasswordNotValidException;
import com.roleplaychat.backend.model.User;
import com.roleplaychat.backend.utils.JwtUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;

@ExtendWith(MockitoExtension.class)
class AuthServiceTest {
    @Mock private AuthenticationManager authenticationManager;
    @Mock private Authentication authentication;
    @Mock private Authentication newAuthentication;
    @Mock private JwtUtils jwtUtils;
    @Mock private PasswordEncoder encoder;
    @Mock private UserService userService;

    private AuthService authService;

    @BeforeEach
    private void initAuthService() {
        authService = new AuthService(
                authenticationManager, jwtUtils, encoder, userService
        );
    }

    @Test
    @DisplayName("AuthServiceTest:createNewUser()")
    void createNewUser() {
        String username = "testUsername";
        String email = "email@email.com";
        String gender = "male";
        String password = "password";

        SignupRequest request = new SignupRequest(
                username, email, gender, password, new HashSet<>()
        );

        User user = new User(username, password, gender, email);
        Mockito.when(userService.save(Mockito.any())).thenReturn(user);

        User savedUser = authService.createNewUser(request);

        Assertions.assertEquals(user.getName(),     savedUser.getName());
        Assertions.assertEquals(user.getEmail(),    savedUser.getEmail());
        Assertions.assertEquals(user.getGender(),   savedUser.getGender());
        Assertions.assertEquals(user.getPassword(), savedUser.getPassword());
    }

    @Test
    @DisplayName("AuthServiceTest:editUserInfo()")
    void editUserInfo() {
        String username = "testUsername";
        String email = "email@email.com";
        String gender = "male";
        String oldPassword = "oldPassword";
        String newPassword = "newPassword";
        String jwt = "jwt";

        EditRequest request = new EditRequest(
                username, oldPassword, newPassword, gender, null
        );

        User user = new User(username, oldPassword, gender, email);
        Mockito.when(userService.getUserByEmail(Mockito.any())).thenReturn(Optional.of(user));

        Mockito.when(encoder.matches(Mockito.any(), Mockito.anyString())).thenReturn(true);

        Mockito.when(jwtUtils.generateToken(Mockito.any())).thenReturn(jwt);

        ResponseEntity<?> response = authService.editUserInfo(authentication, request);
        Assertions.assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    @DisplayName("AuthServiceTest:editUserInfoWhenNameAndGenderAreNull()")
    void editUserInfoWhenNameAndGenderAreNull() {
        String username = "testUsername";
        String email = "email@email.com";
        String gender = "male";
        String oldPassword = "oldPassword";
        String newPassword = "newPassword";
        String jwt = "jwt";

        EditRequest request = new EditRequest(
                null, oldPassword, newPassword, null, null
        );

        User user = new User(username, oldPassword, gender, email);
        Mockito.when(userService.getUserByEmail(Mockito.any())).thenReturn(Optional.of(user));

        Mockito.when(encoder.matches(Mockito.any(), Mockito.anyString())).thenReturn(true);

        Mockito.when(jwtUtils.generateToken(Mockito.any())).thenReturn(jwt);

        ResponseEntity<?> response = authService.editUserInfo(authentication, request);
        Assertions.assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    @DisplayName("AuthServiceTest:editUserInfoThrowsUserNotFoundException()")
    void editUserInfoThrowsUserNotFoundException() {
        Mockito.when(userService.getUserByEmail(Mockito.any())).thenReturn(Optional.empty());

        EditRequest request = new EditRequest(
                null, null, null, null, null
                );
        ResponseEntity<?> response = authService.editUserInfo(authentication, request);
        Assertions.assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    @Test
    @DisplayName("AuthServiceTest:getUserInfoReturnsOk()")
    void getUserInfoReturnsOk() {
        long userId = 1L;
        String username = "testUsername";
        String email = "email@email.com";
        String gender = "male";
        String password = "password";

        User user = new User(username, password, gender, email);

        Mockito.when(userService.getUserById(Mockito.any())).thenReturn(Optional.of(user));

        ResponseEntity<?> response = authService.getUserInfo(userId);
        Assertions.assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    @DisplayName("AuthServiceTest:getUserInfoReturnsBadRequest()")
    void getUserInfoReturnsBadRequest() {
        long userId = 1L;

        Mockito.when(userService.getUserById(Mockito.any())).thenReturn(Optional.empty());

        ResponseEntity<?> response = authService.getUserInfo(userId);
        Assertions.assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    @Test
    @DisplayName("AuthServiceTest:createJwtResponse()")
    void createJwtResponse() {
        String jwt = "jwt";

        Long id = 1L;
        String name = "testUsername";
        String email = "email@email.com";
        String password = "password";
        String gender = "male";

        List<GrantedAuthority> authorities = new ArrayList<>();
        UserDetailsImpl userDetails = new UserDetailsImpl(
                id, name, email, password, gender, authorities
        );
        LoginRequest loginRequest = new LoginRequest(email, password);

        Mockito.when(authenticationManager.authenticate(Mockito.any())).thenReturn(authentication);

        Mockito.when(jwtUtils.generateToken(Mockito.any())).thenReturn(jwt);

        Mockito.when(authentication.getPrincipal()).thenReturn(userDetails);

        JwtResponse response = authService.createJwtResponse(loginRequest);
        Assertions.assertEquals(jwt, response.getToken());
        Assertions.assertEquals(id, response.getId());
        Assertions.assertEquals(name, response.getName());
        Assertions.assertEquals(gender, response.getGender());
        Assertions.assertEquals(email, response.getEmail());
    }

    @Test
    @DisplayName("AuthServiceTest:createNewJwtToken()")
    void createNewJwtToken() throws EmailInUseException, PasswordNotValidException {
        String username = "testUsername";
        String email = "email@email.com";
        String otherEmail = "otherEmail@email.com";
        String gender = "male";
        String oldPassword = "oldPassword";
        String newPassword = "newPassword";
        String jwt = "jwt";

        UserDto userDto = UserDto.builder()
                .name(username)
                .email(email)
                .password(oldPassword)
                .gender(gender).build();

        EditRequest request = new EditRequest(
                username, oldPassword, newPassword, gender, otherEmail
        );

        Mockito.when(userService.save(Mockito.any())).thenReturn(new User());
        Mockito.when(userService.existsByEmail(Mockito.any())).thenReturn(false);

        Mockito.when(encoder.matches(Mockito.any(), Mockito.anyString())).thenReturn(true);

        Mockito.when(authenticationManager.authenticate(Mockito.any())).thenReturn(newAuthentication);

        Mockito.when(jwtUtils.generateToken(Mockito.any())).thenReturn(jwt);

        Assertions.assertEquals("jwt",
                authService.createNewJwtToken(request, userDto, authentication));
    }

    @Test
    @DisplayName("AuthServiceTest:createNewJwtTokenWhenEmailIsSame()")
    void createNewJwtTokenWhenEmailIsSame() throws EmailInUseException, PasswordNotValidException {
        String username = "testUsername";
        String email = "email@email.com";
        String gender = "male";
        String oldPassword = "oldPassword";
        String newPassword = "newPassword";
        String jwt = "jwt";

        UserDto userDto = UserDto.builder()
                .name(username)
                .email(email)
                .password(oldPassword)
                .gender(gender).build();

        EditRequest request = new EditRequest(
                username, oldPassword, newPassword, gender, email
        );

        Mockito.when(userService.save(Mockito.any())).thenReturn(new User());

        Mockito.when(encoder.matches(Mockito.any(), Mockito.anyString())).thenReturn(true);

        Mockito.when(authenticationManager.authenticate(Mockito.any())).thenReturn(newAuthentication);

        Mockito.when(jwtUtils.generateToken(Mockito.any())).thenReturn(jwt);

        Assertions.assertEquals("jwt",
                authService.createNewJwtToken(request, userDto, authentication));
    }

    @Test
    @DisplayName("AuthServiceTest:createNewJwtTokenWhenEmailIsNull()")
    void createNewJwtTokenWhenEmailIsNull() throws EmailInUseException, PasswordNotValidException {
        String username = "testUsername";
        String email = "email@email.com";
        String gender = "male";
        String oldPassword = "oldPassword";
        String newPassword = "newPassword";
        String jwt = "jwt";

        UserDto userDto = UserDto.builder()
                .name(username)
                .email(email)
                .password(oldPassword)
                .gender(gender).build();

        EditRequest request = new EditRequest(
                username, oldPassword, newPassword, gender, null
        );

        Mockito.when(userService.save(Mockito.any())).thenReturn(new User());

        Mockito.when(encoder.matches(Mockito.any(), Mockito.anyString())).thenReturn(true);

        Mockito.when(authenticationManager.authenticate(Mockito.any())).thenReturn(newAuthentication);

        Mockito.when(jwtUtils.generateToken(Mockito.any())).thenReturn(jwt);

        Assertions.assertEquals("jwt",
                authService.createNewJwtToken(request, userDto, authentication));
    }

    @Test
    @DisplayName("AuthServiceTest:createNewJwtTokenWhenNewPasswordIsNull()")
    void createNewJwtTokenWhenNewPasswordIsNull() throws EmailInUseException, PasswordNotValidException {
        String username = "testUsername";
        String email = "email@email.com";
        String otherEmail = "otherEmail@email.com";
        String gender = "male";
        String oldPassword = "oldPassword";
        String jwt = "jwt";

        UserDto userDto = UserDto.builder()
                .name(username)
                .email(email)
                .password(oldPassword)
                .gender(gender).build();

        EditRequest request = new EditRequest(
                username, oldPassword, null, gender, otherEmail
        );

        Mockito.when(userService.save(Mockito.any())).thenReturn(new User());
        Mockito.when(userService.existsByEmail(Mockito.any())).thenReturn(false);

        Mockito.when(encoder.matches(Mockito.any(), Mockito.anyString())).thenReturn(true);

        Mockito.when(authenticationManager.authenticate(Mockito.any())).thenReturn(newAuthentication);

        Mockito.when(jwtUtils.generateToken(Mockito.any())).thenReturn(jwt);

        Assertions.assertEquals("jwt",
                authService.createNewJwtToken(request, userDto, authentication));
    }

    @Test
    @DisplayName("AuthServiceTest:createNewJwtTokenThrowsPasswordNotValidException()")
    void createNewJwtTokenThrowsPasswordNotValidException() {
        UserDto userDto = UserDto.builder()
                .password("password").build();

        EditRequest request = new EditRequest(
                null, null, null, null, null
        );

        // PasswordNotValidException("No old password!") is thrown
        Assertions.assertThrows(PasswordNotValidException.class, () ->
                authService.createNewJwtToken(request, userDto, authentication));

        // PasswordNotValidException("Old password doesn't match!") is thrown
        request.setOldPassword("oldPassword");
        Assertions.assertThrows(PasswordNotValidException.class, () ->
                authService.createNewJwtToken(request, userDto, authentication));
    }

    @Test
    @DisplayName("AuthServiceTest:createNewJwtTokenThrowsEmailInUseException()")
    void createNewJwtTokenThrowsEmailInUseException() {
        String email = "email@email.com";
        String otherEmail = "otherEmail@email.com";
        String encodedPassword = "encodedPassword";
        String oldPassword = "oldPassword";
        String jwt = "jwt";

        UserDto userDto = UserDto.builder()
                .email(email)
                .password(encodedPassword).build();

        EditRequest request = new EditRequest(
                null, oldPassword, null, null, otherEmail
        );

        Mockito.when(encoder.matches(Mockito.any(), Mockito.anyString())).thenReturn(true);

        Mockito.when(userService.existsByEmail(Mockito.any())).thenReturn(true);

        Assertions.assertThrows(EmailInUseException.class, () ->
                authService.createNewJwtToken(request, userDto, authentication));
    }
}