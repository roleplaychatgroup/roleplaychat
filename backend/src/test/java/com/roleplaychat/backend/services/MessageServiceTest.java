package com.roleplaychat.backend.services;

import com.roleplaychat.backend.exceptions.CharacterNotFoundException;
import com.roleplaychat.backend.model.Group;
import com.roleplaychat.backend.model.Message;
import com.roleplaychat.backend.model.User;
import com.roleplaychat.backend.model.Character;
import com.roleplaychat.backend.repo.MessageRepository;
import com.roleplaychat.backend.request.SendMessageRequest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@ExtendWith(MockitoExtension.class)
class MessageServiceTest {
    private final Long groupId = 1L;

    private final SendMessageRequest request = new SendMessageRequest(
            "messageText", new Date(), 1L, 1L
    );

    @Mock private MessageRepository messageRepository;
    @Mock private UserService userService;
    @Mock private GroupService groupService;
    @Mock private CharacterService characterService;

    private MessageService messageService;

    @BeforeEach
    private void initMessageService() {
        messageService = new MessageService(
                messageRepository, userService,
                groupService, characterService
        );
    }

    @Test
    @DisplayName("MessageServiceTest:getAllMessagesByGroupId()")
    void getAllMessagesByGroupId() {
        User sender = new User(
                "senderName", "password",
                "male", "sender@email.com");
        Group group = new Group("groupName");
        Message firstMessage = new Message(
                "firstMessageText", new Date(), sender, group,
                1L, "characterName", "male"
        );

        Message secondMessage = new Message(
                "secondMessageText", new Date(), sender, group,
                2L, "characterName", "male"
        );

        List<Message> messages = new ArrayList<>();
        messages.add(firstMessage);
        messages.add(secondMessage);

        Mockito.when(messageRepository.findMessagesByGroup_GroupIdOrderBySentTime(groupId))
                .thenReturn(messages);

        ResponseEntity<?> response = messageService.getAllMessagesByGroupId(groupId);
        Assertions.assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    @DisplayName("MessageServiceTest:addNewMessageSentByUser()")
    void addNewMessageSentByUser() {
        SendMessageRequest messageRequest = new SendMessageRequest(
                "messageText", new Date(), 1L, null
        );

        User user = new User(
                "userName", "password",
                "male", "user@email.com");

        Group group = new Group("groupName");

        Mockito.when(userService.getUserById(Mockito.any())).thenReturn(Optional.of(user));

        Mockito.when(groupService.getGroupById(Mockito.any())).thenReturn(Optional.of(group));

        ResponseEntity<?> response = messageService.addNewMessage(messageRequest, groupId);
        Assertions.assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    @DisplayName("MessageServiceTest:addNewMessageSentByCharacter()")
    void addNewMessageSentByCharacter() throws CharacterNotFoundException {
        User user = new User(
                "userName", "password",
                "male", "user@email.com");

        Group group = new Group("groupName");

        Character character = new Character(
                "characterName", user, "male", "note", 10
        );

        Mockito.when(userService.getUserById(Mockito.any())).thenReturn(Optional.of(user));

        Mockito.when(groupService.getGroupById(Mockito.any())).thenReturn(Optional.of(group));

        Mockito.when(characterService.getCharacterById(1L)).thenReturn(character);

        ResponseEntity<?> response = messageService.addNewMessage(request, groupId);
        Assertions.assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    @DisplayName("MessageServiceTest:addNewMessageThrowsUserNotFoundException()")
    void addNewMessageThrowsUserNotFoundException() {
        Mockito.when(userService.getUserById(Mockito.any())).thenReturn(Optional.empty());

        ResponseEntity<?> response = messageService.addNewMessage(request, groupId);
        Assertions.assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    @Test
    @DisplayName("MessageServiceTest:addNewMessageThrowsGroupNotFoundException()")
    void addNewMessageThrowsGroupNotFoundException() {
        User user = new User(
                "userName", "password",
                "male", "user@email.com");

        Mockito.when(userService.getUserById(Mockito.any())).thenReturn(Optional.of(user));

        Mockito.when(groupService.getGroupById(Mockito.any())).thenReturn(Optional.empty());

        ResponseEntity<?> response = messageService.addNewMessage(request, groupId);
        Assertions.assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    @Test
    @DisplayName("MessageServiceTest:addNewMessageThrowsCharacterNotFoundException()")
    void addNewMessageThrowsCharacterNotFoundException() throws CharacterNotFoundException {
        User user = new User(
                "userName", "password",
                "male", "user@email.com");
        Group group = new Group("groupName");

        Mockito.when(userService.getUserById(Mockito.any())).thenReturn(Optional.of(user));

        Mockito.when(groupService.getGroupById(Mockito.any())).thenReturn(Optional.of(group));

        Mockito.when(characterService.getCharacterById(1L)).thenThrow(CharacterNotFoundException.class);

        ResponseEntity<?> response = messageService.addNewMessage(request, groupId);
        Assertions.assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }
}