package com.roleplaychat.backend.exceptions;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class UserNotFoundExceptionTest {

    @Test
    @DisplayName("UserNotFoundExceptionTest:constructorWithMessage()")
    void constructorWithMessage() {
        String exceptionMessage = "exceptionMessage";
        UserNotFoundException exception = new UserNotFoundException(exceptionMessage);
        Assertions.assertNotNull(exception);
        Assertions.assertEquals(exceptionMessage, exception.getMessage());
    }
}
