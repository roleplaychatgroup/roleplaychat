package com.roleplaychat.backend.exceptions;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class EmailInUseExceptionTest {

    @Test
    @DisplayName("EmailInUseExceptionTest:constructorWithMessage()")
    void constructorWithMessage() {
        String exceptionMessage = "exceptionMessage";
        EmailInUseException exception = new EmailInUseException(exceptionMessage);
        Assertions.assertNotNull(exception);
        Assertions.assertEquals(exceptionMessage, exception.getMessage());
    }
}