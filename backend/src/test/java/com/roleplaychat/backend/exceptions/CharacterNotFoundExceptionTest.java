package com.roleplaychat.backend.exceptions;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class CharacterNotFoundExceptionTest {

    @Test
    @DisplayName("CharacterNotFoundExceptionTest:constructorWithMessage()")
    void constructorWithMessage() {
        String exceptionMessage = "exceptionMessage";
        CharacterNotFoundException exception = new CharacterNotFoundException(exceptionMessage);
        Assertions.assertNotNull(exception);
        Assertions.assertEquals(exceptionMessage, exception.getMessage());
    }
}