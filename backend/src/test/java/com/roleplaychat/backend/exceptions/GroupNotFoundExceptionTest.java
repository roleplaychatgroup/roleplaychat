package com.roleplaychat.backend.exceptions;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class GroupNotFoundExceptionTest {

    @Test
    @DisplayName("GroupNotFoundExceptionTest:constructorWithMessage()")
    void constructorWithMessage() {
        String exceptionMessage = "exceptionMessage";
        GroupNotFoundException exception = new GroupNotFoundException(exceptionMessage);
        Assertions.assertNotNull(exception);
        Assertions.assertEquals(exceptionMessage, exception.getMessage());
    }
}