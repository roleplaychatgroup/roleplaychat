package com.roleplaychat.backend.exceptions;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class PasswordNotValidExceptionTest {

    @Test
    @DisplayName("PasswordNotValidExceptionTest:constructorWithMessage()")
    void constructorWithMessage() {
        String exceptionMessage = "exceptionMessage";
        PasswordNotValidException exception = new PasswordNotValidException(exceptionMessage);
        Assertions.assertNotNull(exception);
        Assertions.assertEquals(exceptionMessage, exception.getMessage());
    }
}