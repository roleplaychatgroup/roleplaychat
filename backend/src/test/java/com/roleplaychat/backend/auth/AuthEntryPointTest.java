package com.roleplaychat.backend.auth;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.AuthenticationException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@ExtendWith(MockitoExtension.class)
class AuthEntryPointTest {
    private AuthEntryPoint entryPoint = new AuthEntryPoint();

    @Mock private HttpServletRequest request;
    @Mock private HttpServletResponse response;
    @Mock private AuthenticationException authException;

    @Test
    @DisplayName("AuthEntryPointTest:commence()")
    void commence() throws IOException {
        Mockito.when(authException.getMessage()).thenReturn("Exception message");

        entryPoint.commence(request, response, authException);

        Mockito.verify(response, Mockito.times(1))
                .sendError(HttpServletResponse.SC_UNAUTHORIZED, authException.getMessage());
    }

    @Test
    @DisplayName("AuthEntryPointTest:commenceThrowsIOException()")
    void commenceThrowsIOException() throws IOException {
        Mockito.when(authException.getMessage()).thenReturn("Exception message");

        Mockito.doThrow(IOException.class).when(response)
                .sendError(HttpServletResponse.SC_UNAUTHORIZED, "Exception message");

        Assertions.assertThrows(IOException.class, () -> entryPoint.commence(request, response, authException));

        Mockito.verify(response, Mockito.times(1))
                .sendError(HttpServletResponse.SC_UNAUTHORIZED, authException.getMessage());
    }
}