package com.roleplaychat.backend.auth.payload.response;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class ErrorMessageResponseTest {
    private ErrorMessageResponse messageResponse;

    @BeforeEach
    private void initMessageResponse() {
        messageResponse = new ErrorMessageResponse("message");
    }

    @Test
    @DisplayName("ErrorMessageResponseTest:getMessage()")
    void getMessage() {
        String message = "message";
        Assertions.assertEquals(message, messageResponse.getMessage());
    }

    @Test
    @DisplayName("ErrorMessageResponseTest:setMessage()")
    void setMessage() {
        String newMessage = "newMessage";
        messageResponse.setMessage(newMessage);
        Assertions.assertEquals(newMessage, messageResponse.getMessage());
    }
}