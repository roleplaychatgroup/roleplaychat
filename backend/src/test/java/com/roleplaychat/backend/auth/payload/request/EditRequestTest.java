package com.roleplaychat.backend.auth.payload.request;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class EditRequestTest {
    private final String name = "name";
    private final String oldPassword = "oldPassword";
    private final String newPassword = "newPassword";
    private final String gender = "male";
    private final String email = "email@email.com";

    private EditRequest request;

    @BeforeEach
    private void initRequest() {
        request = new EditRequest(
                name,
                oldPassword,
                newPassword,
                gender,
                email
        );
    }

    @Test
    @DisplayName("EditRequestTest:getName()")
    void getName() {
        Assertions.assertEquals(name, request.getName());
    }

    @Test
    @DisplayName("EditRequestTest:getOldPassword()")
    void getOldPassword() {
        Assertions.assertEquals(oldPassword, request.getOldPassword());
    }

    @Test
    @DisplayName("EditRequestTest:getNewPassword()")
    void getNewPassword() {
        Assertions.assertEquals(newPassword, request.getNewPassword());
    }

    @Test
    @DisplayName("EditRequestTest:getGender()")
    void getGender() {
        Assertions.assertEquals(gender, request.getGender());
    }

    @Test
    @DisplayName("EditRequestTest:getEmail()")
    void getEmail() {
        Assertions.assertEquals(email, request.getEmail());
    }

    @Test
    @DisplayName("EditRequestTest:setName()")
    void setName() {
        Assertions.assertEquals(name, request.getName());
        String newName = "newName";
        request.setName(newName);
        Assertions.assertEquals(newName, request.getName());
    }

    @Test
    @DisplayName("EditRequestTest:setOldPassword()")
    void setOldPassword() {
        Assertions.assertEquals(oldPassword, request.getOldPassword());
        String newOldPassword = "newOldPassword";
        request.setOldPassword(newOldPassword);
        Assertions.assertEquals(newOldPassword, request.getOldPassword());
    }

    @Test
    @DisplayName("EditRequestTest:setNewPassword()")
    void setNewPassword() {
        Assertions.assertEquals(newPassword, request.getNewPassword());
        String newNewPassword = "newNewPassword";
        request.setNewPassword(newNewPassword);
        Assertions.assertEquals(newNewPassword, request.getNewPassword());
    }

    @Test
    @DisplayName("EditRequestTest:setGender()")
    void setGender() {
        Assertions.assertEquals(gender, request.getGender());
        String newGender = "female";
        request.setGender(newGender);
        Assertions.assertEquals(newGender, request.getGender());
    }

    @Test
    @DisplayName("EditRequestTest:setEmail()")
    void setEmail() {
        Assertions.assertEquals(email, request.getEmail());
        String newEmail = "new@email.com";
        request.setEmail(newEmail);
        Assertions.assertEquals(newEmail, request.getEmail());
    }
}