package com.roleplaychat.backend.auth.user_info;

import com.roleplaychat.backend.model.Role;
import com.roleplaychat.backend.model.RoleName;
import com.roleplaychat.backend.model.User;
import org.junit.jupiter.api.*;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;
import java.util.Objects;

class UserDetailsImplTest {
    private final Long id         = 1L;
    private final String name     = "Name";
    private final String email    = "email@email.com";
    private final String password = "password";
    private final String gender   = "male";
    private Collection<? extends GrantedAuthority> authorities;

    @Mock private User user = Mockito.mock(User.class);
    private UserDetailsImpl details;

    @BeforeEach
    private void initUserDetails() {
        details = new UserDetailsImpl(
                id,
                name,
                email,
                password,
                gender,
                authorities);
    }

    @Test
    @DisplayName("UserDetailsImplTest:build()")
    void build() {
        Mockito.when(user.getUserId())
                .thenReturn(id);
        Mockito.when(user.getName())
                .thenReturn(name);
        Mockito.when(user.getEmail())
                .thenReturn(email);
        Mockito.when(user.getPassword())
                .thenReturn(password);
        Mockito.when(user.getGender())
                .thenReturn(gender);
        Mockito.when(user.getRole())
                .thenReturn(new Role(1, RoleName.ROLE_USER));

        UserDetailsImpl details = UserDetailsImpl.build(user);

        Assertions.assertNotNull(details);
        Assertions.assertNotNull(details.getAuthorities());
        Assertions.assertEquals(id,       details.getId());
        Assertions.assertEquals(name,     details.getName());
        Assertions.assertEquals(email,    details.getEmail());
        Assertions.assertEquals(password, details.getPassword());
        Assertions.assertEquals(gender,   details.getGender());
    }

    @Test
    @DisplayName("UserDetailsImplTest:allArgsConstructor()")
    void allArgsConstructor() {
        Assertions.assertNotNull(details);
        Assertions.assertEquals(id,          details.getId());
        Assertions.assertEquals(name,        details.getName());
        Assertions.assertEquals(email,       details.getEmail());
        Assertions.assertEquals(password,    details.getPassword());
        Assertions.assertEquals(gender,      details.getGender());
        Assertions.assertEquals(authorities, details.getAuthorities());
    }

    @Test
    @DisplayName("UserDetailsImplTest:isAccountNonExpired()")
    void isAccountNonExpired() {
        Assertions.assertTrue(details.isAccountNonExpired());
    }

    @Test
    @DisplayName("UserDetailsImplTest:isAccountNonLocked()")
    void isAccountNonLocked() {
        Assertions.assertTrue(details.isAccountNonLocked());
    }

    @Test
    @DisplayName("UserDetailsImplTest:isCredentialsNonExpired()")
    void isCredentialsNonExpired() {
        Assertions.assertTrue(details.isCredentialsNonExpired());
    }

    @Test
    @DisplayName("UserDetailsImplTest:isEnabled()")
    void isEnabled() {
        Assertions.assertTrue(details.isEnabled());
    }

    @Test
    @DisplayName("UserDetailsImplTest:testEqualsTrue()")
    void testEqualsTrue() {
        UserDetailsImpl otherDetails = new UserDetailsImpl(
                id,
                name,
                email,
                password,
                gender,
                authorities);
        Assertions.assertEquals(details, otherDetails);
    }

    @Test
    @DisplayName("UserDetailsImplTest:testEqualsFalse()")
    void testEqualsFalse() {
        UserDetailsImpl otherDetails = new UserDetailsImpl(
                id + 1,
                name,
                email,
                password,
                gender,
                authorities);
        Assertions.assertNotEquals(details, otherDetails);
    }

    @Test
    @DisplayName("UserDetailsImplTest:testEqualsSame()")
    void testEqualsSame() {
        Assertions.assertEquals(details, details);
    }

    @Test
    @DisplayName("UserDetailsImplTest:testEqualsNull()")
    void testEqualsNull() {
        UserDetailsImpl otherDetails = null;
        Assertions.assertFalse(details.equals(otherDetails));
    }

    @Test
    @DisplayName("UserDetailsImplTest:testEqualsOtherClass()")
    void testEqualsOtherClass() {
        String otherDetails = "";
        Assertions.assertFalse(details.equals(otherDetails));
    }

    @Test
    @DisplayName("UserDetailsImplTest:testHashCode()")
    void testHashCode() {
        Assertions.assertEquals(
                Objects.hash(id, name, email),
                details.hashCode()
        );
    }

    @Test
    @DisplayName("UserDetailsImplTest:getId()")
    void getId() {
        Assertions.assertEquals(id, details.getId());
    }

    @Test
    @DisplayName("UserDetailsImplTest:getName()")
    void getName() {
        Assertions.assertEquals(name, details.getName());
    }

    @Test
    @DisplayName("UserDetailsImplTest:getGender()")
    void getGender() {
        Assertions.assertEquals(gender, details.getGender());
    }

    @Test
    @DisplayName("UserDetailsImplTest:getAuthorities()")
    void getAuthorities() {
        Assertions.assertEquals(authorities, details.getAuthorities());
    }

    @Test
    @DisplayName("UserDetailsImplTest:getPassword()")
    void getPassword() {
        Assertions.assertEquals(password, details.getPassword());
    }

    @Test
    @DisplayName("UserDetailsImplTest:getUsername()")
    void getUsername() {
        Assertions.assertEquals(email, details.getUsername());
    }

    @Test
    @DisplayName("UserDetailsImplTest:setId()")
    void setId() {
        Assertions.assertEquals(id, details.getId());
        Long newId = 2L;
        details.setId(newId);
        Assertions.assertEquals(newId, details.getId());
    }

    @Test
    @DisplayName("UserDetailsImplTest:setName()")
    void setName() {
        Assertions.assertEquals(name, details.getName());
        String newName = "newName";
        details.setName(newName);
        Assertions.assertEquals(newName, details.getName());
    }

    @Test
    @DisplayName("UserDetailsImplTest:setEmail()")
    void setEmail() {
        Assertions.assertEquals(email, details.getEmail());
        String newEmail = "new@email.com";
        details.setEmail(newEmail);
        Assertions.assertEquals(newEmail, details.getEmail());
    }

    @Test
    @DisplayName("UserDetailsImplTest:setPassword()")
    void setPassword() {
        Assertions.assertEquals(password, details.getPassword());
        String newPassword = "newPassword";
        details.setPassword(newPassword);
        Assertions.assertEquals(newPassword, details.getPassword());
    }

    @Test
    @DisplayName("UserDetailsImplTest:setGender()")
    void setGender() {
        Assertions.assertEquals(gender, details.getGender());
        String newGender = "female";
        details.setGender(newGender);
        Assertions.assertEquals(newGender, details.getGender());
    }
}