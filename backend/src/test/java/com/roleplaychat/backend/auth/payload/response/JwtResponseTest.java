package com.roleplaychat.backend.auth.payload.response;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

class JwtResponseTest {
    private JwtResponse jwtResponse;

    @BeforeEach
    private void initJwtResponse() {
        List<String> roles = new ArrayList<>();
        roles.add("ROLE_USER");

        jwtResponse = new JwtResponse(
                "accessToken",
                1L,
                "Name",
                "male",
                "email@email.com",
                roles
        );
    }

    @Test
    @DisplayName("JwtResponseTest:getToken()")
    void getToken() {
        Assertions.assertEquals("accessToken", jwtResponse.getToken());
    }

    @Test
    @DisplayName("JwtResponseTest:getId()")
    void getId() {
        Assertions.assertEquals(1L, jwtResponse.getId());
    }

    @Test
    @DisplayName("JwtResponseTest:getName()")
    void getName() {
        Assertions.assertEquals("Name", jwtResponse.getName());
    }

    @Test
    @DisplayName("JwtResponseTest:getGender()")
    void getGender() {
        Assertions.assertEquals("male", jwtResponse.getGender());
    }

    @Test
    @DisplayName("JwtResponseTest:getEmail()")
    void getEmail() {
        Assertions.assertEquals("email@email.com", jwtResponse.getEmail());
    }

    @Test
    @DisplayName("JwtResponseTest:getRoles()")
    void getRoles() {
        List<String> roles = new ArrayList<>();
        roles.add("ROLE_USER");

        Assertions.assertEquals(roles, jwtResponse.getRoles());
    }

    @Test
    @DisplayName("JwtResponseTest:setToken()")
    void setToken() {
        String newAccessToken = "newAccessToken";
        jwtResponse.setToken(newAccessToken);
        Assertions.assertEquals(newAccessToken, jwtResponse.getToken());
    }

    @Test
    @DisplayName("JwtResponseTest:setId()")
    void setId() {
        Long newId = 2L;
        jwtResponse.setId(newId);
        Assertions.assertEquals(newId, jwtResponse.getId());
    }

    @Test
    @DisplayName("JwtResponseTest:setName()")
    void setName() {
        String newName = "NewName";
        jwtResponse.setName(newName);
        Assertions.assertEquals(newName, jwtResponse.getName());
    }

    @Test
    @DisplayName("JwtResponseTest:setGender()")
    void setGender() {
        String newGender = "female";
        jwtResponse.setGender(newGender);
        Assertions.assertEquals(newGender, jwtResponse.getGender());
    }

    @Test
    @DisplayName("JwtResponseTest:setEmail()")
    void setEmail() {
        String newEmail = "new@email.com";
        jwtResponse.setEmail(newEmail);
        Assertions.assertEquals(newEmail, jwtResponse.getEmail());
    }

    @Test
    @DisplayName("JwtResponseTest:setRoles()")
    void setRoles() {
        List<String> newRoles = new ArrayList<>();
        newRoles.add("ROLE_ADMIN");
        jwtResponse.setRoles(newRoles);
        Assertions.assertEquals(newRoles, jwtResponse.getRoles());
    }
}