package com.roleplaychat.backend.auth.payload.request;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.Set;

class SignupRequestTest {
    private String username;
    private String email;
    private String gender;
    private String password;
    private Set<String> role;

    @BeforeEach
    private void setDefaultData() {
        this.username = "username";
        this.email = "email@email.com";
        this.gender = "male";
        this.password = "password";
        this.role = new HashSet<>();
        role.add("ROLE_USER");
    }

    @Test
    @DisplayName("SignupRequestTest:getUsername()")
    void getUsername() {
        SignupRequest signupRequest = SignupRequest.builder()
                .username(username).build();
        Assertions.assertEquals(username, signupRequest.getUsername());
    }

    @Test
    @DisplayName("SignupRequestTest:getEmail()")
    void getEmail() {
        SignupRequest signupRequest = SignupRequest.builder()
                .email(email).build();
        Assertions.assertEquals(email, signupRequest.getEmail());
    }

    @Test
    @DisplayName("SignupRequestTest:getGender()")
    void getGender() {
        SignupRequest signupRequest = SignupRequest.builder()
                .gender(gender).build();
        Assertions.assertEquals(gender, signupRequest.getGender());
    }

    @Test
    @DisplayName("SignupRequestTest:getPassword()")
    void getPassword() {
        SignupRequest signupRequest = SignupRequest.builder()
                .password(password).build();
        Assertions.assertEquals(password, signupRequest.getPassword());
    }

    @Test
    @DisplayName("SignupRequestTest:getRole()")
    void getRole() {
        SignupRequest signupRequest = SignupRequest.builder()
                .role(role).build();
        Assertions.assertEquals(role, signupRequest.getRole());
    }

    @Test
    @DisplayName("SignupRequestTest:setUsername()")
    void setUsername() {
        String newUsername = "newUsername";
        SignupRequest signupRequest = SignupRequest.builder()
                .username(username).build();
        signupRequest.setUsername(newUsername);
        Assertions.assertEquals(newUsername, signupRequest.getUsername());
    }

    @Test
    @DisplayName("SignupRequestTest:setEmail()")
    void setEmail() {
        String newEmail = "newEmail@email.com";
        SignupRequest signupRequest = SignupRequest.builder()
                .email(email).build();
        signupRequest.setEmail(newEmail);
        Assertions.assertEquals(newEmail, signupRequest.getEmail());
    }

    @Test
    @DisplayName("SignupRequestTest:setGender()")
    void setGender() {
        String newGender = "female";
        SignupRequest signupRequest = SignupRequest.builder()
                .gender(gender).build();
        signupRequest.setGender(newGender);
        Assertions.assertEquals(newGender, signupRequest.getGender());
    }

    @Test
    @DisplayName("SignupRequestTest:setPassword()")
    void setPassword() {
        String newPassword = "newPassword";
        SignupRequest signupRequest = SignupRequest.builder()
                .password(password).build();
        signupRequest.setPassword(newPassword);
        Assertions.assertEquals(newPassword, signupRequest.getPassword());
    }

    @Test
    @DisplayName("SignupRequestTest:setRole()")
    void setRole() {
        Set<String> newRole = new HashSet<>();
        newRole.add("ROLE_ADMIN");
        SignupRequest signupRequest = SignupRequest.builder()
                .role(role).build();

        signupRequest.setRole(newRole);
        Assertions.assertEquals(newRole, signupRequest.getRole());
    }

    @Test
    @DisplayName("SignupRequestTest:toStringTest()")
    void toStringTest() {
        SignupRequest.SignupRequestBuilder signupRequestBuilder
                = SignupRequest.builder()
                .username(username)
                .email(email)
                .gender(gender)
                .password(password)
                .role(role);

        Assertions.assertEquals("SignupRequest.SignupRequestBuilder(username=" + username + ", email=" + email + ", gender=male, password=" + password + ", role=[ROLE_USER])",
                signupRequestBuilder.toString());

        SignupRequest signupRequest = signupRequestBuilder.build();
        Assertions.assertEquals(
                "SignupRequest{username='" + username + "', email='" + email + "', gender='" + gender + "', password='" + password + "', role=[ROLE_USER]}",
                signupRequest.toString());
    }
}