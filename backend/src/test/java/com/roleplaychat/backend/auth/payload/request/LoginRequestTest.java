package com.roleplaychat.backend.auth.payload.request;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class LoginRequestTest {
    private String email;
    private String password;

    @BeforeEach
    private void setDefaultData() {
        this.email = "email@email.com";
        this.password = "password";
    }

    @Test
    @DisplayName("LoginRequestTest:getEmail()")
    void getEmail() {
        LoginRequest request = LoginRequest.builder()
                .email(email).build();
        Assertions.assertEquals(email, request.getEmail());
    }

    @Test
    @DisplayName("LoginRequestTest:getPassword()")
    void getPassword() {
        LoginRequest request = LoginRequest.builder()
                .password(password).build();
        Assertions.assertEquals(password, request.getPassword());
    }

    @Test
    @DisplayName("LoginRequestTest:setEmail()")
    void setEmail() {
        LoginRequest request = new LoginRequest(email, password);

        String newEmail = "newEmail@email.com";
        request.setEmail(newEmail);
        Assertions.assertEquals(newEmail, request.getEmail());
    }

    @Test
    @DisplayName("LoginRequestTest:setPassword()")
    void setPassword() {
        LoginRequest request = new LoginRequest(email, password);

        String newPassword = "newPassword";
        request.setPassword(newPassword);
        Assertions.assertEquals(newPassword, request.getPassword());
    }

    @Test
    @DisplayName("LoginRequestTest:toStringTest()")
    void toStringTest() {
        LoginRequest.LoginRequestBuilder loginRequestBuilder =
                LoginRequest.builder()
                .email(email)
                .password(password);

        Assertions.assertEquals("LoginRequest.LoginRequestBuilder(email=" + email + ", password=" + password + ")",
                loginRequestBuilder.toString());

        LoginRequest loginRequest = loginRequestBuilder.build();

        Assertions.assertEquals(
                "LoginRequest{email='" + email + "', password='" + password + "'}",
                loginRequest.toString());
    }
}