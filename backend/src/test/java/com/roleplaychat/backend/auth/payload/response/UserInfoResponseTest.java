package com.roleplaychat.backend.auth.payload.response;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class UserInfoResponseTest {
    private final String name = "name";
    private final String email = "email@email.com";
    private final String gender = "male";

    private UserInfoResponse response;

    @BeforeEach
    private void initUserInfoResponse() {
        response = new UserInfoResponse(
                name, email, gender
        );
    }

    @Test
    @DisplayName("UserInfoResponseTest:getName()")
    void getName() {
        Assertions.assertEquals(name, response.getName());
    }

    @Test
    @DisplayName("UserInfoResponseTest:getEmail()")
    void getEmail() {
        Assertions.assertEquals(email, response.getEmail());
    }

    @Test
    @DisplayName("UserInfoResponseTest:getGender()")
    void getGender() {
        Assertions.assertEquals(gender, response.getGender());
    }

    @Test
    @DisplayName("UserInfoResponseTest:setName()")
    void setName() {
        Assertions.assertEquals(name, response.getName());
        String newName = "newName";
        response.setName(newName);
        Assertions.assertEquals(newName, response.getName());
    }

    @Test
    @DisplayName("UserInfoResponseTest:setEmail()")
    void setEmail() {
        Assertions.assertEquals(email, response.getEmail());
        String newEmail = "new@email.com";
        response.setEmail(newEmail);
        Assertions.assertEquals(newEmail, response.getEmail());
    }

    @Test
    @DisplayName("UserInfoResponseTest:setGender()")
    void setGender() {
        Assertions.assertEquals(gender, response.getGender());
        String newGender = "female";
        response.setGender(newGender);
        Assertions.assertEquals(newGender, response.getGender());
    }
}