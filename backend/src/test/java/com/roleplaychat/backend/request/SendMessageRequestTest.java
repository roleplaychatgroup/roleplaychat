package com.roleplaychat.backend.request;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Date;

class SendMessageRequestTest {
    private final String text = "messageText";
    private final Date sendTime = new Date();
    private final Long userId = 1L;
    private final Long characterId = 1L;
    private SendMessageRequest request;

    @BeforeEach
    private void initSendMessageRequest() {
        request = new SendMessageRequest(
                text, sendTime, userId, characterId
        );
    }

    @Test
    @DisplayName("SendMessageRequestTest:getText()")
    void getText() {
        Assertions.assertEquals(text, request.getText());
    }

    @Test
    @DisplayName("SendMessageRequestTest:getSendTime()")
    void getSendTime() {
        Assertions.assertEquals(sendTime, request.getSendTime());
    }

    @Test
    @DisplayName("SendMessageRequestTest:getUserId()")
    void getUserId() {
        Assertions.assertEquals(userId, request.getUserId());
    }

    @Test
    @DisplayName("SendMessageRequestTest:getCharacterId()")
    void getCharacterId() {
        Assertions.assertEquals(characterId, request.getCharacterId());
    }

    @Test
    @DisplayName("SendMessageRequestTest:setText()")
    void setText() {
        Assertions.assertEquals(text, request.getText());
        String newText = "newMessageText";
        request.setText(newText);
        Assertions.assertEquals(newText, request.getText());
    }

    @Test
    @DisplayName("SendMessageRequestTest:setSendTime()")
    void setSendTime() {
        Assertions.assertEquals(sendTime, request.getSendTime());
        Date newDate = new Date();
        request.setSendTime(newDate);
        Assertions.assertEquals(newDate, request.getSendTime());
    }

    @Test
    @DisplayName("SendMessageRequestTest:setUserId()")
    void setUserId() {
        Assertions.assertEquals(userId, request.getUserId());
        Long newUserId = 2L;
        request.setUserId(newUserId);
        Assertions.assertEquals(newUserId, request.getUserId());
    }

    @Test
    @DisplayName("SendMessageRequestTest:setCharacterId()")
    void setCharacterId() {
        Assertions.assertEquals(characterId, request.getCharacterId());
        Long newCharacterId = 2L;
        request.setCharacterId(newCharacterId);
        Assertions.assertEquals(newCharacterId, request.getCharacterId());
    }
}