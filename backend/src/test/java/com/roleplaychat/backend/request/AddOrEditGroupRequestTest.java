package com.roleplaychat.backend.request;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

class AddOrEditGroupRequestTest {
    private List<String> userList;
    private final String groupName = "testGroupName";
    private final Long adminId = 1L;
    private AddOrEditGroupRequest request;

    @BeforeEach
    private void initRequest() {
        userList = new ArrayList<>();
        userList.add("1");
        userList.add("2");

        request = new AddOrEditGroupRequest(userList, groupName, adminId);
    }

    @Test
    @DisplayName("AddOrEditGroupRequestTest:getUserIds()")
    void getUserList() {
        Assertions.assertEquals(userList, request.getUserList());
    }

    @Test
    @DisplayName("AddOrEditGroupRequestTest:getGroupName()")
    void getGroupName() {
        Assertions.assertEquals(groupName, request.getGroupName());
    }

    @Test
    @DisplayName("AddOrEditGroupRequestTest:getAdminId()")
    void getAdminId() {
        Assertions.assertEquals(adminId, request.getAdminId());
    }

    @Test
    @DisplayName("AddOrEditGroupRequestTest:setUserIds()")
    void setUserList() {
        List<String> newUserIds = new ArrayList<>();
        newUserIds.add("2");
        newUserIds.add("3");

        request.setUserList(newUserIds);
        Assertions.assertEquals(newUserIds, request.getUserList());
    }

    @Test
    @DisplayName("AddOrEditGroupRequestTest:setGroupName()")
    void setGroupName() {
        String newGroupName = "newGroupName";
        request.setGroupName(newGroupName);
        Assertions.assertEquals(newGroupName, request.getGroupName());
    }

    @Test
    @DisplayName("AddOrEditGroupRequestTest:setAdminId()")
    void setAdminId() {
        Long newAdminId = 2L;
        request.setAdminId(newAdminId);
        Assertions.assertEquals(newAdminId, request.getAdminId());
    }
}
