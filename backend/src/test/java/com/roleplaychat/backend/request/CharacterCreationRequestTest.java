package com.roleplaychat.backend.request;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class CharacterCreationRequestTest {
    private final String characterName = "characterName";
    private final String characterGender = "male";

    private CharacterCreationRequest request;

    @BeforeEach
    private void initCharacterCreationRequest() {
        request = new CharacterCreationRequest(
                characterName, characterGender
        );
    }

    @Test
    @DisplayName("CharacterCreationRequestTest:getCharacterName()")
    void getCharacterName() {
        Assertions.assertEquals(characterName, request.getCharacterName());
    }

    @Test
    @DisplayName("CharacterCreationRequestTest:getCharacterGender()")
    void getCharacterGender() {
        Assertions.assertEquals(characterGender, request.getCharacterGender());
    }

    @Test
    @DisplayName("CharacterCreationRequestTest:setCharacterName()")
    void setCharacterName() {
        Assertions.assertEquals(characterName, request.getCharacterName());
        String newCharacterName = "newCharacterName";
        request.setCharacterName(newCharacterName);
        Assertions.assertEquals(newCharacterName, request.getCharacterName());
    }

    @Test
    @DisplayName("CharacterCreationRequestTest:setCharacterGender()")
    void setCharacterGender() {
        Assertions.assertEquals(characterGender, request.getCharacterGender());
        String newGender = "female";
        request.setCharacterGender(newGender);
        Assertions.assertEquals(newGender, request.getCharacterGender());
    }
}