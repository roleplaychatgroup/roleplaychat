package com.roleplaychat.backend.dto;

import com.roleplaychat.backend.model.Character;
import com.roleplaychat.backend.model.User;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
class CharacterDtoConverterTest {
    private Character character;
    private CharacterDto dto;

    @Mock private User user = Mockito.mock(User.class);
    @Mock private UserDto userDto = Mockito.mock(UserDto.class);

    private CharacterDtoConverter converter;

    @BeforeEach
    private void initDtoConverter() {
        converter = new CharacterDtoConverter();
    }

    @BeforeEach
    private void initUserMocks() {
        Mockito.when(user.getName()).thenReturn("Name");
        Mockito.when(userDto.getName()).thenReturn("Name");
    }

    @Test
    @DisplayName("CharacterDtoConverterTest:convertToDto()")
    void convertToDto() {
        character = Character.builder()
                .name("name")
                .user(user)
                .gender("male")
                .note("note").build();

        dto = converter.convertToDto(character);

        Assertions.assertNotNull(dto);
        Assertions.assertEquals(character.getName(), dto.getName());
        Assertions.assertEquals(character.getGender(), dto.getGender());
        Assertions.assertEquals(character.getNote(), dto.getNote());
        Assertions.assertEquals(
                character.getUser().getName(),
                dto.getUser().getName());
    }

    @Test
    @DisplayName("CharacterDtoConverterTest:convertFromDto()")
    void convertFromDto() {
        dto = CharacterDto.builder()
                .name("name")
                .user(userDto)
                .gender("male")
                .note("note").build();

        character = converter.convertFromDto(dto);
        Assertions.assertNotNull(dto);
        Assertions.assertEquals(dto.getName(), character.getName());
        Assertions.assertEquals(dto.getGender(), character.getGender());
        Assertions.assertEquals(dto.getNote(), character.getNote());
        Assertions.assertEquals(
                dto.getUser().getName(),
                character.getUser().getName());
    }
}