package com.roleplaychat.backend.dto;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Objects;

@RunWith(MockitoJUnitRunner.class)
class UserDtoTest {
    private final Long   userId   = 1L;
    private final String name     = "Name";
    private final String email    = "email@email.com";
    private final String password = "password";
    private final String gender   = "male";

    @Mock private RoleDto role    = Mockito.mock(RoleDto.class);
    @Mock private RoleDto newRole = Mockito.mock(RoleDto.class);
    private UserDto dto;

    @BeforeEach
    private void initDto() {
        dto = new UserDto(
                userId,
                name,
                email,
                password,
                gender,
                role
        );
    }

    @Test
    @DisplayName("UserDtoTest:builder()")
    void builder() {
        UserDto dto = UserDto.builder()
                .userId(userId)
                .name(name)
                .email(email)
                .password(password)
                .gender(gender)
                .role(role).build();

        Assertions.assertNotNull(dto);
        Assertions.assertEquals(userId,   dto.getUserId());
        Assertions.assertEquals(name,     dto.getName());
        Assertions.assertEquals(email,    dto.getEmail());
        Assertions.assertEquals(password, dto.getPassword());
        Assertions.assertEquals(gender,   dto.getGender());
        Assertions.assertEquals(role,     dto.getRole());
    }

    @Test
    @DisplayName("UserDtoTest:builderToString()")
    void builderToString() {
        UserDto.UserDtoBuilder builder = UserDto.builder()
                .userId(userId)
                .name(name)
                .email(email)
                .password(password)
                .gender(gender)
                .role(role);
        Assertions.assertEquals(
                "UserDto.UserDtoBuilder(userId=" + userId + ", " +
                        "name=" + name + ", " +
                        "email=" + email + ", " +
                        "password=" + password + ", " +
                        "gender=" + gender + ", " +
                        "role=" + role + ")",
                builder.toString());
    }

    @Test
    @DisplayName("UserDtoTest:allArgsConstructor()")
    void allArgsConstructor() {
        Assertions.assertNotNull(dto);
        Assertions.assertEquals(userId,   dto.getUserId());
        Assertions.assertEquals(name,     dto.getName());
        Assertions.assertEquals(email,    dto.getEmail());
        Assertions.assertEquals(password, dto.getPassword());
        Assertions.assertEquals(gender,   dto.getGender());
        Assertions.assertEquals(role,     dto.getRole());
    }

    @Test
    @DisplayName("UserDtoTest:noArgsConstructor()")
    void noArgsConstructor() {
        dto = new UserDto();
        Assertions.assertNotNull(dto);
        Assertions.assertNull(dto.getUserId());
        Assertions.assertNull(dto.getName());
        Assertions.assertNull(dto.getEmail());
        Assertions.assertNull(dto.getPassword());
        Assertions.assertNull(dto.getGender());
        Assertions.assertNull(dto.getRole());
    }

    @Test
    @DisplayName("UserDtoTest:testEqualsTrue()")
    void testEqualsTrue() {
        UserDto dto1 = UserDto.builder()
                .userId(userId)
                .name(name)
                .email(email).build();
        UserDto dto2 = UserDto.builder()
                .userId(userId)
                .name(name)
                .email(email).build();
        Assertions.assertEquals(dto1, dto2);
    }

    @Test
    @DisplayName("UserDtoTest:testEqualsDifferentUserId()")
    void testEqualsDifferentUserId() {
        UserDto dto1 = UserDto.builder()
                .userId(userId)
                .name(name)
                .email(email).build();
        UserDto dto2 = UserDto.builder()
                .userId(userId + 1)
                .name(name)
                .email(email).build();
        Assertions.assertNotEquals(dto1, dto2);
    }

    @Test
    @DisplayName("UserDtoTest:testEqualsDifferentName()")
    void testEqualsDifferentName() {
        UserDto dto1 = UserDto.builder()
                .userId(userId)
                .name(name)
                .email(email).build();
        UserDto dto2 = UserDto.builder()
                .userId(userId)
                .name("differentName")
                .email(email).build();
        Assertions.assertNotEquals(dto1, dto2);
    }

    @Test
    @DisplayName("UserDtoTest:testEqualsDifferentEmail()")
    void testEqualsDifferentEmail() {
        UserDto dto1 = UserDto.builder()
                .userId(userId)
                .name(name)
                .email(email).build();
        UserDto dto2 = UserDto.builder()
                .userId(userId)
                .name(name)
                .email("different@email.com").build();
        Assertions.assertNotEquals(dto1, dto2);
    }

    @Test
    @DisplayName("UserDtoTest:testEqualsSameUserId()")
    void testEqualsSameUserId() {
        UserDto dto1 = UserDto.builder()
                .userId(userId)
                .name(name)
                .email(email).build();
        UserDto dto2 = UserDto.builder()
                .userId(userId)
                .name("differentName")
                .email("different@email.com").build();
        Assertions.assertNotEquals(dto1, dto2);
    }

    @Test
    @DisplayName("UserDtoTest:testEqualsSameName()")
    void testEqualsSameName() {
        UserDto dto1 = UserDto.builder()
                .userId(userId)
                .name(name)
                .email(email).build();
        UserDto dto2 = UserDto.builder()
                .userId(userId + 1)
                .name(name)
                .email("different@email.com").build();
        Assertions.assertNotEquals(dto1, dto2);
    }

    @Test
    @DisplayName("UserDtoTest:testEqualsSameEmail()")
    void testEqualsSameEmail() {
        UserDto dto1 = UserDto.builder()
                .userId(userId)
                .name(name)
                .email(email).build();
        UserDto dto2 = UserDto.builder()
                .userId(userId + 1)
                .name("differentName")
                .email(email).build();
        Assertions.assertNotEquals(dto1, dto2);
    }

    @Test
    @DisplayName("UserDtoTest:testEqualsFalse()")
    void testEqualsFalse() {
        UserDto dto1 = UserDto.builder()
                .userId(userId)
                .name(name)
                .email(email).build();
        UserDto dto2 = UserDto.builder()
                .userId(userId + 1)
                .name("differentName")
                .email("different@email.com").build();
        Assertions.assertNotEquals(dto1, dto2);
    }

    @Test
    @DisplayName("UserDtoTest:testEqualsSame()")
    void testEqualsSame() {
        UserDto dto = new UserDto();
        Assertions.assertEquals(dto, dto);
    }

    @Test
    @DisplayName("UserDtoTest:testEqualsNull()")
    void testEqualsNull() {
        UserDto dto1 = new UserDto();
        UserDto dto2 = null;
        Assertions.assertNotEquals(dto1, dto2);
    }

    @Test
    @DisplayName("UserDtoTest:testEqualsOtherClass()")
    void testEqualsOtherClass() {
        UserDto dto1 = new UserDto();
        String dto2 = "";
        Assertions.assertNotEquals(dto1, dto2);
    }

    @Test
    @DisplayName("UserDtoTest:testHashCode()")
    void testHashCode() {
        dto = new UserDto();
        int hash = Objects.hash(null, null, null);
        Assertions.assertEquals(hash, dto.hashCode());

        dto = UserDto.builder()
                .userId(userId)
                .name(name)
                .email(email).build();
        hash = Objects.hash(userId, name, email);
        Assertions.assertEquals(hash, dto.hashCode());
    }

    @Test
    @DisplayName("UserDtoTest:testToString()")
    void testToString() {
        Assertions.assertEquals(
                "UserDto(userId=" + userId + ", " +
                        "name=" + name + ", " +
                        "email=" + email + ", " +
                        "password=" + password + ", " +
                        "gender=" + gender + ", " +
                        "role=" + role + ")",
                dto.toString()
        );
    }

    @Test
    @DisplayName("UserDtoTest:getUserId()")
    void getUserId() {
        Assertions.assertEquals(userId, dto.getUserId());
    }

    @Test
    @DisplayName("UserDtoTest:getName()")
    void getName() {
        Assertions.assertEquals(name, dto.getName());
    }

    @Test
    @DisplayName("UserDtoTest:getEmail()")
    void getEmail() {
        Assertions.assertEquals(email, dto.getEmail());
    }

    @Test
    @DisplayName("UserDtoTest:getPassword()")
    void getPassword() {
        Assertions.assertEquals(password, dto.getPassword());
    }

    @Test
    @DisplayName("UserDtoTest:getGender()")
    void getGender() {
        Assertions.assertEquals(gender, dto.getGender());
    }

    @Test
    @DisplayName("UserDtoTest:getRole()")
    void getRole() {
        Assertions.assertEquals(role, dto.getRole());
    }

    @Test
    @DisplayName("UserDtoTest:setUserId()")
    void setUserId() {
        Assertions.assertEquals(userId, dto.getUserId());
        Long newUserId = 2L;
        dto.setUserId(newUserId);
        Assertions.assertEquals(newUserId, dto.getUserId());
    }

    @Test
    @DisplayName("UserDtoTest:setName()")
    void setName() {
        Assertions.assertEquals(name, dto.getName());
        String newName = "NewName";
        dto.setName(newName);
        Assertions.assertEquals(newName, dto.getName());
    }

    @Test
    @DisplayName("UserDtoTest:setEmail()")
    void setEmail() {
        Assertions.assertEquals(email, dto.getEmail());
        String newEmail = "new@email.com";
        dto.setEmail(newEmail);
        Assertions.assertEquals(newEmail, dto.getEmail());
    }

    @Test
    @DisplayName("UserDtoTest:setPassword()")
    void setPassword() {
        Assertions.assertEquals(password, dto.getPassword());
        String newPassword = "newPassword";
        dto.setPassword(newPassword);
        Assertions.assertEquals(newPassword, dto.getPassword());
    }

    @Test
    @DisplayName("UserDtoTest:setGender()")
    void setGender() {
        Assertions.assertEquals(gender, dto.getGender());
        String newGender = "female";
        dto.setGender(newGender);
        Assertions.assertEquals(newGender, dto.getGender());
    }

    @Test
    @DisplayName("UserDtoTest:setRole()")
    void setRole() {
        Assertions.assertEquals(role, dto.getRole());
        dto.setRole(newRole);
        Assertions.assertEquals(newRole, dto.getRole());
    }
}