package com.roleplaychat.backend.dto;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Objects;

@RunWith(MockitoJUnitRunner.class)
class CharacterDtoTest {
    private final Long characterId = 1L;
    private final String name      = "name";
    private final String gender    = "male";
    private final String note      = "note";
    private final long lifeCount   = 10L;
    @Mock private UserDto user = Mockito.mock(UserDto.class);
    @Mock private UserDto differentUser = Mockito.mock(UserDto.class);

    private CharacterDto dto;

    @BeforeEach
    private void initDto() {
        dto = new CharacterDto(
                characterId,
                name,
                gender,
                note,
                user,
                lifeCount
        );
    }

    @Test
    @DisplayName("CharacterDtoTest:builder()")
    void builder() {
        CharacterDto dto = CharacterDto.builder()
                .characterId(characterId)
                .name(name)
                .gender(gender)
                .note(note)
                .user(user)
                .lifeCount(lifeCount).build();

        Assertions.assertNotNull(dto);
        Assertions.assertEquals(characterId, dto.getCharacterId());
        Assertions.assertEquals(name, dto.getName());
        Assertions.assertEquals(gender, dto.getGender());
        Assertions.assertEquals(note, dto.getNote());
        Assertions.assertEquals(user, dto.getUser());
        Assertions.assertEquals(lifeCount, dto.getLifeCount());
    }

    @Test
    @DisplayName("CharacterDtoTest:builderToString()")
    void builderToString() {
        CharacterDto.CharacterDtoBuilder builder = CharacterDto.builder()
                .characterId(characterId)
                .name(name)
                .gender(gender)
                .note(note)
                .user(user)
                .lifeCount(lifeCount);

        Assertions.assertEquals(
                "CharacterDto.CharacterDtoBuilder(characterId=" + characterId + ", " +
                        "name=" + name + ", " +
                        "gender=" + gender + ", " +
                        "note=" + note + ", " +
                        "user=" + user + ", " +
                        "lifeCount=" + lifeCount + ")",
                builder.toString());
    }

    @Test
    @DisplayName("CharacterDtoTest:allArgsConstructor()")
    void allArgsConstructor() {
        Assertions.assertNotNull(dto);
        Assertions.assertEquals(characterId, dto.getCharacterId());
        Assertions.assertEquals(name, dto.getName());
        Assertions.assertEquals(gender, dto.getGender());
        Assertions.assertEquals(note, dto.getNote());
        Assertions.assertEquals(user, dto.getUser());
        Assertions.assertEquals(lifeCount, dto.getLifeCount());
    }

    @Test
    @DisplayName("CharacterDtoTest:noArgsConstructor()")
    void noArgsConstructor() {
        dto = new CharacterDto();
        Assertions.assertNotNull(dto);
        Assertions.assertNull(dto.getCharacterId());
        Assertions.assertNull(dto.getName());
        Assertions.assertNull(dto.getGender());
        Assertions.assertNull(dto.getNote());
        Assertions.assertNull(dto.getUser());
        Assertions.assertEquals(0, dto.getLifeCount());
    }

    @Test
    @DisplayName("CharacterDtoTest:testEqualsTrue()")
    void testEqualsTrue() {
        CharacterDto otherDto = CharacterDto.builder()
                .characterId(characterId)
                .name(name)
                .user(user).build();
        Assertions.assertEquals(dto, otherDto);
    }

    @Test
    @DisplayName("CharacterDtoTest:testEqualsDifferentId()")
    void testEqualsDifferentId() {
        CharacterDto otherDto = CharacterDto.builder()
                .characterId(characterId + 1)
                .name(name)
                .user(user).build();
        Assertions.assertNotEquals(dto, otherDto);
    }

    @Test
    @DisplayName("CharacterDtoTest:testEqualsDifferentName()")
    void testEqualsDifferentName() {
        CharacterDto otherDto = CharacterDto.builder()
                .characterId(characterId)
                .name("differentName")
                .user(user).build();
        Assertions.assertNotEquals(dto, otherDto);
    }

    @Test
    @DisplayName("CharacterDtoTest:testEqualsDifferentUser()")
    void testEqualsDifferentUser() {
        CharacterDto otherDto = CharacterDto.builder()
                .characterId(characterId)
                .name(name)
                .user(differentUser).build();
        Assertions.assertNotEquals(dto, otherDto);
    }

    @Test
    @DisplayName("CharacterDtoTest:testEqualsSameId()")
    void testEqualsSameId() {
        CharacterDto otherDto = CharacterDto.builder()
                .characterId(characterId)
                .name("differentName")
                .user(differentUser).build();
        Assertions.assertNotEquals(dto, otherDto);
    }

    @Test
    @DisplayName("CharacterDtoTest:testEqualsSameName()")
    void testEqualsSameName() {
        CharacterDto otherDto = CharacterDto.builder()
                .characterId(characterId + 1)
                .name(name)
                .user(differentUser).build();
        Assertions.assertNotEquals(dto, otherDto);
    }

    @Test
    @DisplayName("CharacterDtoTest:testEqualsSameUser()")
    void testEqualsSameUser() {
        CharacterDto otherDto = CharacterDto.builder()
                .characterId(characterId + 1)
                .name("differentName")
                .user(user).build();
        Assertions.assertNotEquals(dto, otherDto);
    }

    @Test
    @DisplayName("CharacterDtoTest:testEqualsFalse()")
    void testEqualsFalse() {
        CharacterDto otherDto = CharacterDto.builder()
                .characterId(characterId + 1)
                .name("differentName")
                .user(differentUser).build();
        Assertions.assertNotEquals(dto, otherDto);
    }

    @Test
    @DisplayName("CharacterDtoTest:testEqualsSame()")
    void testEqualsSame() {
        Assertions.assertEquals(dto, dto);
    }

    @Test
    @DisplayName("CharacterDtoTest:testEqualsNull()")
    void testEqualsNull() {
        Assertions.assertNotEquals(dto, null);
    }

    @Test
    @DisplayName("CharacterDtoTest:testEqualsOtherClass()")
    void testEqualsOtherClass() {
        Assertions.assertNotEquals(dto, "");
    }

    @Test
    @DisplayName("CharacterDtoTest:testHashCode()")
    void testHashCode() {
        Assertions.assertEquals(
                Objects.hash(characterId, name, user),
                dto.hashCode()
        );
    }

    @Test
    @DisplayName("CharacterDtoTest:testToString()")
    void testToString() {
        Assertions.assertEquals(
                "CharacterDto(characterId=" + characterId + ", " +
                        "name=" + name + ", " +
                        "gender=" + gender + ", " +
                        "note=" + note + ", " +
                        "user=" + user + ", " +
                        "lifeCount=" + lifeCount + ")",
                dto.toString());
    }

    @Test
    @DisplayName("CharacterDtoTest:getCharacterId()")
    void getCharacterId() {
        Assertions.assertEquals(characterId, dto.getCharacterId());
    }

    @Test
    @DisplayName("CharacterDtoTest:getName()")
    void getName() {
        Assertions.assertEquals(name, dto.getName());
    }

    @Test
    @DisplayName("CharacterDtoTest:getGender()")
    void getGender() {
        Assertions.assertEquals(gender, dto.getGender());
    }

    @Test
    @DisplayName("CharacterDtoTest:getNote()")
    void getNote() {
        Assertions.assertEquals(note, dto.getNote());
    }

    @Test
    @DisplayName("CharacterDtoTest:getUser()")
    void getUser() {
        Assertions.assertEquals(user, dto.getUser());
    }

    @Test
    @DisplayName("CharacterDtoTest:getLifeCount()")
    void getLifeCount() {
        Assertions.assertEquals(lifeCount, dto.getLifeCount());
    }

    @Test
    @DisplayName("CharacterDtoTest:setCharacterId()")
    void setCharacterId() {
        Assertions.assertEquals(characterId, dto.getCharacterId());
        Long newId = 2L;
        dto.setCharacterId(newId);
        Assertions.assertEquals(newId, dto.getCharacterId());
    }

    @Test
    @DisplayName("CharacterDtoTest:setName()")
    void setName() {
        Assertions.assertEquals(name, dto.getName());
        String newName = "newName";
        dto.setName(newName);
        Assertions.assertEquals(newName, dto.getName());
    }

    @Test
    @DisplayName("CharacterDtoTest:setGender()")
    void setGender() {
        Assertions.assertEquals(gender, dto.getGender());
        String newGender = "female";
        dto.setGender(newGender);
        Assertions.assertEquals(newGender, dto.getGender());
    }

    @Test
    @DisplayName("CharacterDtoTest:setNote()")
    void setNote() {
        Assertions.assertEquals(note, dto.getNote());
        String newNote = "newNote";
        dto.setNote(newNote);
        Assertions.assertEquals(newNote, dto.getNote());
    }

    @Test
    @DisplayName("CharacterDtoTest:setUser()")
    void setUser() {
        Assertions.assertEquals(user, dto.getUser());
        dto.setUser(differentUser);
        Assertions.assertEquals(differentUser, dto.getUser());
    }

    @Test
    @DisplayName("CharacterDtoTest:setUser()")
    void setLifeCount() {
        Assertions.assertEquals(lifeCount, dto.getLifeCount());
        dto.setLifeCount(lifeCount + 1);
        Assertions.assertEquals(lifeCount + 1, dto.getLifeCount());
    }
}