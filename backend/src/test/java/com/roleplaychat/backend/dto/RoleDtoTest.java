package com.roleplaychat.backend.dto;

import com.roleplaychat.backend.model.RoleName;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Objects;

class RoleDtoTest {
    private final Integer id        = 1;
    private final RoleName roleName = RoleName.ROLE_USER;

    private RoleDto dto;

    @BeforeEach
    private void initDto() {
        dto = new RoleDto(id, roleName);
    }

    @Test
    @DisplayName("RoleDtoTest:builder()")
    void builder() {
        RoleDto dto = RoleDto.builder()
                .id(id).roleName(roleName).build();

        Assertions.assertNotNull(dto);
        Assertions.assertEquals(id, dto.getId());
        Assertions.assertEquals(roleName, dto.getRoleName());
    }

    @Test
    @DisplayName("RoleDtoTest:builderToString()")
    void builderToString() {
        RoleDto.RoleDtoBuilder builder = RoleDto.builder()
                .id(id)
                .roleName(roleName);
        Assertions.assertEquals(
                "RoleDto.RoleDtoBuilder(id=" + id + ", roleName=" + roleName + ")",
                builder.toString());
    }

    @Test
    @DisplayName("RoleDtoTest:allArgsConstructor()")
    void allArgsConstructor() {
        dto = new RoleDto(id, roleName);
        Assertions.assertNotNull(dto);
        Assertions.assertEquals(id, dto.getId());
        Assertions.assertEquals(roleName, dto.getRoleName());
    }

    @Test
    @DisplayName("RoleDtoTest:noArgsConstructor()")
    void noArgsConstructor() {
        dto = new RoleDto();
        Assertions.assertNull(dto.getId());
        Assertions.assertNull(dto.getRoleName());
    }

    @Test
    @DisplayName("RoleDtoTest:testEqualsTrue()")
    void testEqualsTrue() {
        RoleDto otherDto = new RoleDto(id, roleName);
        Assertions.assertEquals(dto, otherDto);
    }

    @Test
    @DisplayName("RoleDtoTest:testEqualsDifferentId()")
    void testEqualsDifferentUserId() {
        RoleDto otherDto = new RoleDto(id + 1, roleName);
        Assertions.assertNotEquals(dto, otherDto);
    }

    @Test
    @DisplayName("RoleDtoTest:testEqualsDifferentName()")
    void testEqualsDifferentName() {
        RoleDto otherDto = new RoleDto(id, null);
        Assertions.assertNotEquals(dto, otherDto);
    }

    @Test
    @DisplayName("RoleDtoTest:testEqualsFalse()")
    void testEqualsFalse() {
        RoleDto otherDto = new RoleDto(id + 1, null);
        Assertions.assertNotEquals(dto, otherDto);
    }

    @Test
    @DisplayName("RoleDtoTest:testEqualsSame()")
    void testEqualsSame() {
        Assertions.assertEquals(dto, dto);
    }

    @Test
    @DisplayName("RoleDtoTest:testEqualsNull()")
    void testEqualsNull() {
        Assertions.assertNotEquals(dto, null);
    }

    @Test
    @DisplayName("RoleDtoTest:testEqualsOtherClass()")
    void testEqualsOtherClass() {
        Assertions.assertNotEquals(dto, "");
    }

    @Test
    @DisplayName("RoleDtoTest:testHashCode()")
    void testHashCode() {
        Assertions.assertEquals(
                Objects.hash(id, roleName),
                dto.hashCode()
        );
    }

    @Test
    @DisplayName("RoleDtoTest:testToString()")
    void testToString() {
        Assertions.assertEquals(
                "RoleDto(id=" + id + ", roleName=" + roleName + ")",
                dto.toString()
        );
    }

    @Test
    @DisplayName("RoleDtoTest:getId()")
    void getId() {
        Assertions.assertEquals(id, dto.getId());
    }

    @Test
    @DisplayName("RoleDtoTest:getRoleName()")
    void getRoleName() {
        Assertions.assertEquals(roleName, dto.getRoleName());
    }

    @Test
    @DisplayName("RoleDtoTest:setId()")
    void setId() {
        Assertions.assertEquals(id, dto.getId());
        Integer newId = 2;
        dto.setId(newId);
        Assertions.assertEquals(newId, dto.getId());
    }

    @Test
    @DisplayName("RoleDtoTest:setRoleName()")
    void setRoleName() {
        Assertions.assertEquals(roleName, dto.getRoleName());
        RoleName newRoleName = null;
        dto.setRoleName(newRoleName);
        Assertions.assertEquals(newRoleName, dto.getRoleName());
    }
}