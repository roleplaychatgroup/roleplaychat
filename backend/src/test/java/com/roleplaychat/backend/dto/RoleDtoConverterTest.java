package com.roleplaychat.backend.dto;

import com.roleplaychat.backend.model.Role;
import com.roleplaychat.backend.model.RoleName;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class RoleDtoConverterTest {
    private Role role;
    private RoleDto dto;

    private RoleDtoConverter converter;

    @BeforeEach
    private void initDtoConverter() {
        converter = new RoleDtoConverter();
    }

    @Test
    @DisplayName("RoleDtoConverterTest:convertToDto()")
    void convertToDto() {
        role = new Role(
                1, RoleName.ROLE_USER
        );

        dto = converter.convertToDto(role);

        Assertions.assertNotNull(dto);
        Assertions.assertEquals(role.getId(), dto.getId());
        Assertions.assertEquals(role.getRoleName(), dto.getRoleName());
    }

    @Test
    @DisplayName("RoleDtoConverterTest:convertFromDto()")
    void convertFromDto() {
        dto = new RoleDto(
                1, RoleName.ROLE_USER
        );

        role = converter.convertFromDto(dto);

        Assertions.assertNotNull(role);
        Assertions.assertEquals(dto.getId(), role.getId());
        Assertions.assertEquals(dto.getRoleName(), role.getRoleName());
    }
}