package com.roleplaychat.backend.dto;

import com.roleplaychat.backend.model.User;
import org.junit.jupiter.api.*;
import org.mockito.Mock;
import org.mockito.Mockito;

class UserDtoConverterTest {
    private final Long userId     = 1L;
    private final String name     = "Name";
    private final String email    = "email@email.com";
    private final String password = "password";
    private final String gender   = "male";
    @Mock private RoleDto roleDto = Mockito.mock(RoleDto.class);

    private User user;
    private UserDto dto;

    private RoleDtoConverter roleConverter;
    private UserDtoConverter userConverter;

    @BeforeEach
    private void initUserDtoConverter() {
        userConverter = new UserDtoConverter();
        roleConverter = new RoleDtoConverter();
    }

    @BeforeEach
    private void initUser() {
        user = new User(
                userId,
                name,
                email,
                password,
                gender,
                roleConverter.convertFromDto(roleDto)
        );
    }

    @BeforeEach
    private void initDto() {
        dto = new UserDto(
                userId,
                name,
                email,
                password,
                gender,
                roleDto
        );
    }

    @Test
    @DisplayName("UserDtoConverterTest:convertToDto()")
    void convertToDto() {
        dto = userConverter.convertToDto(user);

        Assertions.assertNotNull(dto);
        Assertions.assertEquals(user.getName(),     dto.getName());
        Assertions.assertEquals(user.getEmail(),    dto.getEmail());
        Assertions.assertEquals(user.getPassword(), dto.getPassword());
        Assertions.assertEquals(user.getGender(),   dto.getGender());

        Assertions.assertEquals(user.getRole().getRoleName(),
                roleConverter.convertFromDto(dto.getRole()).getRoleName());
    }

    @Test
    @DisplayName("UserDtoConverterTest:convertFromDto()")
    void convertFromDto() {
        user = userConverter.convertFromDto(dto);

        Assertions.assertNotNull(user);
        Assertions.assertEquals(dto.getName(),     user.getName());
        Assertions.assertEquals(dto.getEmail(),    user.getEmail());
        Assertions.assertEquals(dto.getPassword(), user.getPassword());
        Assertions.assertEquals(dto.getGender(),   user.getGender());

        Assertions.assertEquals(dto.getRole().getRoleName(),
                roleConverter.convertToDto(user.getRole()).getRoleName());
    }
}