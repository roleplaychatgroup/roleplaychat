package com.roleplaychat.backend.repo;

import com.roleplaychat.backend.model.User;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Optional;

@SpringBootTest
class UserRepositoryTest {
    @Autowired
    UserRepository userRepository;

    private User testUser;

    @BeforeEach
    void initTestUser() {
        PasswordEncoder encoder = new BCryptPasswordEncoder();

        testUser = User.builder()
                .name("Пастилова Полина")
                .password(encoder.encode("123456"))
                .gender("female")
                .email("pastilova@email.com")
                .build();
    }

    @Test
    @DisplayName("UserRepositoryTest:findByName()")
    void findByName() {
        Optional<User> user = userRepository.findByName(testUser.getName());

        Assertions.assertTrue(user.isPresent());
        Assertions.assertEquals(testUser.getName(), user.get().getName());
    }

    @Test
    @DisplayName("UserRepositoryTest:findByNameFailed()")
    void findByNameFailed() {
        Optional<User> user = userRepository.findByName("аволитсаП анилоП");
        Assertions.assertTrue(user.isEmpty());
    }

    @Test
    @DisplayName("UserRepositoryTest:findByEmail()")
    void findByEmail() {
        Optional<User> user = userRepository.findByEmail(testUser.getEmail());

        Assertions.assertTrue(user.isPresent());
        Assertions.assertEquals(testUser.getEmail(), user.get().getEmail());
    }

    @Test
    @DisplayName("UserRepositoryTest:findByEmailFailed()")
    void findByEmailFailed() {
        Optional<User> user = userRepository.findByEmail("avolitsap@liame.moc");
        Assertions.assertTrue(user.isEmpty());
    }

    @Test
    @DisplayName("UserRepositoryTest:existsByEmail()")
    void existsByEmail() {
        Assertions.assertTrue(userRepository.existsByEmail(testUser.getEmail()));
    }

    @Test
    @DisplayName("UserRepositoryTest:existsByEmailFailed()")
    void existsByEmailFailed() {
        Assertions.assertFalse(userRepository.existsByEmail("avolitsap@liame.moc"));
    }
}