package com.roleplaychat.backend.auth.payload.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@AllArgsConstructor
public class EditRequest {
    @Size(min = 3, max = 30)
    @Getter @Setter
    private String name;

    @NotBlank
    @Size(min = 6, max = 20)
    @Getter @Setter
    private String oldPassword;

    @Size(min = 6, max = 20)
    @Getter @Setter
    private String newPassword;

    @Size(max = 10)
    @Getter @Setter
    private String gender;

    @Getter @Setter
    @Email
    private String email;
}
