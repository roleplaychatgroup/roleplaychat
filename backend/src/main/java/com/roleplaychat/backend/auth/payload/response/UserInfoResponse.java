package com.roleplaychat.backend.auth.payload.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Email;

@AllArgsConstructor
public class UserInfoResponse {

    @Getter @Setter
    private String name;

    @Getter @Setter
    @Email
    private String email;

    @Getter @Setter
    private String gender;
}
