package com.roleplaychat.backend.auth.payload.response;

import lombok.Getter;
import lombok.Setter;

public class ErrorMessageResponse {
    @Getter @Setter
    private String message;

    public ErrorMessageResponse(String message) {
      this.message = message;
    }
    
}
