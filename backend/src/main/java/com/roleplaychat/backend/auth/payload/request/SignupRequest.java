package com.roleplaychat.backend.auth.payload.request;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.Set;

public class SignupRequest {  
    @NotBlank
    @Size(min = 3, max = 30)
    @Getter @Setter
    private String username;

    @NotBlank
    @Size(max = 50)
    @Email
    @Getter @Setter
    private String email;

    @NotBlank
    @Size(max = 10)
    @Getter @Setter
    private String gender;

    @NotBlank
    @Size(min = 6, max = 20)
    @Getter @Setter
    private String password;

    @Getter @Setter
    private Set<String> role;

    @Override
    public String toString() {
        return "SignupRequest{" +
                "username='" + username + '\'' +
                ", email='" + email + '\'' +
                ", gender='" + gender + '\'' +
                ", password='" + password + '\'' +
                ", role=" + role +
                '}';
    }

    @Builder
    public SignupRequest(String username, String email, String gender, String password, Set<String> role) {
        this.username = username;
        this.email = email;
        this.gender = gender;
        this.password = password;
        this.role = role;
    }
}
