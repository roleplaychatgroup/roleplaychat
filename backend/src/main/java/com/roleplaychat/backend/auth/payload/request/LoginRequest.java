package com.roleplaychat.backend.auth.payload.request;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public class LoginRequest {
	@NotBlank
    @Getter @Setter
    @Size(max = 50)
    @Email
    private String email;

	@NotBlank
    @Getter @Setter
	private String password;

    @Override
    public String toString() {
        return "LoginRequest{" +
                "email='" + email + '\'' +
                ", password='" + password + '\'' +
                '}';
    }

    @Builder
    public LoginRequest(String email, String password) {
        this.email = email;
        this.password = password;
    }
}