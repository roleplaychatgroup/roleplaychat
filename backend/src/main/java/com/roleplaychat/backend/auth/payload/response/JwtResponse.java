package com.roleplaychat.backend.auth.payload.response;

import lombok.Getter;
import lombok.Setter;
import java.util.List;

public class JwtResponse {
    @Getter @Setter
    private String token;

    private String type = "Bearer";

    @Getter @Setter
    private Long id;

    @Getter @Setter
    private String name;

    @Getter @Setter
    private String gender;

    @Getter @Setter
    private String email;
    
    @Getter @Setter
    private List<String> roles;
  
    public JwtResponse(String accessToken, Long id, String name, String gender, String email, List<String> roles) {
      this.token = accessToken;
      this.id = id;
      this.name = name;
      this.gender = gender;
      this.email = email;
      this.roles = roles;
    }
}
