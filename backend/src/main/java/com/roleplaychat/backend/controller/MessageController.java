package com.roleplaychat.backend.controller;

import com.roleplaychat.backend.Env;
import com.roleplaychat.backend.request.SendMessageRequest;
import com.roleplaychat.backend.services.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = {Env.ipAddress, Env.host}, maxAge = 3600)
@RestController
@RequestMapping("/api/")
public class MessageController {
    private final MessageService messageService;

    @Autowired
    public MessageController(MessageService messageService) {
        this.messageService = messageService;
    }

    @GetMapping({"/groups/{group_id}/messages"})
    public ResponseEntity<?> getAllMessages(@PathVariable Long group_id) {
        return messageService.getAllMessagesByGroupId(group_id);
    }

    @PostMapping({"groups/{group_id}/messages"})
    public ResponseEntity<?> sendMessage(@RequestBody SendMessageRequest messageRequest,
                                         @PathVariable Long group_id) {
        return messageService.addNewMessage(messageRequest, group_id);

    }
}
