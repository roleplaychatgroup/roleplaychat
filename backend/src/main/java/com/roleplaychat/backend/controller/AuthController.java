package com.roleplaychat.backend.controller;

import com.roleplaychat.backend.Env;
import com.roleplaychat.backend.auth.payload.request.EditRequest;
import com.roleplaychat.backend.auth.payload.request.LoginRequest;
import com.roleplaychat.backend.auth.payload.request.SignupRequest;
import com.roleplaychat.backend.auth.payload.response.ErrorMessageResponse;
import com.roleplaychat.backend.services.AuthService;
import com.roleplaychat.backend.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@CrossOrigin(origins = {Env.ipAddress, Env.host}, maxAge = 3600)
@RestController
@RequestMapping("/api")
public class AuthController {

    private final UserService userService;
    private final AuthService authService;

    @Autowired
    public AuthController(UserService userService, AuthService authService) {

        this.userService = userService;
        this.authService = authService;
    }

    @PostMapping("/auth/signin")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {
        return ResponseEntity.ok(authService.createJwtResponse(loginRequest));
    }

    @PostMapping("/user/edit")
    public ResponseEntity<?> editUserInfo(Authentication authentication, @RequestBody EditRequest editRequest) {
        return authService.editUserInfo(authentication, editRequest);
    }

    @GetMapping("/user/info")
    public ResponseEntity<?> getUserInfo(@RequestParam(value = "user_id") long userId) {
        return authService.getUserInfo(userId);
    }

    @PostMapping("/auth/signup")
    public ResponseEntity<?> registerUser(@Valid @RequestBody SignupRequest signUpRequest) {

        if (userService.existsByEmail(signUpRequest.getEmail())) {
            return ResponseEntity
                    .badRequest()
                    .body(new ErrorMessageResponse("Email is already in use!"));
        }

        authService.createNewUser(signUpRequest);
        return ResponseEntity.ok(new ErrorMessageResponse("User registered successfully!"));
    }
}
