package com.roleplaychat.backend.controller;

import com.roleplaychat.backend.Env;
import com.roleplaychat.backend.request.CharacterCreationRequest;
import com.roleplaychat.backend.services.CharacterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

@CrossOrigin(origins = {Env.ipAddress, Env.host}, maxAge = 3600)
@RestController
@RequestMapping("/api")
public class CharacterController {
    private final CharacterService characterService;

    @Autowired
    public CharacterController(CharacterService characterService) {
        this.characterService = characterService;
    }

    @PostMapping("/character")
    public ResponseEntity<?> addNewCharacter(Principal principal, @RequestBody CharacterCreationRequest characterRequest) {
        return characterService.createCharacter(principal, characterRequest);
    }

    @GetMapping("/character/list")
    public ResponseEntity<?> getListCharacter(Principal principal) {
        return characterService.getListCharacters(principal);
    }

    @DeleteMapping("/character/delete")
    public ResponseEntity<HttpStatus> deleteCharacter(@RequestParam(value = "character_id") long characterId) {
        return characterService.deleteCharacter(characterId);
    }

    @PostMapping("/character/life/add")
    public ResponseEntity<String> addCharacterLife(@RequestParam(value = "character_id") long characterId) {
        return characterService.addLife(characterId);
    }

    @PostMapping("/character/life/delete")
    public ResponseEntity<String> deleteCharacterLife(@RequestParam(value = "character_id") long characterId) {
        return characterService.deleteLife(characterId);
    }

    @GetMapping("/character/list/email")
    public ResponseEntity<?> getListCharacterByEmail(@RequestParam(value = "user_email") String userEmail) {
        return  characterService.getListCharactersByEmail(userEmail);
    }
}
