package com.roleplaychat.backend.controller;

import com.roleplaychat.backend.Env;
import com.roleplaychat.backend.request.AddOrEditGroupRequest;
import com.roleplaychat.backend.services.GroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = {Env.ipAddress, Env.host}, maxAge = 3600)
@RestController
@RequestMapping("/api/")
public class GroupController {
    private final GroupService groupService;

    @Autowired
    public GroupController(GroupService groupService) {
        this.groupService = groupService;
    }

    @GetMapping("/users/{userId}/groups")
    public ResponseEntity<?> getAllGroups(@PathVariable Long userId) {
        return groupService.getAllGroups(userId);
    }

    @GetMapping("/groups/{groupId}")
    public ResponseEntity<?> getGroupInfo(@PathVariable Long groupId) {
        return groupService.getGroupInfo(groupId);
    }

    @PostMapping("/groups")
    public ResponseEntity<?> addNewGroup(@RequestBody AddOrEditGroupRequest addRequest) {
        return groupService.addNewGroup(addRequest);
    }

    @PatchMapping("/groups/{groupId}")
    public ResponseEntity<?> editGroup(@PathVariable Long groupId, @RequestBody AddOrEditGroupRequest editRequest) {
        return groupService.editGroup(groupId,editRequest);
    }
}
