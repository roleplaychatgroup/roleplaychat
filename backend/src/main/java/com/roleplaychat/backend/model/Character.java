package com.roleplaychat.backend.model;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

@Entity
@NoArgsConstructor
@Table(name = "Characters")
public class Character {

    @Id
    @Column(name = "character_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter @Setter
    private Long characterId;

    @NotBlank
    @Column(name = "name")
    @NotBlank
    @Getter @Setter
    private String name;

    @NotBlank
    @Column(name = "gender")
    @Getter @Setter
    private String gender;

    @Column(name = "note", columnDefinition = "text")
    @Getter @Setter
    private String note;

    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    @Getter @Setter
    private User user;

    @Column(name = "life_count")
    @Getter @Setter
    private long lifeCount;

    @Builder
    public Character(String name, User user, String gender, String note, long lifeCount) {
        this.name = name;
        this.user = user;
        this.gender = gender;
        this.note = note;
        this.lifeCount = lifeCount;
    }
}
