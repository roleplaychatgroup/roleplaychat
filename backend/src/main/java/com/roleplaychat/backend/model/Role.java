package com.roleplaychat.backend.model;

import lombok.*;

import javax.persistence.*;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "Roles")
public class Role {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter @Setter
    private Integer id;

    @Column(name = "role_name", length = 20)
    @Enumerated(EnumType.STRING)
    @Getter @Setter
    private RoleName roleName;
}