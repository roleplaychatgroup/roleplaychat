package com.roleplaychat.backend.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "group_user")
@NoArgsConstructor
@AllArgsConstructor
public class GroupUser {

    @EmbeddedId
    @Getter @Setter
    private GroupUserKey id;

    @ManyToOne
    @MapsId("userId")
    @JoinColumn(name = "user_id")
    @Getter @Setter
    private User user;

    @ManyToOne
    @MapsId("groupId")
    @JoinColumn(name = "group_id")
    @Getter @Setter
    private Group group;

    @Column(name = "is_it_admin")
    @NotNull
    @Getter @Setter
    private boolean isItAdmin;

}
