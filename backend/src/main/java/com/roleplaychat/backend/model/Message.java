package com.roleplaychat.backend.model;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.Date;

@Entity
@NoArgsConstructor
@Table(name = "Messages")
public class Message {

    @Id
    @Column(name = "message_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter @Setter
    private Long messageId;

    @Column(name = "message_text", columnDefinition = "text")
    @NotBlank
    @Getter @Setter
    private String messageText;

    @Column(name = "sent_time")
    @NotBlank
    @Getter @Setter
    private Date sentTime;

    @ManyToOne
    @JoinColumn(name = "sender_id", nullable = false)
    @Getter @Setter
    private User sender;

    @JoinColumn(name = "character_id")
    @Getter @Setter
    private Long characterId;

    @JoinColumn(name = "character_name")
    @Getter @Setter
    private String characterName;

    @JoinColumn(name = "character_gender")
    @Getter @Setter
    private String characterGender;

    @ManyToOne
    @JoinColumn(name = "group_id", nullable = false)
    @Getter @Setter
    private Group group;

    @Builder
    public Message(String messageText, Date sentTime, User sender, Group group,
                   Long characterId, String characterName, String characterGender) {
        this.messageText = messageText;
        this.sentTime = sentTime;
        this.sender = sender;
        this.group = group;
        this.characterId = characterId;
        this.characterName = characterName;
        this.characterGender = characterGender;
    }

}
