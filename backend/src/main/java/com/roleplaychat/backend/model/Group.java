package com.roleplaychat.backend.model;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

@Entity
@NoArgsConstructor
@Table(name = "Groups")
public class Group {

    @Id
    @Column(name = "group_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter @Setter
    private Long groupId;

    @Column(name = "group_name")
    @NotBlank
    @Getter @Setter
    private String groupName;

    @Builder
    public Group(String groupName) {
        this.groupName = groupName;
    }

}
