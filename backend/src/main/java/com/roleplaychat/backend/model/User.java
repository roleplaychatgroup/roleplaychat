package com.roleplaychat.backend.model;

import lombok.*;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "Users")
public class User {

    @Id
    @Column(name = "user_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter @Setter
    private Long userId;

    @Column(name = "name")
    @NotBlank
    @Getter @Setter
    private String name;

    @Column(name = "email")
    @NotBlank
    @Getter @Setter
    @Email
    private String email;

    @Column(name = "password")
    @NotBlank
    @Getter @Setter
    private String password;

    @Column(name = "gender")
    @NotBlank
    @Getter @Setter
    private String gender;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "role_id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @Getter @Setter
    private Role role;

    @Builder
    public User(String name, String password, String gender, String email) {
        this.name = name;
        this.password = password;
        this.gender = gender;
        this.email = email;
    }

    public User(String name, String password, String gender, String email, Role role) {
        this.name = name;
        this.password = password;
        this.gender = gender;
        this.email = email;
        this.role = role;
    }

}
