package com.roleplaychat.backend.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@AllArgsConstructor
public class UserCharacterResponse {
    @Getter @Setter
    String username;

    @Getter @Setter
    Set<CharacterResponse> characters;
}
