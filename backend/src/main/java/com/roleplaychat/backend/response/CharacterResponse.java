package com.roleplaychat.backend.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
public class CharacterResponse {

    @Getter @Setter
    private Long characterId;

    @Getter @Setter
    private String characterName;

    @Getter @Setter
    private String characterGender;

    @Getter @Setter
    private String note;

    @Getter @Setter
    private long lifeCount;
}
