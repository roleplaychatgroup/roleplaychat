package com.roleplaychat.backend.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import java.util.Date;

@AllArgsConstructor
public class MessageInfoResponse {
    @NotBlank
    @Getter @Setter
    private String text;

    @NotBlank
    @Getter @Setter
    private Date sendTime;

    @NotBlank
    @Getter @Setter
    private Long userId;

    @NotBlank
    @Getter @Setter
    private String name;

    @NotBlank
    @Getter @Setter
    private String gender;

    @Getter @Setter
    private Long characterId;

    @Getter @Setter
    private String characterName;

    @Getter @Setter
    private String characterGender;

    public MessageInfoResponse(String text, Date sendTime, Long userId, String name, String gender) {
        this.text = text;
        this.sendTime = sendTime;
        this.userId = userId;
        this.name = name;
        this.gender = gender;
    }
}
