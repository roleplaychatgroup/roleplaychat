package com.roleplaychat.backend.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
public class TokenResponse {
    @Getter @Setter
    String token;
}
