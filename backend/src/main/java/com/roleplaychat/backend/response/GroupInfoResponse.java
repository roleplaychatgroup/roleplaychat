package com.roleplaychat.backend.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

@AllArgsConstructor
public class GroupInfoResponse {
    @NotBlank
    @Getter @Setter
    private Long groupId;

    @NotBlank
    @Getter @Setter
    private String groupName;
}
