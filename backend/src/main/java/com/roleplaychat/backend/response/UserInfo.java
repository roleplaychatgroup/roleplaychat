package com.roleplaychat.backend.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import java.util.Set;

@AllArgsConstructor
public class UserInfo {

    @NotBlank
    @Getter @Setter
    private String userEmail;

    @NotBlank
    @Getter @Setter
    private Set<CharacterResponse> characters;

}
