package com.roleplaychat.backend.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import java.util.List;

@AllArgsConstructor
public class GroupFullInfoResponse {

    @NotBlank
    @Getter @Setter
    private Long groupId;

    @NotBlank
    @Getter @Setter
    private String groupName;

    @NotBlank
    @Getter @Setter
    private List<UserInfo> userList;

    @NotBlank
    @Getter @Setter
    private Long adminId;
}
