package com.roleplaychat.backend;

import org.springframework.boot.Banner;
import com.roleplaychat.backend.repo.GroupRepository;
import com.roleplaychat.backend.repo.GroupUserRepository;
import com.roleplaychat.backend.repo.RoleRepository;
import com.roleplaychat.backend.repo.UserRepository;
import com.roleplaychat.backend.utils.InitializeTestData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;

@SpringBootApplication(exclude = { SecurityAutoConfiguration.class })
public class BackendApplication implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication application = new SpringApplication(BackendApplication.class);
        application.setBanner(applicationBanner);
        application.run(args);
    }

    private final static Banner applicationBanner = (environment, sourceClass, out) -> out.print("\n" +
            "                                                                                                                                                                                \n" +
            "                     ,,                    ,,                                      ,,                                                                                       ,,  \n" +
            "`7MM\"\"\"Mq.         `7MM                  `7MM                          .g8\"\"\"bgd `7MM                 mm       `7MM\"\"\"Yp,                `7MM                             `7MM  \n" +
            "  MM   `MM.          MM                    MM                        .dP'     `M   MM                 MM         MM    Yb                  MM                               MM  \n" +
            "  MM   ,M9  ,pW\"Wq.  MM  .gP\"Ya `7MMpdMAo. MM   ,6\"Yb.`7M'   `MF'    dM'       `   MMpMMMb.   ,6\"Yb.mmMMmm       MM    dP  ,6\"Yb.  ,p6\"bo  MM  ,MP'.gP\"Ya `7MMpMMMb.   ,M\"\"bMM  \n" +
            "  MMmmdM9  6W'   `Wb MM ,M'   Yb  MM   `Wb MM  8)   MM  VA   ,V      MM            MM    MM  8)   MM  MM         MM\"\"\"bg. 8)   MM 6M'  OO  MM ;Y  ,M'   Yb  MM    MM ,AP    MM  \n" +
            "  MM  YM.  8M     M8 MM 8M\"\"\"\"\"\"  MM    M8 MM   ,pm9MM   VA ,V       MM.           MM    MM   ,pm9MM  MM         MM    `Y  ,pm9MM 8M       MM;Mm  8M\"\"\"\"\"\"  MM    MM 8MI    MM  \n" +
            "  MM   `Mb.YA.   ,A9 MM YM.    ,  MM   ,AP MM  8M   MM    VVV        `Mb.     ,'   MM    MM  8M   MM  MM         MM    ,9 8M   MM YM.    , MM `Mb.YM.    ,  MM    MM `Mb    MM  \n" +
            ".JMML. .JMM.`Ybmd9'.JMML.`Mbmmd'  MMbmmd'.JMML.`Moo9^Yo.  ,V           `\"bmmmd'  .JMML  JMML.`Moo9^Yo.`Mbmo    .JMMmmmd9  `Moo9^Yo.YMbmd'.JMML. YA.`Mbmmd'.JMML  JMML.`Wbmd\"MML.\n" +
            "                                  MM                     ,V                                                                                                                     \n" +
            "                                .JMML.                OOb\"                                                                                                                      \n");


    @Autowired UserRepository userRepository;
    @Autowired RoleRepository roleRepository;
    @Autowired GroupRepository groupRepository;

    @Override
    public void run(String... args) {
        InitializeTestData.initializeUsers(userRepository, roleRepository);
        InitializeTestData.initializeGroups(groupRepository);
    }

}
