package com.roleplaychat.backend.request;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import java.util.Date;

public class SendMessageRequest {

    @NotBlank
    @Getter @Setter
    private String text;

    @NotBlank
    @Getter @Setter
    private Date sendTime;

    @NotBlank
    @Getter @Setter
    private Long userId;

    @Getter @Setter
    private Long characterId;

    public SendMessageRequest(String text, Date sendTime, Long userId, Long characterId) {
        this.text = text;
        this.sendTime = sendTime;
        this.userId = userId;
        this.characterId = characterId;
    }
}
