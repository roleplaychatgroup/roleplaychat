package com.roleplaychat.backend.request;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

public class CharacterCreationRequest {

    @NotBlank
    @Getter @Setter
    private String characterName;

    @NotBlank
    @Getter @Setter
    private String characterGender;

    public CharacterCreationRequest(String characterName, String characterGender) {
        this.characterName = characterName;
        this.characterGender = characterGender;
    }
}
