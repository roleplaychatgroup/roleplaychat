package com.roleplaychat.backend.request;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import java.util.List;

public class AddOrEditGroupRequest {

    @NotBlank
    @Getter @Setter
    private List<String> userList;

    @NotBlank
    @Getter @Setter
    private String groupName;

    @NotBlank
    @Getter @Setter
    private Long adminId;

    public AddOrEditGroupRequest(List<String> userList, String groupName, Long adminId) {
        this.userList = userList;
        this.groupName = groupName;
        this.adminId = adminId;
    }
}
