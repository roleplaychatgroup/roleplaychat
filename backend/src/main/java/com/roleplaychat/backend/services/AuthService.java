package com.roleplaychat.backend.services;

import com.roleplaychat.backend.auth.payload.request.EditRequest;
import com.roleplaychat.backend.auth.payload.request.LoginRequest;
import com.roleplaychat.backend.auth.payload.request.SignupRequest;
import com.roleplaychat.backend.auth.payload.response.JwtResponse;
import com.roleplaychat.backend.auth.payload.response.UserInfoResponse;
import com.roleplaychat.backend.auth.user_info.UserDetailsImpl;
import com.roleplaychat.backend.dto.UserDto;
import com.roleplaychat.backend.dto.UserDtoConverter;
import com.roleplaychat.backend.exceptions.EmailInUseException;
import com.roleplaychat.backend.exceptions.PasswordNotValidException;
import com.roleplaychat.backend.exceptions.UserNotFoundException;
import com.roleplaychat.backend.model.User;
import com.roleplaychat.backend.utils.JwtUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class AuthService {
    private final AuthenticationManager authenticationManager;

    private final JwtUtils jwtUtils;
    private final PasswordEncoder encoder;
    private final UserService userService;
    private final UserDtoConverter converter = new UserDtoConverter();

    @Autowired
    public AuthService(AuthenticationManager authenticationManager, JwtUtils jwtUtils, PasswordEncoder encoder, UserService userService) {
        this.authenticationManager = authenticationManager;
        this.jwtUtils = jwtUtils;
        this.encoder = encoder;
        this.userService = userService;
    }

    public ResponseEntity<?> getUserInfo(long userId) {
        try {
            UserDto userDto = converter.convertToDto(userService.getUserById(userId)
                    .orElseThrow(() -> new UserNotFoundException("User with this id doesn't exist!")));
            UserInfoResponse response = new UserInfoResponse(userDto.getName(), userDto.getEmail(), userDto.getGender());
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (UserNotFoundException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    public JwtResponse createJwtResponse(LoginRequest loginRequest) {
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginRequest.getEmail(), loginRequest.getPassword()));
        SecurityContextHolder.getContext().setAuthentication(authentication);

        String jwt = jwtUtils.generateToken(authentication);

        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();

        List<String> roles = userDetails.getAuthorities().stream()
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.toList());
        return new JwtResponse(jwt, userDetails.getId(), userDetails.getName(), userDetails.getGender(), userDetails.getUsername(), roles);
    }

    public String createNewJwtToken(EditRequest request, UserDto userDto, Authentication authentication) throws PasswordNotValidException, EmailInUseException {
        String jwt = "";
        var oldPasswordRequest = request.getOldPassword();
        var oldPasswordDb = userDto.getPassword();
        if (oldPasswordRequest == null) {
            throw new PasswordNotValidException("No old password!");
        }
        if (encoder.matches(oldPasswordRequest, oldPasswordDb)) {
            if (request.getNewPassword() != null) {
                userDto.setPassword(encoder.encode(request.getNewPassword()));
                userService.save(converter.convertFromDto(userDto));
                Authentication newAuthentication = authenticationManager.authenticate(
                        new UsernamePasswordAuthenticationToken(userDto.getEmail(), request.getNewPassword()));
                SecurityContextHolder.getContext().setAuthentication(newAuthentication);
                jwt = jwtUtils.generateToken(newAuthentication);
            }
        } else {
            throw new PasswordNotValidException("Old password doesn't match!");
        }

        if (request.getEmail() != null) {
            if (!userDto.getEmail().equals(request.getEmail())) {
                if (userService.existsByEmail(request.getEmail())) {
                    throw new EmailInUseException("User with this email already exist!");
                }
                userDto.setEmail(request.getEmail());
                userService.save(converter.convertFromDto(userDto));
                Authentication newAuthentication = authenticationManager.authenticate(
                        new UsernamePasswordAuthenticationToken(userDto.getEmail(),
                                request.getNewPassword() == null ? request.getOldPassword() : request.getNewPassword()));
                SecurityContextHolder.getContext().setAuthentication(newAuthentication);
                jwt = jwtUtils.generateToken(newAuthentication);
            }
        }
        return jwt;
    }

    public ResponseEntity<?> editUserInfo(Authentication authentication, EditRequest editRequest) {
        try {
            User user = userService.getUserByEmail(authentication.getName())
                    .orElseThrow(() -> new UserNotFoundException("User with this id doesn't exist!"));
            UserDto userDto = converter.convertToDto(user);

            String jwt = createNewJwtToken(editRequest, userDto, authentication);

            if (editRequest.getName() != null) {
                userDto.setName(editRequest.getName());
            }
            if (editRequest.getGender() != null) {
                userDto.setGender(editRequest.getGender());
            }
            userService.save(converter.convertFromDto(userDto));
            return new ResponseEntity<>(jwt, HttpStatus.OK);

        } catch (UserNotFoundException | EmailInUseException | PasswordNotValidException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    public User createNewUser(SignupRequest signUpRequest) {
        UserDto userDto = UserDto.builder().
                name(signUpRequest.getUsername()).
                email(signUpRequest.getEmail()).
                gender(signUpRequest.getGender()).
                password(encoder.encode(signUpRequest.getPassword())).
                build();

        Set<String> strRoles = signUpRequest.getRole();
        userService.setRoles(strRoles, userDto);
        return userService.save(converter.convertFromDto(userDto));
    }
}
