package com.roleplaychat.backend.services;

import com.roleplaychat.backend.dto.RoleDtoConverter;
import com.roleplaychat.backend.dto.UserDto;
import com.roleplaychat.backend.model.RoleName;
import com.roleplaychat.backend.model.Role;
import com.roleplaychat.backend.model.User;
import com.roleplaychat.backend.repo.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Service
public class UserService {
    private final UserRepository userRepository;
    private final RoleService roleService;
    private final RoleDtoConverter converter = new RoleDtoConverter();

    @Autowired
    public UserService(UserRepository userRepository, RoleService roleService) {
        this.userRepository = userRepository;
        this.roleService = roleService;
    }

    public Optional<User> getUserById(Long userId){
        return userRepository.findById(userId);
    }

    public Optional<User> getUserByEmail(String email){
        return userRepository.findByEmail(email);
    }

    public User save(User user) {
        return userRepository.save(user);
    }

    public Boolean existsByEmail(String email) {
        return userRepository.existsByEmail(email);
    }

    public void setRoles(Set<String> strRoles, UserDto userDto) {
        HashSet<Role> roles = new HashSet<>();
        Role userRole = roleService.findByRoleName(RoleName.ROLE_USER)
                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
        roles.add(userRole);
        userDto.setRole(converter.convertToDto(roles.iterator().next()));
    }
}