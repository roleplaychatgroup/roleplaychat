package com.roleplaychat.backend.services;

import com.roleplaychat.backend.dto.CharacterDto;
import com.roleplaychat.backend.dto.CharacterDtoConverter;
import com.roleplaychat.backend.dto.UserDtoConverter;
import com.roleplaychat.backend.exceptions.CharacterNotFoundException;
import com.roleplaychat.backend.exceptions.UserNotFoundException;
import com.roleplaychat.backend.model.Character;
import com.roleplaychat.backend.model.User;
import com.roleplaychat.backend.repo.CharacterRepository;
import com.roleplaychat.backend.request.CharacterCreationRequest;
import com.roleplaychat.backend.response.CharacterResponse;
import com.roleplaychat.backend.response.UserCharacterResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.util.HashSet;
import java.util.Set;

@Service
public class CharacterService {
    private final static int CHARACTER_LIMIT = 5;
    private final UserService userService;
    private final CharacterRepository characterRepository;
    private final CharacterDtoConverter characterDtoConverter = new CharacterDtoConverter();
    private final UserDtoConverter userDtoConverter = new UserDtoConverter();

    @Autowired
    public CharacterService(UserService userService, CharacterRepository characterRepository) {
        this.userService = userService;
        this.characterRepository = characterRepository;
    }

    public Set<CharacterResponse> getCharactersByUser(User user) {
        Set<Character> characters = characterRepository.findCharactersByUser(user);
        Set<CharacterResponse> characterResponses = new HashSet<>();
        characters.forEach(character -> {
            characterResponses.add(new CharacterResponse(character.getCharacterId(), character.getName(),
                    character.getGender(), character.getNote(), character.getLifeCount()));
        });
        return characterResponses;
    }

    public Character getCharacterById(long characterId) throws CharacterNotFoundException {
        return characterRepository.findById(characterId)
                .orElseThrow(() -> new CharacterNotFoundException("Character with this id doesn't exist!"));
    }

    public ResponseEntity<?> getListCharacters(Principal principal) {
        User user;
        try {
            user = userService.getUserByEmail(principal.getName())
                    .orElseThrow(() -> new UserNotFoundException("User with this id doesn't exist!"));
        } catch (UserNotFoundException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(getCharactersByUser(user), HttpStatus.OK);
    }

    public Character save(CharacterDto characterDto) {
        Character character = characterDtoConverter.convertFromDto(characterDto);
        return characterRepository.save(character);
    }

    public ResponseEntity<?> createCharacter(Principal principal, CharacterCreationRequest characterRequest) {
        User user;
        try {
            user = userService.getUserByEmail(principal.getName())
                    .orElseThrow(() -> new UserNotFoundException("User with this id doesn't exist!"));
            Set<CharacterResponse> characterDtos = getCharactersByUser(user);

            if (characterDtos.size() < CHARACTER_LIMIT) {
                CharacterDto characterDto = CharacterDto.builder()
                        .name(characterRequest.getCharacterName())
                        .gender(characterRequest.getCharacterGender())
                        .user(userDtoConverter.convertToDto(user))
                        .note(null) // Notes will be added later
                        .lifeCount(10)
                        .build();

                Character savedCharacter = save(characterDto);

                CharacterResponse characterCreationResponse = new CharacterResponse(
                        savedCharacter.getCharacterId(), savedCharacter.getName(),
                        savedCharacter.getGender(), savedCharacter.getNote(), savedCharacter.getLifeCount()
                );
                return new ResponseEntity<>(characterCreationResponse, HttpStatus.OK);
            } else
                return new ResponseEntity<>("You reached the limit of characters!", HttpStatus.BAD_REQUEST);
        } catch (UserNotFoundException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.NO_CONTENT);
        }
    }

    public ResponseEntity<HttpStatus> deleteCharacter(long characterId) {
        characterRepository.deleteById(characterId);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    public ResponseEntity<String> addLife(long characterId) {
        try {
            Character character = getCharacterById(characterId);
            CharacterDto characterDto = characterDtoConverter.convertToDto(character);
            characterDto.setLifeCount(characterDto.getLifeCount() + 1);
            save(characterDto);
            return new ResponseEntity<>("Success!", HttpStatus.OK);
        } catch (CharacterNotFoundException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    public ResponseEntity<String> deleteLife(long characterId) {
        try {
            Character character = getCharacterById(characterId);
            CharacterDto characterDto = characterDtoConverter.convertToDto(character);
            characterDto.setLifeCount(characterDto.getLifeCount() - 1);
            save(characterDto);
            return new ResponseEntity<>("Success!", HttpStatus.OK);
        } catch (CharacterNotFoundException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    public ResponseEntity<?> getListCharactersByEmail(String userEmail) {
        User user;
        try {
            user = userService.getUserByEmail(userEmail)
                    .orElseThrow(() -> new UserNotFoundException("User with this id doesn't exist!"));
        } catch (UserNotFoundException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
        UserCharacterResponse userCharacterResponse = new UserCharacterResponse(user.getName(), getCharactersByUser(user));
        return new ResponseEntity<>(userCharacterResponse, HttpStatus.OK);
    }
}
