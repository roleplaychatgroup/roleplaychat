package com.roleplaychat.backend.services;

import com.roleplaychat.backend.model.RoleName;
import com.roleplaychat.backend.model.Role;
import com.roleplaychat.backend.repo.RoleRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class RoleService {
    private final RoleRepository roleRepository;

    public RoleService(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    Optional<Role> findByRoleName(RoleName roleName) {
        return roleRepository.findByRoleName(roleName);
    }
}
