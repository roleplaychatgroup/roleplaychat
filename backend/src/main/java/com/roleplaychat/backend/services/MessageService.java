package com.roleplaychat.backend.services;

import com.roleplaychat.backend.exceptions.CharacterNotFoundException;
import com.roleplaychat.backend.exceptions.GroupNotFoundException;
import com.roleplaychat.backend.exceptions.UserNotFoundException;
import com.roleplaychat.backend.model.Character;
import com.roleplaychat.backend.model.Group;
import com.roleplaychat.backend.model.Message;
import com.roleplaychat.backend.model.User;
import com.roleplaychat.backend.repo.MessageRepository;
import com.roleplaychat.backend.request.SendMessageRequest;
import com.roleplaychat.backend.response.MessageInfoResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class MessageService {

    private final MessageRepository messageRepository;
    private final UserService userService;
    private final GroupService groupService;
    private final CharacterService characterService;

    @Autowired
    public MessageService(MessageRepository messageRepository, UserService userService,
                          GroupService groupService, CharacterService characterService) {
        this.messageRepository = messageRepository;
        this.userService = userService;
        this.groupService = groupService;
        this.characterService = characterService;
    }

    public ResponseEntity<?> getAllMessagesByGroupId(Long groupId) {
        List<Message> messages = messageRepository.findMessagesByGroup_GroupIdOrderBySentTime(groupId);
        List<MessageInfoResponse> messagesResponses = new ArrayList<>();
        for (Message message : messages) {
            MessageInfoResponse messagesResponse;
                messagesResponse = new MessageInfoResponse(
                        message.getMessageText(),
                        message.getSentTime(),
                        message.getSender().getUserId(),
                        message.getSender().getName(),
                        message.getSender().getGender(),
                        message.getCharacterId(),
                        message.getCharacterName(),
                        message.getCharacterGender()
                        );
            messagesResponses.add(messagesResponse);
        }
        return new ResponseEntity<>(messagesResponses, HttpStatus.OK);
    }

    public ResponseEntity<?> addNewMessage(SendMessageRequest messageRequest, Long groupId)  {
        Long userId = messageRequest.getUserId();
        User user;
        Group group;

        try {
            user = userService.getUserById(userId)
                    .orElseThrow(() -> new UserNotFoundException("User not found with id: " + userId));
            group = groupService.getGroupById(groupId)
                    .orElseThrow(() -> new GroupNotFoundException("Group not found with id:" + groupId));
        } catch (UserNotFoundException | GroupNotFoundException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }

        Message message;
        if (messageRequest.getCharacterId() != null) {
            Character character;
            try {
                character = characterService.getCharacterById(messageRequest.getCharacterId());
            } catch (CharacterNotFoundException e) {
                return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
            }
            message = new Message(messageRequest.getText(), messageRequest.getSendTime(), user, group,
                    character.getCharacterId(), character.getName(), character.getGender());
        } else {
            message = new Message(messageRequest.getText(), messageRequest.getSendTime(), user, group,
                    null, null, null);
        }
        messageRepository.save(message);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
