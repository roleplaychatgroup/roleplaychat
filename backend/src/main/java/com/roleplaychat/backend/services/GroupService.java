package com.roleplaychat.backend.services;

import com.roleplaychat.backend.exceptions.GroupNotFoundException;
import com.roleplaychat.backend.exceptions.UserNotFoundException;
import com.roleplaychat.backend.model.Group;
import com.roleplaychat.backend.model.GroupUser;
import com.roleplaychat.backend.model.GroupUserKey;
import com.roleplaychat.backend.model.User;
import com.roleplaychat.backend.repo.GroupRepository;
import com.roleplaychat.backend.repo.GroupUserRepository;
import com.roleplaychat.backend.request.AddOrEditGroupRequest;
import com.roleplaychat.backend.response.CharacterResponse;
import com.roleplaychat.backend.response.GroupFullInfoResponse;
import com.roleplaychat.backend.response.GroupInfoResponse;
import com.roleplaychat.backend.response.UserInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class GroupService {
    private final GroupRepository groupRepository;
    private final GroupUserRepository groupUserRepository;
    private final UserService userService;
    private final CharacterService characterService;

    @Autowired
    public GroupService(GroupRepository groupRepository, GroupUserRepository groupUserRepository, UserService userService, CharacterService characterService) {
        this.groupRepository = groupRepository;
        this.groupUserRepository = groupUserRepository;
        this.userService = userService;
        this.characterService = characterService;
    }

    public Optional<Group> getGroupById(Long groupId) {
        return groupRepository.findById(groupId);
    }

    public ResponseEntity<?> getAllGroups(Long userId) {
        List<GroupUser> groupUsers = groupUserRepository.findGroupUsersByUser_UserId(userId);
        if (groupUsers.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        List<GroupInfoResponse> groupInfoResponses = new ArrayList<>();
        for (GroupUser groupUser : groupUsers) {
            groupInfoResponses.add(
                    new GroupInfoResponse(groupUser.getGroup().getGroupId(), groupUser.getGroup().getGroupName()));
        }
        return new ResponseEntity<>(groupInfoResponses, HttpStatus.OK);
    }

    public ResponseEntity<?> getGroupInfo(Long groupId) {
        Group group;
        try {
            group = groupRepository.findById(groupId).orElseThrow(() -> new GroupNotFoundException("Group Not Found with id:" + groupId));
        } catch (GroupNotFoundException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
        List<UserInfo> userList = new ArrayList<>();
        List<GroupUser> groupUsers = groupUserRepository.findGroupUsersByGroup_GroupId(groupId);
        Long adminId = 0L;
        for (GroupUser groupUser : groupUsers) {
            User user = groupUser.getUser();
            Set<CharacterResponse> characters = characterService.getCharactersByUser(user);
            userList.add(new UserInfo(user.getEmail(),characters));
            if (groupUser.isItAdmin()) {
                adminId = groupUser.getUser().getUserId();
            }
        }
        GroupFullInfoResponse groupFullInfoResponse = new GroupFullInfoResponse(groupId, group.getGroupName(), userList, adminId);
        return new ResponseEntity<>(groupFullInfoResponse, HttpStatus.OK);
    }

    public ResponseEntity<?> addNewGroup(AddOrEditGroupRequest AddGroupRequest) {
        Group group = new Group(AddGroupRequest.getGroupName());
        Group newGroup = groupRepository.save(group);
        try {
            updateGroupUserRepository(AddGroupRequest, newGroup);
        } catch (UserNotFoundException e) {
            return new ResponseEntity<>(e.getMessage(),HttpStatus.BAD_REQUEST);
        }
        GroupInfoResponse groupInfoResponse = new GroupInfoResponse(newGroup.getGroupId(), newGroup.getGroupName());
        return new ResponseEntity<>(groupInfoResponse, HttpStatus.OK);
    }

    public void updateGroupUserRepository(AddOrEditGroupRequest groupRequest, Group group) throws UserNotFoundException {
        for (String email : groupRequest.getUserList()) {
            User user;
            try {
                user = userService.getUserByEmail(email)
                        .orElseThrow(() -> new UserNotFoundException("User not found with email: " + email));
            } catch (UserNotFoundException e) {
                continue;
            }
            GroupUser groupUser = new GroupUser(
                    new GroupUserKey(user.getUserId(), group.getGroupId()), user, group, false);
            groupUserRepository.save(groupUser);
        }

        User admin = userService.getUserById(groupRequest.getAdminId())
                .orElseThrow(() -> new UserNotFoundException("User not found with id: " + groupRequest.getAdminId()));

        GroupUser groupUser = new GroupUser(
                new GroupUserKey(admin.getUserId(), group.getGroupId()),
                admin, group, true);
        groupUserRepository.save(groupUser);
    }

    public ResponseEntity<?> editGroup(Long groupId, AddOrEditGroupRequest editGroupRequest) {
        Group group;
        try {
            group = groupRepository.findById(groupId).orElseThrow(() -> new GroupNotFoundException("Group Not Found with id:" + groupId));
        } catch (GroupNotFoundException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
        group.setGroupName(editGroupRequest.getGroupName());
        groupRepository.save(group);
        groupUserRepository.deleteGroupUsersByGroup_GroupId(groupId);
        try {
            updateGroupUserRepository(editGroupRequest, group);
        } catch (UserNotFoundException e) {
            return new ResponseEntity<>(e.getMessage(),HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
