package com.roleplaychat.backend.services;

import com.roleplaychat.backend.auth.user_info.UserDetailsImpl;
import com.roleplaychat.backend.model.User;
import com.roleplaychat.backend.repo.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
	private final UserRepository userRepository;

	@Autowired
	public UserDetailsServiceImpl(UserRepository userRepository) {
		this.userRepository = userRepository;
	}

	@Override
	@Transactional
	public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
		User user = userRepository.findByEmail(login)
				.orElseThrow(() -> new UsernameNotFoundException("User Not Found with username: " + login));
		return UserDetailsImpl.build(user);
	}
}