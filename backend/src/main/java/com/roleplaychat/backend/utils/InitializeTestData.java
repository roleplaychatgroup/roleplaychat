package com.roleplaychat.backend.utils;

import com.roleplaychat.backend.model.*;
import com.roleplaychat.backend.repo.GroupRepository;
import com.roleplaychat.backend.repo.RoleRepository;
import com.roleplaychat.backend.repo.UserRepository;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Optional;

public class InitializeTestData {
    public static String usersTestDataPath = "test-data/users-data.txt";
    public static String groupsTestDataPath = "test-data/groups-data.txt";

    public static void initializeUsers(UserRepository userRepository, RoleRepository roleRepository) {
        BufferedReader br;
        try {
            InputStream in = userRepository.getClass()
                    .getClassLoader().getResourceAsStream(usersTestDataPath);
            if (in == null) {
                throw new RuntimeException("Unable to open " + usersTestDataPath);
            }
            br = new BufferedReader(new InputStreamReader(in));

            PasswordEncoder encoder = new BCryptPasswordEncoder();

            Optional<Role> optionalRole = roleRepository.findByRoleName(RoleName.ROLE_USER);
            Role role = optionalRole.orElse(null);

            String s = "";
            while ((s = br.readLine()) != null) {
                User user = User.builder()
                        .name(br.readLine().split(":")[1])
                        .password(encoder.encode(br.readLine().split(":")[1]))
                        .gender(br.readLine().split(":")[1])
                        .email(br.readLine().split(":")[1])
                        .build();
                user.setRole(role);

                if (!userRepository.existsByEmail(user.getEmail())) {
                    userRepository.save(user);
                }
            }
        } catch (Exception ignored) {}
    }

    public static void initializeGroups(GroupRepository groupRepository) {
        BufferedReader br;
        try {
            InputStream in = groupRepository.getClass()
                    .getClassLoader().getResourceAsStream(groupsTestDataPath);
            if (in == null) {
                throw new RuntimeException("Unable to open " + groupsTestDataPath);
            }
            br = new BufferedReader(new InputStreamReader(in));

            String s = "";
            while ((s = br.readLine()) != null) {
                Group group = Group.builder()
                        .groupName(br.readLine().split(":")[1])
                        .build();
                if (!groupRepository.existsByGroupName(group.getGroupName())) {
                    groupRepository.save(group);
                }
            }
        } catch (Exception ignored) {}
    }
}
