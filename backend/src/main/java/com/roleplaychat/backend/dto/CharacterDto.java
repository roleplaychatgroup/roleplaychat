package com.roleplaychat.backend.dto;

import lombok.*;

import java.util.Objects;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CharacterDto {
    @Getter @Setter
    private Long characterId;

    @Getter @Setter
    private String name;

    @Getter @Setter
    private String gender;

    @Getter @Setter
    private String note;

    @Getter @Setter
    private UserDto user;

    @Getter @Setter
    private long lifeCount;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CharacterDto that = (CharacterDto) o;
        return Objects.equals(characterId, that.characterId)
                && Objects.equals(name, that.name)
                && Objects.equals(user, that.user);
    }

    @Override
    public int hashCode() {
        return Objects.hash(characterId, name, user);
    }

}
