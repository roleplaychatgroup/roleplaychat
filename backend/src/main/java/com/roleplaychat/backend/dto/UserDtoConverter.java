package com.roleplaychat.backend.dto;

import com.roleplaychat.backend.model.User;
import org.modelmapper.ModelMapper;

public class UserDtoConverter {
    private final ModelMapper modelMapper;

    public UserDtoConverter() {
        this.modelMapper = new ModelMapper();
    }

    public UserDto convertToDto(User user) {
        return modelMapper.map(user, UserDto.class);
    }

    public User convertFromDto(UserDto userDto) {
        return modelMapper.map(userDto, User.class);
    }
}
