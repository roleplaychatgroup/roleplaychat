package com.roleplaychat.backend.dto;

import com.roleplaychat.backend.model.Character;
import org.modelmapper.ModelMapper;

public class CharacterDtoConverter {
    private final ModelMapper modelMapper;

    public CharacterDtoConverter() {
        this.modelMapper = new ModelMapper();
    }

    public CharacterDto convertToDto(Character character) {
        return modelMapper.map(character, CharacterDto.class);
    }

    public Character convertFromDto(CharacterDto characterDto) {
        return modelMapper.map(characterDto, Character.class);
    }

}
