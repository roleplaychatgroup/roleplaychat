package com.roleplaychat.backend.dto;

import com.roleplaychat.backend.model.Role;
import org.modelmapper.ModelMapper;

public class RoleDtoConverter {
    private final ModelMapper modelMapper;

    public RoleDtoConverter() {
        this.modelMapper = new ModelMapper();
    }

    public RoleDto convertToDto(Role role) {
        return modelMapper.map(role, RoleDto.class);
    }

    public Role convertFromDto(RoleDto roleDto) {
        return modelMapper.map(roleDto, Role.class);
    }
}
