package com.roleplaychat.backend.dto;

import com.roleplaychat.backend.model.RoleName;
import lombok.*;

import java.util.Objects;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RoleDto {
    @Getter @Setter
    private Integer id;

    @Getter @Setter
    private RoleName roleName;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RoleDto that = (RoleDto) o;
        return Objects.equals(id, that.id)
                && Objects.equals(roleName, that.roleName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, roleName);
    }
}
