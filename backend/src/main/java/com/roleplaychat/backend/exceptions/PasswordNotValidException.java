package com.roleplaychat.backend.exceptions;

public class PasswordNotValidException extends Exception {

    public PasswordNotValidException(String message) {
        super(message);
    }

}
