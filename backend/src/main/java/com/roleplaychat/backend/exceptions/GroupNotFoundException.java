package com.roleplaychat.backend.exceptions;

public class GroupNotFoundException extends Exception {

    public GroupNotFoundException(String message) {
        super(message);
    }

}
