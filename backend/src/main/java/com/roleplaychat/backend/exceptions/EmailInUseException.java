package com.roleplaychat.backend.exceptions;

public class EmailInUseException extends Exception{

    public EmailInUseException(String message) {
        super(message);
    }

}
