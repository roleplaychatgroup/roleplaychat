package com.roleplaychat.backend.repo;

import com.roleplaychat.backend.model.GroupUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface GroupUserRepository extends JpaRepository<GroupUser, Long> {
    public List<GroupUser> findGroupUsersByUser_UserId(Long userId);
    public List<GroupUser> findGroupUsersByGroup_GroupId(Long groupId);
    @Transactional
    public void deleteGroupUsersByGroup_GroupId(Long groupId);
}
