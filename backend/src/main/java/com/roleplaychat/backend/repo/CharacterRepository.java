package com.roleplaychat.backend.repo;

import com.roleplaychat.backend.model.Character;
import com.roleplaychat.backend.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public interface CharacterRepository extends JpaRepository<Character, Long> {
    Set<Character> findCharactersByUser(User user);
}
