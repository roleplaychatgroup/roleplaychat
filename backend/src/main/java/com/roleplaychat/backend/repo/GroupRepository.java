package com.roleplaychat.backend.repo;

import com.roleplaychat.backend.model.Group;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GroupRepository extends JpaRepository<Group, Long> {
    Boolean existsByGroupName(String groupName);
}
