import "./index.css";
import React from "react";
import {Route, Routes} from "react-router-dom";
import Welcome from "./pages/authentication/components/Welcome";
import Login from "./pages/authentication/components/Login";
import Register from "./pages/authentication/components/Register";
import Chat from "./pages/chat";
import {Container} from "react-bootstrap";
import Profile from "./pages/profile";
import CreateCharacter from "./pages/charactercreate";
import ChatCreationContainer from "./pages/chat/containers/ChatCreationContainer";
import ChatSettings from "./pages/chat/components/ChatSettings";
import ChatInfoContainer from "./pages/chatinfo/containers/ChatInfoContainer";

function App() {
  return (
      <Container>
          <Routes>
              <Route path="/" element={<Welcome/>} />
              <Route path="/login" element={<Login/>} />
              <Route path="/register" element={<Register/>} />
              <Route path="/chat" element={<Chat/>} />
              <Route path="/chat/create" element={<ChatCreationContainer/>} />
              <Route path="/chat/settings/:chatId" element={<ChatSettings/>} />
              <Route path="/user/edit" element={<Profile/>} />
              <Route path="/character/create" element={<CreateCharacter/>} />
              <Route path="/chat/info/:chatId" element={<ChatInfoContainer/>} />
          </Routes>
      </Container>
  );
}

export default App;
