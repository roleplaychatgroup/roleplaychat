import logo from "../../../resources/d20.png";
import Circle from "./Circle";

function Logo(props) {
    return (
        <Circle  width={props.width} height={props.height}>
            <img src={logo} alt='Logo' width={props.width} height={props.height}/>
        </Circle>
    )
}

export default Logo;