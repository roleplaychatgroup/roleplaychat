import {useNavigate} from "react-router-dom";
import React, {useState} from "react";
import {Formik, Field, Form, ErrorMessage} from "formik";
import * as Yup from 'yup';
import {Alert, Col, Container, Row} from "react-bootstrap";

import AppHeader from "./AppHeader";
import ResizableRoundedButton from "./ResizableRoundedButton";
import ResizableLabel from "./ResizableLabel";
import AuthService from "../services/auth.service";

import "../styles/Login.css"
import "../styles/Fonts.css"

function Login() {
    const WarningAlert = (props) => <Alert variant={"warning"} className="error-message">{props.children}</Alert>;
    const DangerAlert = (props) => <Alert variant={"danger"} className="error-message">{props.children}</Alert>;

    let navigate = useNavigate();
    const [loading, setLoading] = useState(false);
    const [message, setMessage] = useState('');
    const handleLogin = (values) => {
        setLoading(true);
        setMessage('');
        AuthService.login(values.email, values.password).then(
            (response) => {
                setLoading(false);
                setMessage('');
                navigate("/chat");
                window.location.reload();
            },
            (error) => {
                if (error.response && error.response.status === 401) {
                    setMessage('Invalid email or password');
                } else if (error.response) {
                    setMessage('Network error');
                } else {
                    const resMessage =
                        (error.response.data &&
                            error.response.data.message) ||
                        error.message ||
                        error.toString();
                    setMessage(resMessage);
                }
                setLoading(false);
            }
        );
    };
    return (
        <Container className="main-container">
            <AppHeader/>
            <Formik
                initialValues={{email: '', password: ''}}
                validationSchema={Yup.object({
                    email: Yup.string().email('Invalid email address').required('Required'),
                    password: Yup.string()
                        .min(6, 'Password is too short - should be 6 chars minimum.')
                        .max(20, 'Password is too long - should be 20 chars maximum.')
                        .matches(/^[A-Za-z0-9]+$/, 'Password can only contain Latin letters and digital numbers.')
                        .required('Required')
                })}
                onSubmit={(values, actions) => {
                    handleLogin(values);
                    actions.setSubmitting(false);
                }}
            >
                <Container className="form-container">
                    <Form className="login-form">
                        <h1 className="login-header big-font">Вход</h1>
                        <Container className="input-container">
                            <Row className="on-center">
                                <Col>
                                    <ResizableLabel className="normal-font" width="76px"
                                                    height="24px" htmlFor="email">
                                        E-mail:
                                    </ResizableLabel>
                                </Col>
                                <Col>
                                    <Field className="form-input" name="email" type="email"/>
                                </Col>
                            </Row>
                            <ErrorMessage component={WarningAlert} name="email"/>

                            <Row className="on-center">
                                <Col>
                                    <ResizableLabel className="password-label normal-font" width="76px"
                                                    height="24px" htmlFor="password">
                                        Пароль:
                                    </ResizableLabel>
                                </Col>
                                <Col>
                                    <Field className="form-input" name="password" type="password"/>
                                </Col>
                            </Row>
                            <ErrorMessage component={WarningAlert} name="password"/>
                        </Container>

                        <Container className="button-container">
                            {message ? (
                                <DangerAlert>{message}</DangerAlert>
                            ) : null}
                            <ResizableRoundedButton className="dark-grey big-font" width="112px" height=" 72px"
                                                    radius="10px" type="submit" disabled={loading}>
                                {loading ? (
                                    <span className="spinner-border spinner-border-sm"/>
                                ) : "Войти"}
                            </ResizableRoundedButton>
                            <ResizableRoundedButton className="light-grey normal-font" width="235px" height="44px"
                                                    radius="10px" onClick={() => {
                                navigate("/register")
                            }}>
                                Зарегистрироваться
                            </ResizableRoundedButton>
                        </Container>
                    </Form>
                </Container>
            </Formik>
        </Container>
    );
}

export default Login;
