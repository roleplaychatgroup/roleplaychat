import {Button} from "react-bootstrap";
import React from "react";

function ResizableRoundedButton(props) {
    const style = {
        width: props.width,
        height: props.height,
        borderRadius: props.radius
    };

    return (
        <Button
            {...props}
            style={style}
            variant="light"
        >
            {props.children}
        </Button>
    )
}

export default ResizableRoundedButton;
