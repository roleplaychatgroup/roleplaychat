function ResizableLabel(props) {
    const style = {width: props.width, height: props.height};

    return (
        <label
            {...props}
            style={style}
        >
            {props.children}
        </label>
    )
}

export default ResizableLabel;