import React, {useState} from "react";
import {useNavigate} from "react-router-dom";
import {Alert, Col, Container, Row} from "react-bootstrap";
import * as Yup from "yup";
import {ErrorMessage, Field, Formik, Form} from "formik";

import AppHeader from "./AppHeader";
import ResizableRoundedButton from "./ResizableRoundedButton";
import ResizableLabel from "./ResizableLabel";
import AuthService from "../services/auth.service";

import "../styles/Register.css";
import "../styles/Fonts.css"

function Register() {
    const RightMarginCol = (props) => <Col style={{marginRight: props.mg}}>{props.children}</Col>
    const NowrapRow = (props) => <Row style={{flexWrap: "nowrap"}}>{props.children}</Row>
    const SuccessAlert = (props) => <Alert variant={"success"} className="error-message">{props.children}</Alert>;
    const WarningAlert = (props) => <Alert variant={"warning"} className="error-message">{props.children}</Alert>;
    const DangerAlert = (props) => <Alert variant={"danger"} className="error-message">{props.children}</Alert>;

    let navigate = useNavigate();
    const [loading, setLoading] = useState(false);
    const [successful, setSuccessful] = useState(false);
    const [message, setMessage] = useState('');
    const handleRegister = (values) => {
        setLoading(true);
        setSuccessful(false);
        setMessage('');
        AuthService.register(values.username, values.email, values.password, values.gender).then(
            (response) => {
                setLoading(false);
                setSuccessful(true);
                setMessage('');
            },
            (error) => {
                if (error.response && error.response.status === 401) {
                    setMessage('Invalid email or password');
                } else if (error.response) {
                    setMessage('Network error');
                } else {
                    const resMessage =
                        (error.response.data &&
                            error.response.data.message) ||
                        error.message ||
                        error.toString();
                    setMessage(resMessage);
                }
                setLoading(false);
                setSuccessful(false);
            }
        );
    };
    return (
        <Container className="main-container">
            <AppHeader/>
            <Formik
                initialValues={{username: '', email: '', password: '', gender: ''}}
                validationSchema={Yup.object({
                    username: Yup.string()
                        .min(3, 'Username is too short - should be 3 chars minimum.')
                        .max(30, 'Username is too long - should be 30 chars maximum.')
                        .required('Required'),
                    email: Yup.string()
                        .email('Invalid email address')
                        .required('Required'),
                    password: Yup.string()
                        .min(6, 'Password is too short - should be 6 chars minimum.')
                        .max(20, 'Password is too long - should be 20 chars maximum.')
                        .matches(/^[A-Za-z0-9]+$/, 'Password can only contain Latin letters and digital numbers.')
                        .required('Required'),
                    gender: Yup.string()
                        .required('Required')
                })}
                onSubmit={(values, actions) => {
                    handleRegister(values);
                    actions.setSubmitting(false);
                }}
            >
                {!successful ? (
                    <Form className="register-form">
                        <h1 className="register-header big-font">Регистрация</h1>
                        <Container className="input-container">
                            <Row className="on-center">
                                <Col>
                                    <ResizableLabel className="normal-font" width="175px"
                                                    height="24px" htmlFor="username">
                                        Имя пользователя:
                                    </ResizableLabel>
                                </Col>
                                <Col>
                                    <Field className="form-input" name="username" type="text"/>
                                </Col>
                            </Row>
                            <ErrorMessage component={WarningAlert} name="username"/>

                            <Row className="on-center">
                                <Col>
                                    <ResizableLabel className="normal-font" width="76px"
                                                    height="24px" htmlFor="email">
                                        E-mail:
                                    </ResizableLabel>
                                </Col>
                                <Col>
                                    <Field className="form-input" name="email" type="email"/>
                                </Col>
                            </Row>
                            <ErrorMessage component={WarningAlert} name="email"/>

                            <Row className="on-center">
                                <Col>
                                    <ResizableLabel className="password-label normal-font" width="76px"
                                                    height="24px" htmlFor="password">
                                        Пароль:
                                    </ResizableLabel>
                                </Col>
                                <Col>
                                    <Field className="form-input" name="password" type="password"/>
                                </Col>
                            </Row>
                            <ErrorMessage component={WarningAlert} name="password"/>
                        </Container>

                        <Container className="gender-container">
                            <NowrapRow>
                                <Col>
                                    <ResizableLabel className="gender-label normal-font" width="44px"
                                                    height="24px">
                                        Пол:
                                    </ResizableLabel>
                                </Col>
                                <Col>
                                    <Field name="gender" type="radio" value="male"/>
                                </Col>
                                <RightMarginCol mg="185px">
                                    <ResizableLabel className="male-label normal-font" width="76px"
                                                    height="24px">
                                        Мужской
                                    </ResizableLabel>
                                </RightMarginCol>
                            </NowrapRow>

                            <NowrapRow>
                                <Col>
                                    <Field name="gender" type="radio" value="female"/>
                                </Col>
                                <RightMarginCol mg="185px">
                                    <ResizableLabel className="female-label normal-font" width="76px"
                                                    height="24px">
                                        Женский
                                    </ResizableLabel>
                                </RightMarginCol>
                            </NowrapRow>

                            <NowrapRow>
                                <Col>
                                    <Field name="gender" type="radio" value="not_stated"/>
                                </Col>
                                <RightMarginCol mg="153px">
                                    <ResizableLabel className="not-stated-label normal-font" width="108px"
                                                    height="24px">
                                        Не указано
                                    </ResizableLabel>
                                </RightMarginCol>
                            </NowrapRow>
                            <ErrorMessage component={WarningAlert} name="gender"/>
                        </Container>

                        <Container className="button-container">
                            {message ? (
                                <DangerAlert>{message}</DangerAlert>
                            ) : null}

                            <ResizableRoundedButton className="dark-grey big-font" width="300px" height="72px"
                                                    radius="10px" type="submit" disabled={loading}>
                                {loading ? (
                                    <span className="spinner-border spinner-border-sm"/>
                                ) : "Зарегистрироваться"}
                            </ResizableRoundedButton>

                            <ResizableRoundedButton className="light-grey normal-font" width="94px" height="44px"
                                                    radius="10px"
                                                    onClick={() => {
                                                        navigate("/login")
                                                    }}>
                                Войти
                            </ResizableRoundedButton>
                        </Container>
                    </Form>
                ) : (
                    <Container className="success-container">
                        <SuccessAlert>Success!</SuccessAlert>
                        <ResizableRoundedButton className="dark-grey big-font" width="300px" height="72px"
                                                radius="10px"
                                                onClick={() => {
                                                    navigate("/login")
                                                }}>
                            Войти
                        </ResizableRoundedButton>
                    </Container>
                )
                }
            </Formik>
        </Container>
    );
}

export default Register;
