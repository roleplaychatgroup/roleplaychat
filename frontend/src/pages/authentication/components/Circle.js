function Circle(props) {
    const style = {
        display: "flex",
        width: props.width,
        height: props.height,
        backgroundColor: "#B2C0D4",
        borderRadius: "50%"
    };

    return (
        <div style={style}>
            {props.children}
        </div>
    )
}

export default Circle;