import {Col, Container, Row} from "react-bootstrap";

import "../styles/AppHeader.css"
import Logo from "./Logo";

function AppHeader() {
    return (
        <Container className="title-container">
            <Row xs="auto">
                <Col>
                    <Logo width="48px" height="48px"/>
                </Col>
                <Col>
                    <h1 className='small-title'>RPGChat</h1>
                </Col>
            </Row>
        </Container>
    )
}

export default AppHeader;
