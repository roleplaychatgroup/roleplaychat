import { useNavigate } from "react-router-dom";
import {Col, Container, Row} from "react-bootstrap";

import "../styles/Fonts.css"
import "../styles/Welcome.css"
import ResizableRoundedButton from "./ResizableRoundedButton";
import Logo from "./Logo";

function Welcome() {
    let navigate = useNavigate();

    return (
        <Container className="welcome-container">
            <Logo width="148px" height="148px"/>
            <h1 className='big-title'>RPGChat</h1>
            <Row>
                <Col>
                    <ResizableRoundedButton className="light-grey normal-font" width="235px" height="64px" radius="10px" onClick={ () => {navigate("/login")} }>
                        Войти
                    </ResizableRoundedButton>
                </Col>
                <Col>
                    <ResizableRoundedButton className="light-grey normal-font" width="235px" height="64px" radius="10px" onClick={ () => {navigate("/register")} }>
                        Зарегистрироваться
                    </ResizableRoundedButton>
                </Col>
            </Row>
        </Container>
    );
}

export default Welcome;