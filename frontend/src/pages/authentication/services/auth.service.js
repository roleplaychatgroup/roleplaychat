import axios from "axios";
const API_URL = "http://84.252.143.105:8081/api/auth/";

const register = (username, email, password, gender) => {
    return axios.post(API_URL + "signup", {
        username,
        email,
        password,
        gender,
    });
};

const login = (email, password) => {
    return axios
        .post(API_URL + "signin", {
            email,
            password,
        })
        .then((response) => {
            if (response.data.token) {
                localStorage.setItem("user", JSON.stringify(response.data));
            }
            return response.data;
        });
};

const logout = () => {
    localStorage.removeItem("user");
};

const getCurrentUser = () => {
    return JSON.parse(localStorage.getItem("user"));
};

const updateUserInLocalStorage = (name, email, gender) => {
    const currentUser = JSON.parse(localStorage.getItem("user"));
    currentUser.name = name || currentUser.name;
    currentUser.email = email || currentUser.email;
    currentUser.gender = gender || currentUser.gender;
    localStorage.setItem("user", JSON.stringify(currentUser));
}

const AuthService = {
    register,
    login,
    logout,
    getCurrentUser,
    updateUserInLocalStorage,
};

export default AuthService;
