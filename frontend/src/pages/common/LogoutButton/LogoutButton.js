import {useNavigate} from "react-router-dom";
import logout from "../../../resources/logout-icon.png";
import './style.css';

function LogoutButton() {
    let navigate = useNavigate();

    const handleLogout = () => {
        localStorage.removeItem("user");
        navigate('../');
    };

    return (
        <img onClick={handleLogout} className='logout-button' src={logout} alt='logout-button' />
    )
}

export default LogoutButton;
