import axios from "axios";
import authHeader from "../../authentication/services/auth-header";

const API_URL = "http://84.252.143.105:8081/api/";

const getUserInfo = (id) => {
    return axios.get(API_URL + `user/info?user_id=${id}`, {headers: authHeader()});
}

const updateUserInfo = (name, oldPassword, newPassword, gender, email) => {
    return axios.post(API_URL + "user/edit", {
            name,
            oldPassword,
            newPassword,
            gender,
            email,
        },
        {headers: authHeader()});
}

const ProfileService = {
    getUserInfo,
    updateUserInfo,
};

export default ProfileService;