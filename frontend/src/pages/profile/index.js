import AppHeader from "../authentication/components/AppHeader";
import {ErrorMessage, Field, Form, Formik} from "formik";
import * as Yup from "yup";
import {Alert, Col, Container, Row} from "react-bootstrap";
import ResizableLabel from "../authentication/components/ResizableLabel";
import ResizableRoundedButton from "../authentication/components/ResizableRoundedButton";
import authService from "../authentication/services/auth.service";
import {useState, React} from "react";
import ProfileService from "./services/ProfileService";

import "./index.css"
import "../authentication/styles/Fonts.css"
import AuthService from "../authentication/services/auth.service";
import {useNavigate} from "react-router-dom";

function Profile() {
    const navigate = useNavigate();

    const RightMarginCol = (props) => <Col style={{marginRight: props.mg}}>{props.children}</Col>
    const NowrapRow = (props) => <Row style={{flexWrap: "nowrap"}}>{props.children}</Row>
    const WarningAlert = (props) => <Alert variant={"warning"} className="error-message">{props.children}</Alert>;
    const [message, setMessage] = useState(null);
    const [loading, setLoading] = useState(false);
    const [user, setUser] = useState(authService.getCurrentUser);

    const handleUpdate = (values) => {
        setLoading(true);
        setMessage(null);
        ProfileService.updateUserInfo(values.name, values.oldPassword, values.newPassword, values.gender, values.email).then(
            (response) => {
                ProfileService.getUserInfo(user.id).then(
                    (response) => {
                        setLoading(false);
                        setMessage({text: 'Success!', type: 'success'});
                        AuthService.updateUserInLocalStorage(response.data.name, response.data.email, response.data.gender)
                        setUser(authService.getCurrentUser);
                    },
                    (error) => {
                        setLoading(false);
                        setMessage({text: 'Cant get information about user', type: 'danger'});
                    }
                )
            },
            (error) => {
                if (error.response) {
                    if (error.response.status === 401) {
                        setMessage({text: 'Invalid email or password', type: 'danger'});
                    } else if (error.response.status === 403) {
                        setMessage({text: 'Access denied!', type: 'danger'});
                    } else {
                        setMessage({text: error.response.data || error.response, type: 'danger'});
                    }
                } else {
                    const resMessage =
                        (error.response.data &&
                            error.response.data.message) ||
                        error.message ||
                        error.toString();
                    setMessage({text: resMessage, type: 'danger'});
                }
                setLoading(false);
            }
        );
    };

    return (
        <Container className="edit-main-container">
            <AppHeader/>
            <Formik
                initialValues={{
                    name: user.name,
                    oldPassword: '',
                    newPassword: '',
                    email: user.email,
                    gender: user.gender
                }}
                validationSchema={Yup.object({
                    name: Yup.string()
                        .min(3, 'Username is too short - should be 3 chars minimum.')
                        .max(30, 'Username is too long - should be 30 chars maximum.'),
                    newPassword: Yup.string()
                        .min(6, 'Password is too short - should be 6 chars minimum.')
                        .max(20, 'Password is too long - should be 20 chars maximum.')
                        .matches(/^[A-Za-z0-9]+$/, 'Password can only contain Latin letters and digital numbers.'),
                    email: Yup.string()
                        .email('Invalid email address'),
                    gender: Yup.string()
                        .required('Required'),
                    oldPassword: Yup.string()
                        .min(6, 'Password is too short - should be 6 chars minimum.')
                        .max(20, 'Password is too long - should be 20 chars maximum.')
                        .matches(/^[A-Za-z0-9]+$/, 'Password can only contain Latin letters and digital numbers.')
                        .required('Required'),
                })}
                onSubmit={(values, actions) => {
                    handleUpdate(values);
                    actions.setSubmitting(false);
                }}>
                <Form className="edit-form">
                    <h1 className="edit-header big-font">Профиль</h1>
                    <Container className="edit-input-container">
                        <Row className="edit-on-center">
                            <Col>
                                <ResizableLabel className="normal-font" htmlFor="name">
                                    Имя пользователя:
                                </ResizableLabel>
                            </Col>
                            <Col>
                                <Field className="edit-form-input" name="name" type="text"/>
                            </Col>
                        </Row>
                        <ErrorMessage component={WarningAlert} name="name"/>

                        <Row className="edit-on-center">
                            <Col>
                                <ResizableLabel className="normal-font" htmlFor="newPassword">
                                    Новый пароль:
                                </ResizableLabel>
                            </Col>
                            <Col>
                                <Field className="edit-form-input" name="newPassword" type="password"/>
                            </Col>
                        </Row>
                        <ErrorMessage component={WarningAlert} name="newPassword"/>

                        <Row className="edit-on-center">
                            <Col>
                                <ResizableLabel className="normal-font" htmlFor="email">
                                    E-mail:
                                </ResizableLabel>
                            </Col>
                            <Col>
                                <Field className="edit-form-input" name="email" type="email"/>
                            </Col>
                        </Row>
                        <ErrorMessage component={WarningAlert} name="email"/>

                        <Container className="gender-container">
                            <NowrapRow>
                                <Col>
                                    <ResizableLabel className="gender-label normal-font" width="44px"
                                                    height="24px">
                                        Пол:
                                    </ResizableLabel>
                                </Col>
                                <Col>
                                    <Field name="gender" type="radio" value="male"/>
                                </Col>
                                <RightMarginCol mg="185px">
                                    <ResizableLabel className="male-label normal-font" width="76px"
                                                    height="24px">
                                        Мужской
                                    </ResizableLabel>
                                </RightMarginCol>
                            </NowrapRow>

                            <NowrapRow>
                                <Col>
                                    <Field name="gender" type="radio" value="female"/>
                                </Col>
                                <RightMarginCol mg="185px">
                                    <ResizableLabel className="female-label normal-font" width="76px"
                                                    height="24px">
                                        Женский
                                    </ResizableLabel>
                                </RightMarginCol>
                            </NowrapRow>

                            <NowrapRow>
                                <Col>
                                    <Field name="gender" type="radio" value="not_stated"/>
                                </Col>
                                <RightMarginCol mg="153px">
                                    <ResizableLabel className="not-stated-label normal-font" width="108px"
                                                    height="24px">
                                        Не указано
                                    </ResizableLabel>
                                </RightMarginCol>
                            </NowrapRow>
                            <ErrorMessage component={WarningAlert} name="gender"/>
                        </Container>

                        <Row className="edit-on-center">
                            <Col>
                                <ResizableLabel className="normal-font" htmlFor="oldPassword">
                                    Введите старый пароль<br/>для подтверждения:
                                </ResizableLabel>
                            </Col>
                            <Col>
                                <Field className="edit-form-input" name="oldPassword" type="password"/>
                            </Col>
                        </Row>
                        <ErrorMessage component={WarningAlert} name="oldPassword"/>
                    </Container>

                    {message ? (
                        <Alert variant={message.type} className="error-message">{message.text}</Alert>
                    ) : null}

                    <ResizableRoundedButton className="dark-grey big-font" radius="10px" type="submit"
                                            disabled={loading}>
                        {loading ? (
                            <span className="spinner-border spinner-border-sm"/>
                        ) : "Применить"}
                    </ResizableRoundedButton>
                    <ResizableRoundedButton className="light-grey normal-font" radius="10px" onClick={() => {
                        navigate("/chat")
                    }}>
                        Назад
                    </ResizableRoundedButton>
                </Form>
            </Formik>
        </Container>
    )
}

export default Profile;