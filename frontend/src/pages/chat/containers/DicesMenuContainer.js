import DicesMenu from "../components/DicesMenu";

function DicesMenuContainer(props) {

    return (
        <DicesMenu onSend={props.onSend} isShown={props.isShown} />
    )
}

export default DicesMenuContainer;
