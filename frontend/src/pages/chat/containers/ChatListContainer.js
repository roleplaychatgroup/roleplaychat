import { useCallback, useEffect, useState } from "react";
import ChatList from "../components/ChatList";
import chatsApi from "../services/ChatService";

function ChatListContainer(props) {

    const [chats, setChats] = useState([]);

    useEffect(() => {
        const currentUserId = JSON.parse(localStorage.getItem("user")).id;

        chatsApi.getUserChats(currentUserId).then((response) => {
            setChats(response.data);
        });
    }, []);
    
    const selectChat = useCallback((chatId) => {
        chatsApi.getChatMessages(chatId).then((response) => {
            if (response.data) {
                props.setMessages(response.data);
            } else {
                props.setMessages([]);
            }
            props.setChatOpened(true);
            const selectedChat = chats.filter((chat) => chat.groupId === chatId)[0];
            props.setChat(selectedChat);
        });
    }, [props, chats]);
    
    useEffect(() => {
        const updateInterval = setInterval(() => {
            if (props.currentChatId) {
                selectChat(props.currentChatId);
            }  
        }, 2000);

        return () => clearInterval(updateInterval);
    }, [props.currentChatId, selectChat]);

    return (
        <ChatList
            selectChat={selectChat}
            chats={chats}
            currentChatId={props.currentChatId}
        />
    )
}

export default ChatListContainer;
