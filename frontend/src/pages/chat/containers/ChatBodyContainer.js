import ChatBody from "../components/ChatBody";

function ChatBodyContainer(props) {

    return (
        <ChatBody messages={props.messages} />
    )
}

export default ChatBodyContainer;
