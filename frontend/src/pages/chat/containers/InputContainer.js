import { useCallback, useEffect, useState } from 'react';
import Input from "../components/Input";

function InputContainer(props) {
    const currentUserId = JSON.parse(localStorage.getItem("user")).id;
    const currentUserGender = JSON.parse(localStorage.getItem("user")).gender;

    const [message, setMessage] = useState('');

    const onSend = useCallback((text) => {
        if (text) {
            props.sendMessage({
                text,
                userId: currentUserId,
                sendTime: new Date(),
                gender: currentUserGender,
            });
            setMessage('');
            return;
        }    
        if (message === '') {
            return;
        }
        props.sendMessage({
            text: message,
            userId: currentUserId,
            sendTime: new Date(),
            gender: currentUserGender,
        });
        setMessage('');
    }, [props, message, currentUserId, currentUserGender]);

    useEffect(() => {
        const listener = event => {
            if (event.code === "Enter" || event.code === "NumpadEnter") {
                event.preventDefault();
                onSend(message);
            }
        };
        document.addEventListener("keydown", listener);
        return () => {
            document.removeEventListener("keydown", listener);
        };
    }, [message, onSend]);

    return (
        <Input message={message} onSend={onSend} setMessage={setMessage} setCharacter={props.setCharacter}/>
    )
}

export default InputContainer;
