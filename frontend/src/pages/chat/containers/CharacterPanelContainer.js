import { useNavigate } from "react-router-dom";
import CharacterPanel from "../components/CharacterPanel";

function CharacterMenuContainer(props) {
    const navigate = useNavigate();

    const handleOpenSettings = () => {
        navigate('/user/edit');
    }

    const handleCreateCharacter = () => {
        navigate('/character/create');
    }

    return (
        <CharacterPanel
            openSettings={handleOpenSettings}
            openCreateCharacter={handleCreateCharacter}
            character={props.character}
        />
    )
}

export default CharacterMenuContainer;
