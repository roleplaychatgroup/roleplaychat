import React from "react";
import { useEffect, useState } from "react";
import CharactersMenu from "../components/CharactersMenu";
import characterApi from "../services/CharacterService";


function CharactersMenuContainer(props) {
  const [characters, setCharacters] = useState([]);

  const onCharacterPick = (character) => {
    if (character.name === 'Сбросить персонажа') {
      props.setCharacter({});
      characterApi.getCharacters().then((response) => {
        setCharacters([...response.data, { characterName: 'Сбросить персонажа', characterId: -1 }]
          .sort(
            ({ characterId: id1 }, { characterId: id2 }) => {
              if (id1 > id2) return -1;
              if (id2 > id1) return 1;
              return 0;
            }
          ));
      });
      return;
    }
    props.setCharacter(character);
    characterApi.getCharacters().then((response) => {
      setCharacters([...response.data, { characterName: 'Сбросить персонажа', characterId: -1 }]
        .sort(
          ({ characterId: id1 }, { characterId: id2 }) => {
            if (id1 > id2) return -1;
            if (id2 > id1) return 1;
            return 0;
          }
        ));
    });
  }

  useEffect(() => {
    characterApi.getCharacters().then((response) => {
      setCharacters([...response.data, { characterName: 'Сбросить персонажа', characterId: -1 }]);
    });
  },
    []);

  return (
    <CharactersMenu
      characters={characters}
      onSend={props.onPick}
      isShown={props.isShown}
      onCharacterPick={onCharacterPick}
    />
  )
}

export default React.memo(CharactersMenuContainer);
