import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import ChatHeader from "../components/ChatHeader";
import chatsApi from "../services/ChatService";

function ChatHeaderContainer(props) {

    const navigate = useNavigate();

    const currentUserId = JSON.parse(localStorage.getItem("user")).id;

    const [isAdmin, setIsAdmin] = useState(false);

    useEffect(() => {
        chatsApi.getChatInfo(props.chat.groupId).then(
            (response) => {
                setIsAdmin(response.data.adminId === currentUserId);
            }
        ).catch();
    }, [currentUserId, setIsAdmin, props.chat.groupId]);

    const onInfoClick = () => {
        navigate(`/chat/info/${props.chat.groupId}`);
    }

    const onSettingsClick = () => {
        navigate(`/chat/settings/${props.chat.groupId}`);
    }

    return (
        <ChatHeader
            title={props.chat.groupName}
            onInfoClick={onInfoClick}
            onSettingsClick={onSettingsClick}
            isAdmin={isAdmin}
        />
    )
}

export default ChatHeaderContainer;
