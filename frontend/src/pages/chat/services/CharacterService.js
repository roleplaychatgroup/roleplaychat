import axios from "axios";
import authHeader from '../../authentication/services/auth-header.js';

const API_URL = "http://84.252.143.105:8081/api/";



const config = {
  headers: authHeader(),
}

const getCharacters = (userId) => {
  return axios
    .get(API_URL + "character/list", config);
};

const incrementCharacterHP = (characterId) => {
  return axios
    .post(API_URL + `character/life/add?character_id=${characterId}`, {}, config);
};

const decrementCharacterHP = (characterId) => {
  return axios
    .post(API_URL + `character/life/delete?character_id=${characterId}`, {}, config);
};

const characterApi = {
  getCharacters,
  incrementCharacterHP,
  decrementCharacterHP,
};

export default characterApi;
