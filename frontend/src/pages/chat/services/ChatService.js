import axios from "axios";
import authHeader from '../../authentication/services/auth-header.js';

const API_URL = "http://84.252.143.105:8081/api/";


const config = {
    headers: authHeader(),
}

const getUserChats = (userId) => {
    return axios
        .get(API_URL + "users/" + userId + "/groups", config);
};

const getChatMessages = (chatId) => {
    return axios
        .get(API_URL + "groups/" + chatId + "/messages", config);
};

const getChatInfo = (chatId) => {
    return axios
        .get(API_URL + "groups/" + chatId, config);
};

const sendMessage = (chatId, message) => {
    return axios
        .post(API_URL + "groups/" + chatId + "/messages", message, config);
}

const createCharacter = (characterName, characterGender) => {
    return axios.post(API_URL + "character", {
            characterName,
            characterGender,
        },
        config);
}

const createChat = (chatName, emails, adminId) => {
    return axios.post(API_URL + "groups", {
            groupName: chatName,
            userList: emails,
            adminId
        },
        config);
}

const changeChat = (chatName, emails, adminId, groupId) => {
    return axios.patch(API_URL + `groups/${groupId}`, {
            groupName: chatName,
            userList: emails,
            adminId
        },
        config);
}

const chatsApi = {
    getUserChats,
    getChatMessages,
    sendMessage,
    createCharacter,
    createChat,
    getChatInfo,
    changeChat,
};

export default chatsApi;
