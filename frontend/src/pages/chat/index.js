import React, { useCallback, useEffect, useState } from 'react';
import ChatBodyContainer from './containers/ChatBodyContainer';
import ChatHeaderContainer from './containers/ChatHeaderContainer';
import ChatListContainer from './containers/ChatListContainer';
import InputContainer from './containers/InputContainer';
import './styles/chat.css'
import chatsApi from './services/ChatService';
import CharacterPanelContainer from './containers/CharacterPanelContainer';
import ChatClosed from './components/ChatClosed';

function Chat() {
  const [messages, setMessages] = useState([]);
  const [chat, setChat] = useState({});
  const [isChatOpen, setIsChatOpen] = useState(false);

  const [chosenCharacter, setChosenCharacter] = useState({});

  const escFunction = useCallback((event) => {
    if (event.key === "Escape") {
      setIsChatOpen(false);
      setChat({});
    }
  }, []);

  useEffect(() => {
    document.addEventListener("keydown", escFunction, false);

    return () => {
      document.removeEventListener("keydown", escFunction, false);
    };
  }, [escFunction]);

  const sendMessage = useCallback((message) => {
    const currentUserName = JSON.parse(localStorage.getItem("user")).name;

    setMessages((oldMessages) => {
      return [...oldMessages,
      {
        text: message.text,
        userId: message.userId,
        name: currentUserName,
        gender: message.gender,
        characterName: chosenCharacter.name,
        characterGender: chosenCharacter.gender,
        characterId: chosenCharacter.id,
      }
      ]
    });

    if (chosenCharacter) {
      chatsApi.sendMessage(chat.groupId,
        {
          ...message,
          characterId: chosenCharacter.id,
        })
        .then((response) => {
        })
        .catch(error => {
          console.log(error);
        });

      return;
    }

    chatsApi.sendMessage(chat.groupId, message)
      .then((response) => {
      })
      .catch(error => {
        console.log(error);
      });
  }, [chosenCharacter, chat.groupId]);

  return (
    <div className='chat-page'>
      <div className='chat-list'>
        <ChatListContainer currentChatId={chat?.groupId} setChat={setChat} setChatOpened={setIsChatOpen} setMessages={setMessages} />
      </div>
      {isChatOpen ?
        <div className='chat'>
          <ChatHeaderContainer chat={chat} />
          <ChatBodyContainer messages={messages} />
          <InputContainer sendMessage={sendMessage} setCharacter={setChosenCharacter} />
        </div>
        :
        <ChatClosed />
      }
      <div className='character-menu'>
        <CharacterPanelContainer character={chosenCharacter} />
      </div>
    </div>
  )
}

export default React.memo(Chat);
