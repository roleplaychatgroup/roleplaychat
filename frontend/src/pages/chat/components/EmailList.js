import "../styles/character-creation.css"
import "../../authentication/styles/Fonts.css"
import cross from "../../../resources/cross-icon.png";
import ResizableLabel from "../../authentication/components/ResizableLabel";
import ResizableRoundedButton from "../../authentication/components/ResizableRoundedButton";
import React, { useCallback, useState } from "react";

function EmailList(props) {

    const [input, setInput] = useState();

    const onChange = useCallback((event) => {
        setInput(event.target.value);
    }, [setInput]);

    const addEmail = useCallback(() => {
        props.setEmails([
            ...props.emails,
            input,
        ]);
    }, [input, props]);

    return (
        <div>
            <div>
                <ResizableLabel className="normal-font" htmlFor="name">
                    Пригласите по email:
                </ResizableLabel>
                <input className="character-create-form-input" onChange={onChange}></input>
            </div>
            <div style={{display: 'flex', marginLeft: "200px", marginTop: "10px"}}>
                <ResizableRoundedButton className="light-grey normal-font" radius="10px" onClick={addEmail}>
                    Добавить
                </ResizableRoundedButton>
            </div>
            <div style={{display: 'flex', flexDirection: 'column'}}>
                {props.emails.map((email, index) => {
                    return (
                        <div key={email} className="email">
                            {/* {`${index + 1}.`}
                            <div style={{marginLeft: '10px'}} /> */}
                            <div key={index}>{email}</div>
                            <img src={cross} onClick={() => {props.onRemoveUser(index)}} className="cross-icon" alt="delete user"/>
                        </div>
                    );
                })}
            </div>
        </div>
    )
}

export default React.memo(EmailList);
