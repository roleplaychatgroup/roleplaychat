import { useEffect } from "react";
import "../styles/chat-body.css";
import "../styles/message.css";
import EmptyChat from "./EmptyChat";
import Message from "./Message";

function ChatBody(props) {

    const currentUserId = JSON.parse(localStorage.getItem("user")).id;

    useEffect(() => {
        const scroller = document.getElementById('scroller');
        scroller.scrollTop = scroller.scrollHeight;
    }, [props?.messages?.length]);

    return (
        <div id='scroller' className='messages-container'>
            {props.messages.length === 0 && <EmptyChat />}
            {props.messages.map(message => {

                return (
                    <Message
                        key={message.text + message.sendTime}
                        message={message}
                        isOwn={message.userId === currentUserId}
                    />
                );
            })}
        </div>
    )
}

export default ChatBody;
