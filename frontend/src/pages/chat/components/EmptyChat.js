import "../styles/chat-body.css";

function EmptyChat() {

    return (
        <div className='empty-chat'>
            <p>Этот чат ещё пустой</p>
            <p>Будьте первым, кто отправит сообщение!</p>
        </div>
    )
}

export default EmptyChat;
