import React, { useCallback } from 'react';
import { useNavigate } from 'react-router-dom';
import logo from '../../../resources/d20.png';
import "../styles/chat-list.css";
import ListElement from "./ListElement";

function ChatList(props) {
    const navigate = useNavigate();

    const openCreateChat = useCallback(() => {
        navigate('/chat/create');
    }, [navigate]);

    return (
        <div>
            <div className='logo'>
                <img className='logo-icon' src={logo} alt='logo' />
                <b className='logo-text'>RPGChat</b>
            </div>
            {props.chats ?
            <div className='chats-box'>
                {
                    props.chats.map(chat =>
                        <ListElement
                            key={chat.groupId}
                            chat={chat}
                            onClick={() => {props.selectChat(chat.groupId);}}
                            isChosen={chat.groupId === props.currentChatId}
                        />
                    )
                }
                <ListElement
                    key={-1}
                    isAddChat={true}
                    openCreateChat={openCreateChat}
                />
            </div>
            :
            <div>There are no chats</div>
            }
        </div>
    )
}

export default React.memo(ChatList);
