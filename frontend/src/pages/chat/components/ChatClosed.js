import '../styles/chat.css'

function ChatClosed(props) {

    return <div className='no-chat'>
            <div className='no-chat-message'>
            Никакой чат не открыт
            </div>
        </div>
}

export default ChatClosed;
