import diceIcon from "../../../resources/dices-icon.png";
import characterIcon from "../../../resources/male-colored-icon.png";
import classNames from 'classnames';
import "../styles/input.css";
import DicesMenuContainer from "../containers/DicesMenuContainer";
import { useState } from "react";
import CharactersMenuContainer from "../containers/CharactersMenuContainer";

function Input(props) {
    const [diceMenuShown, setDiceMenuShown] = useState(false);
    const [charactersMenuShown, setCharactersMenuShown] = useState(false);

    const switchDiceMenu = () => {
        setDiceMenuShown(!diceMenuShown);
    }

    const switchCharactersMenu = () => {
        setCharactersMenuShown(!charactersMenuShown);
    }

    return (
        <>
            <DicesMenuContainer onSend={props.onSend} isShown={diceMenuShown} />
            <CharactersMenuContainer onPick={props.onPick} isShown={charactersMenuShown} setCharacter={props.setCharacter} />
            <div className='message-box'>
                <img onClick={switchCharactersMenu} src={characterIcon} className={classNames('button', 'character-button')} alt='choose character buton' />
                <input
                    autoComplete='false'
                    onChange={event => props.setMessage(event.target.value)}
                    value={props.message}
                    className='message-input'
                    placeholder='Введите сообщение...'
                />
                <img onClick={switchDiceMenu} src={diceIcon} className={classNames('button', 'dice-button')} alt='throw dices buton' />
            </div>
        </>
    )
}

export default Input;
