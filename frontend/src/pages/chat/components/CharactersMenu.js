import classNames from "classnames";
import "../styles/characters-menu.css"

function CharactersMenu(props) {
  return (
    <div className={classNames('characters-menu', { 'menu-hidden': !props.isShown })}>
      {props.characters.map((character) => {
        return <div
          onClick={() => {
            props.onCharacterPick({
              name: character.characterName,
              id: character.characterId,
              gender: character.characterGender,
              hp: character.lifeCount,
            })
          }}
          className="character-button"
          key={character.characterId}
        >
          {character.characterName}
        </div>
      })}
    </div>
  )
}

export default CharactersMenu;
