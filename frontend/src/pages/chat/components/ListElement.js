import classNames from 'classnames';
import "../styles/chat-list.css";

function ListElement(props) {

    return (
        (props.isAddChat ?
        <div onClick={props.openCreateChat} className={classNames('list-element')}>
            <b>+ Add Chat</b>
        </div>
            :
        <div onClick={props.onClick} className={classNames('list-element', {'list-element-chosen': props.isChosen})}>
            <b>{props.chat.groupName}</b>
        </div>
        )
    )
}

export default ListElement;
