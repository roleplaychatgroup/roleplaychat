import classNames from "classnames";
import "../styles/dices-menu.css"

function DicesMenu(props) {

    const rollDice = (diceSides) => {
        const result = Math.ceil(Math.random() * diceSides);

        props.onSend(`Throwing D${diceSides} = ${result}`);
    }

    return (
        <div className={classNames('dices-menu', {'menu-hidden': !props.isShown})}>
            <div onClick={() => rollDice(4)} className="dice-button">D4</div>
            <div onClick={() => rollDice(6)} className="dice-button">D6</div>
            <div onClick={() => rollDice(8)} className="dice-button">D8</div>
            <div onClick={() => rollDice(10)} className="dice-button">D10</div>
            <div onClick={() => rollDice(100)} className="dice-button">D%</div>
            <div onClick={() => rollDice(12)} className="dice-button">D12</div>
            <div onClick={() => rollDice(20)} className="dice-button">D20</div>
        </div>
    )
}

export default DicesMenu;
