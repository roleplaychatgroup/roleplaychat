import AppHeader from "../../authentication/components/AppHeader";
import {ErrorMessage, Field, Form, Formik} from "formik";
import * as Yup from "yup";
import {Alert, Col, Container, Row} from "react-bootstrap";
import ResizableLabel from "../../authentication/components/ResizableLabel";
import ResizableRoundedButton from "../../authentication/components/ResizableRoundedButton";
import {React, useEffect, useState} from "react";

import "../styles/character-creation.css"
import "../../authentication/styles/Fonts.css"
import ChatService from "../../chat/services/ChatService";
import {useLocation, useNavigate} from "react-router-dom";
import EmailList from "./EmailList";
import chatsApi from "../../chat/services/ChatService";

function ChatSettings() {
    const navigate = useNavigate();
    const currentUserId = JSON.parse(localStorage.getItem("user")).id;
    const currentUserEmail = JSON.parse(localStorage.getItem("user")).email;

    const WarningAlert = (props) => <Alert variant={"warning"} className="error-message">{props.children}</Alert>;
    const [message, setMessage] = useState(null);
    const [loading, setLoading] = useState(false);

    const [chatName, setChatName] = useState('');
    const [emails, setEmails] = useState([]);
    
    const onRemoveUser = (index) => {
        setEmails((oldEmails) => [...oldEmails.slice(0, index), ...oldEmails.slice(index + 1, oldEmails.length)])
    }

    const location = useLocation();
    const chatId = Number(location.pathname.substring(location.pathname.lastIndexOf('/') + 1));

    useEffect(() => {
        chatsApi.getChatInfo(chatId).then(
            (response) => {
                setChatName(response.data.groupName)
                setEmails(
                    response.data.userList.map(user => {
                        return user.userEmail;
                    })
                );
            }
        ).catch();
    }, [chatId, currentUserEmail]);

    const chatNameChange = (event) => {
        setChatName(event.target.value);
    };

    const handleSubmit = (values) => {
        setLoading(true);
        setMessage(null);
        ChatService.changeChat(chatName, emails, currentUserId, chatId).then(
            (response) => {
                setLoading(false);
                setMessage({text: 'Success!', type: 'success'});
                navigate("/chat")
            },
            (error) => {
                if (error.response) {
                    if (error.response.status === 401) {
                        setMessage({text: 'Invalid email or password', type: 'danger'});
                    } else if (error.response.status === 403) {
                        setMessage({text: 'Access denied!', type: 'danger'});
                    } else {
                        setMessage({text: error.response.data || error.response, type: 'danger'});
                    }
                } else {
                    const resMessage =
                        (error.response.data &&
                            error.response.data.message) ||
                        error.message ||
                        error.toString();
                    setMessage({text: resMessage, type: 'danger'});
                }
                setLoading(false);
            }
        )
    }

    return (
        <Container className="character-create-main-container">
            <AppHeader/>
            <Formik
                initialValues={{
                    name: '',
                    gender: '',
                }}
                validationSchema={Yup.object({
                    name: Yup.string()
                        .min(2, 'Name is too short - should be 2 chars minimum.')
                        .max(100, 'Name is too long - should be 100 chars maximum.'),
                })}
                onSubmit={(values, actions) => {
                    handleSubmit(values);
                    actions.setSubmitting(false);
                }}
            >
                <Form className="character-create-form">
                    <h1 className="character-create-header big-font">Настройки чата</h1>
                    <Container className="character-create-input-container">
                        <Row className="character-create-on-center">
                            <Col>
                                <ResizableLabel className="normal-font" htmlFor="name">
                                    Имя чата:
                                </ResizableLabel>
                            </Col>
                            <Col>
                                <Field className="character-create-form-input" name="name" type="text" onChange={chatNameChange} value={chatName}/>
                            </Col>
                        </Row>
                        <ErrorMessage component={WarningAlert} name="name"/>


                        <div style={{alignSelf: "center"}}>
                            <EmailList emails={emails} onRemoveUser={onRemoveUser} setEmails={setEmails}/>
                        </div>

                    </Container>

                    {message ? (
                        <Alert variant={message.type} className="error-message">{message.text}</Alert>
                    ) : null}

                    <ResizableRoundedButton className="dark-grey big-font" radius="10px" type="submit"
                                            disabled={loading}>
                        {loading ? (
                            <span className="spinner-border spinner-border-sm"/>
                        ) : "Подтвердить"}
                    </ResizableRoundedButton>
                    <span style={{margin: "5px"}} />
                    <ResizableRoundedButton className="light-grey normal-font" radius="10px" onClick={() => {
                        navigate("/chat")
                    }}>
                        Назад
                    </ResizableRoundedButton>
                </Form>
            </Formik>
        </Container>
    )
}

export default ChatSettings;