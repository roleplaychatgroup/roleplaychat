import icon from "../../../resources/chat-icon.png";
import info from "../../../resources/info-icon.png";
import settings from "../../../resources/settings-icon.png";
import "../styles/chat-header.css";

function ChatHeader(props) {

    return (
        <div className='chat-header'>
            <div className="chat-icon-box">
                <img className='chat-icon' src={icon} alt='chat title' />
            </div>
            <h1 className='chat-title'>{props.title}</h1>
            <div className='button-box'>
                {props.isAdmin &&
                    <img src={settings} onClick={props.onSettingsClick} className='button-chat' alt='chat settings button' />
                }
                <img src={info} onClick={props.onInfoClick} className='button-chat' alt='chat info button' />
            </div>
        </div>
    )
}

export default ChatHeader;
