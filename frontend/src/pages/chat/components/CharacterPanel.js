import male from "../../../resources/male-colored-icon.png";
import female from "../../../resources/female-colored-icon.png";
import nbinary from "../../../resources/non-binary-colored-icon.png";
import LogoutButton from "../../common/LogoutButton/LogoutButton";
import settings from "../../../resources/settings-icon.png";
import plus from "../../../resources/plus-icon.png";
import minus from "../../../resources/minus-icon.png";
import hp_icon from "../../../resources/hp-icon.png";
import createCharacter from "../../../resources/create-character-icon.png";
import "../styles/character-panel.css";
import { useEffect, useState } from "react";
import characterApi from "../services/CharacterService";


function CharacterPanel(props) {

  const [hp, setHp] = useState(props.character.hp);

  useEffect(() => {
    setHp(props.character.hp);
  }, [props.character]);

  const handleIncrementHp = () => {
    characterApi.incrementCharacterHP(props.character.id)
      .then().catch();
    setHp((oldHp) => setHp(oldHp + 1));
  }

  const handleDecrementHp = () => {
    characterApi.decrementCharacterHP(props.character.id)
      .then().catch();
    setHp((oldHp) => setHp(oldHp - 1));
  }

  let gender = 'не указано'
  let icon = nbinary;
  if (props.character.gender === 'male') {
    icon = male;
    gender = 'мужской';
  } else if (props.character.gender === 'female') {
    icon = female;
    gender = 'женский';
  }

  return (
    <div className='character-panel'>
      <div className='buttons-box'>
        <img onClick={props.openSettings} className='settings-button' src={settings} alt='logout-button' />
        <img onClick={props.openCreateCharacter} className='settings-button' src={createCharacter} alt='logout-button' />
        <LogoutButton />
      </div>
      {props.character &&
        <div className="character-info">
          <div className="character-info-title">Информация о персонаже</div>
          <img src={icon} className='character-avatar' alt='avatar' />
          <div>Имя: {props.character.name}</div>
          <div>Пол: {gender}</div>
          {props.character.id &&
            <div className="hp-panel">
              <img className="button-icon pointer" src={minus} onClick={handleDecrementHp} alt='substract health points' />
              <div>
                {hp}
                <img className="button-icon hp-icon" src={hp_icon} alt='health symbol' />
              </div>
              <img className="button-icon pointer" src={plus} onClick={handleIncrementHp} alt='add health points' />
            </div>
          }
          <textarea className="notes" placeholder="Введите заметки..." type="text"></textarea>
        </div>
      }
    </div>
  )
}

export default CharacterPanel;
