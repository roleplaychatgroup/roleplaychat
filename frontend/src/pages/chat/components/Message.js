import male from "../../../resources/male-icon.png";
import female from "../../../resources/female-icon.png";
import nbinary from "../../../resources/non-binary-icon.png";
import maleColored from "../../../resources/male-colored-icon.png";
import femaleColored from "../../../resources/female-colored-icon.png";
import nbinaryColored from "../../../resources/non-binary-colored-icon.png";
import "../styles/message.css";

function Message(props) {

    let icon = nbinary;
    if (props.message.gender === 'male') {
        icon = male;
    } else if (props.message.gender === 'female') {
        icon = female;
    }
    
    if (props.message.characterId && props.message.characterId !== -1) {
        icon = nbinaryColored
        if (props.message.characterGender === 'male') {
            icon = maleColored;
        } else if (props.message.characterGender === 'female') {
            icon = femaleColored;
        }
    }


    return (
        <div className={props.isOwn ? 'message-own' : 'message'}>
            {!props.isOwn && <img src={icon} className='avatar' alt='avatar'/>}
            <div className='message-text'>
                <p className='message-author'>
                    {props.message.characterId && props.message.characterId &&
                        <b>{props.message.characterName} ~ </b>
                    }
                    <b>{props.message.name}</b>
                </p>
                <p className='message-inner-text'>
                    {props.message.text}
                </p>
            </div>
            {props.isOwn && <img src={icon} className='avatar' alt='avatar'/>}
        </div>
    )
}

export default Message;
