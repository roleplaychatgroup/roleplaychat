import ChatInfo from "../index";
import {useCallback, useEffect, useState} from "react";
import {useNavigate, useParams} from "react-router-dom";
import ChatInfoService from "../services/ChatInfoService";
import AuthService from "../../authentication/services/auth.service";
import React from "react";

function ChatInfoContainer() {
    const navigate = useNavigate();
    const chatId = useParams().chatId;
    const [chatName, setChatName] = useState('');
    const [users, setUsers] = useState([]);
    const [isAdmin, setIsAdmin] = useState(false);
    const [message, setMessage] = useState(null);

    const getChatInfo = useCallback((chatId) => {
        setUsers([]);
        setMessage(null)
        ChatInfoService.getChatInfo(chatId).then(
            (response) => {
                setChatName(response.data.groupName);
                setUsers(response.data.userList);
                setIsAdmin(response.data.adminId === AuthService.getCurrentUser().id);
                setMessage(null);
            },
            (error) => {
                if (error.response) {
                    if (error.response.status === 401) {
                        setMessage({text: 'Invalid email or password', type: 'danger'});
                    } else if (error.response.status === 403) {
                        setMessage({text: 'Access denied!', type: 'danger'});
                    } else {
                        setMessage({text: error.response.data || error.response, type: 'danger'});
                    }
                } else {
                    const resMessage =
                        (error.response.data &&
                            error.response.data.message) ||
                        error.message ||
                        error.toString();
                    setMessage({text: resMessage, type: 'danger'});
                }
            }
        )
    }, []);

    useEffect(() => {
        getChatInfo(chatId);
    }, [chatId, getChatInfo])

    const editChat = () => {
        // TODO: redirect
    };

    const back = () => {
        navigate("/chat");
    }

    return (
        <ChatInfo chatName={chatName} users={users} isAdmin={isAdmin} editChat={editChat} back={back} message={message}/>
    )
}

export default ChatInfoContainer;