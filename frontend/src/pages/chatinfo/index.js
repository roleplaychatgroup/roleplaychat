import AppHeader from '../authentication/components/AppHeader';
import React from 'react';

import './index.css';
import '../authentication/styles/Fonts.css'

import UserList from "./components/UserList";
import settings from "../../resources/settings-icon.png";
import ResizableRoundedButton from "../authentication/components/ResizableRoundedButton";
import {Alert} from "react-bootstrap";

function ChatInfo(props) {
    return (
        <div className='chat-info-c1'>
            <AppHeader/>
            <div className='chat-info-c2'>
                <h1 className='chat-name'>{props.chatName}</h1>
                {
                    props.isAdmin &&
                    <img onClick={props.editChat} className='settings-button' src={settings} alt='logout-button' />
                }
            </div>
            <UserList users={props.users} isAdmin={props.isAdmin}/>
            <ResizableRoundedButton className="normal-font chat-info-back-btn" radius="10px" onClick={props.back}>
                Назад
            </ResizableRoundedButton>
            {props.message ? (
                <Alert variant={props.message.type} className="error-message">{props.message.text}</Alert>
            ) : null}
        </div>
    )
}

export default ChatInfo;