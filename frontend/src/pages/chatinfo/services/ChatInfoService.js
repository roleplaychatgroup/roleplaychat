import axios from "axios";
import authHeader from "../../authentication/services/auth-header";

const API_URL = "http://84.252.143.105:8081/api/";

const getCharacterList = (email) => {
    return axios.get(API_URL + `character/list/email?user_email=${email}`, {headers: authHeader()});
}

const getChatInfo = (id) => {
    return axios.get(API_URL + `groups/${id}`, {headers: authHeader()});
}

const ChatInfoService = {
    getCharacterList,
    getChatInfo,
}

export default ChatInfoService;