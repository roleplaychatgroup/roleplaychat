import CharacterList from "./CharacterList";

import '../styles/UserList.css';
import AuthService from "../../authentication/services/auth.service";

function UserList(props) {
    return (
        <div className='user-list-c1'>
            <h3 className='user-list-h1'>Участники</h3>
            <div className='user-list-c2'>
                {props.users.map((user, index) => {
                    const isCurUser = AuthService.getCurrentUser().email === user.userEmail;

                    return (
                        <div key={index} className='user-list-c3'>
                            <b className='user-email'>{user.userEmail}<br/></b>
                            {user.characters.length === 0 ? (
                                <span>Нет персонажей</span>
                            ) : (
                                <CharacterList characters={user.characters} isAdmin={props.isAdmin} isCurUser={isCurUser}/>
                            )}
                        </div>
                    )
                })}
            </div>
        </div>
    )
}

export default UserList;