import {useState} from "react";
import characterApi from "../../chat/services/CharacterService";
import minus from "../../../resources/minus-icon.png";
import hp_icon from "../../../resources/hp-icon.png";
import plus from "../../../resources/plus-icon.png";

import '../../chat/styles/character-panel.css';
import '../styles/CharacterListElement.css';
import classNames from "classnames";

function CharacterListElement(props) {
    const [hp, setHp] = useState(props.character.lifeCount);

    const handleIncrementHp = () => {
        characterApi.incrementCharacterHP(props.character.characterId)
            .then().catch();
        setHp((oldHp) => setHp(oldHp + 1));
    }

    const handleDecrementHp = () => {
        characterApi.decrementCharacterHP(props.character.characterId)
            .then().catch();
        setHp((oldHp) => setHp(oldHp - 1));
    }

    return (
        <div className='char-list-elem-c1'>
            <span>{props.character.characterName}</span>
            <div className="hp-panel">
                <img className={classNames("button-icon pointer", {'disabled-btn': !props.isAdmin && !props.isCurUser})}
                     src={minus}
                     onClick={(props.isAdmin || props.isCurUser) ? handleDecrementHp : () => {}}
                     alt='substract health points'/>
                <div className='char-list-elem-c2'>
                    {hp}
                    <img className="button-icon" src={hp_icon} alt='health symbol' />
                </div>
                <img className={classNames("button-icon pointer", {'disabled-btn': !props.isAdmin && !props.isCurUser})}
                     src={plus}
                     onClick={(props.isAdmin || props.isCurUser) ? handleIncrementHp : () => {}}
                     alt='add health points' />
            </div>
        </div>
    );
}

export default CharacterListElement;