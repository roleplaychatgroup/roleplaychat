import '../index.css';
import CharacterListElement from "./CharacterListElement";

import '../styles/CharacterList.css';

function CharacterList(props) {
    return (
        <div className='char-list-c1'>
            {props.characters.map((character) => {
                return (
                    <CharacterListElement key={character.characterId} character={character} isAdmin={props.isAdmin} isCurUser={props.isCurUser}/>
                )
            })}
        </div>
    )
}

export default CharacterList;