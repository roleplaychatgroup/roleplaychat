import AppHeader from "../authentication/components/AppHeader";
import {ErrorMessage, Field, Form, Formik} from "formik";
import * as Yup from "yup";
import {Alert, Col, Container, Row} from "react-bootstrap";
import ResizableLabel from "../authentication/components/ResizableLabel";
import ResizableRoundedButton from "../authentication/components/ResizableRoundedButton";
import {React, useState} from "react";

import "./index.css"
import "../authentication/styles/Fonts.css"
import ChatService from "../chat/services/ChatService";
import {useNavigate} from "react-router-dom";

function CreateCharacter() {
    const navigate = useNavigate();

    const RightMarginCol = (props) => <Col style={{marginRight: props.mg}}>{props.children}</Col>
    const NowrapRow = (props) => <Row style={{flexWrap: "nowrap"}}>{props.children}</Row>
    const WarningAlert = (props) => <Alert variant={"warning"} className="error-message">{props.children}</Alert>;
    const [message, setMessage] = useState(null);
    const [loading, setLoading] = useState(false);

    const handleCreate = (values) => {
        setLoading(true);
        setMessage(null);
        ChatService.createCharacter(values.name, values.gender).then(
            (response) => {
                setLoading(false);
                setMessage({text: 'Success!', type: 'success'});
                navigate("/chat")
            },
            (error) => {
                if (error.response) {
                    if (error.response.status === 401) {
                        setMessage({text: 'Invalid email or password', type: 'danger'});
                    } else if (error.response.status === 403) {
                        setMessage({text: 'Access denied!', type: 'danger'});
                    } else {
                        setMessage({text: error.response.data || error.response, type: 'danger'});
                    }
                } else {
                    const resMessage =
                        (error.response.data &&
                            error.response.data.message) ||
                        error.message ||
                        error.toString();
                    setMessage({text: resMessage, type: 'danger'});
                }
                setLoading(false);
            }
        )
    }

    return (
        <Container className="character-create-main-container">
            <AppHeader/>
            <Formik
                initialValues={{
                    name: '',
                    gender: '',
                }}
                validationSchema={Yup.object({
                    name: Yup.string()
                        .min(3, 'Name is too short - should be 3 chars minimum.')
                        .max(30, 'Name is too long - should be 30 chars maximum.'),
                    gender: Yup.string()
                        .required('Required'),
                })}
                onSubmit={(values, actions) => {
                    handleCreate(values);
                    actions.setSubmitting(false);
                }}>
                <Form className="character-create-form">
                    <h1 className="character-create-header big-font">Создание персонажа</h1>
                    <Container className="character-create-input-container">
                        <Row className="character-create-on-center">
                            <Col>
                                <ResizableLabel className="normal-font" htmlFor="name">
                                    Имя персонажа:
                                </ResizableLabel>
                            </Col>
                            <Col>
                                <Field className="character-create-form-input" name="name" type="text"/>
                            </Col>
                        </Row>
                        <ErrorMessage component={WarningAlert} name="name"/>

                        <Container className="gender-container">
                            <NowrapRow>
                                <Col>
                                    <ResizableLabel className="gender-label normal-font" width="44px"
                                                    height="24px">
                                        Пол:
                                    </ResizableLabel>
                                </Col>
                                <Col>
                                    <Field name="gender" type="radio" value="male"/>
                                </Col>
                                <RightMarginCol mg="185px">
                                    <ResizableLabel className="male-label normal-font" width="76px"
                                                    height="24px">
                                        Мужской
                                    </ResizableLabel>
                                </RightMarginCol>
                            </NowrapRow>

                            <NowrapRow>
                                <Col>
                                    <Field name="gender" type="radio" value="female"/>
                                </Col>
                                <RightMarginCol mg="185px">
                                    <ResizableLabel className="female-label normal-font" width="76px"
                                                    height="24px">
                                        Женский
                                    </ResizableLabel>
                                </RightMarginCol>
                            </NowrapRow>

                            <NowrapRow>
                                <Col>
                                    <Field name="gender" type="radio" value="not_stated"/>
                                </Col>
                                <RightMarginCol mg="153px">
                                    <ResizableLabel className="not-stated-label normal-font" width="108px"
                                                    height="24px">
                                        Не указано
                                    </ResizableLabel>
                                </RightMarginCol>
                            </NowrapRow>
                            <ErrorMessage component={WarningAlert} name="gender"/>
                        </Container>
                    </Container>

                    {message ? (
                        <Alert variant={message.type} className="error-message">{message.text}</Alert>
                    ) : null}

                    <ResizableRoundedButton className="dark-grey big-font" radius="10px" type="submit"
                                            disabled={loading}>
                        {loading ? (
                            <span className="spinner-border spinner-border-sm"/>
                        ) : "Создать"}
                    </ResizableRoundedButton>
                    <ResizableRoundedButton className="light-grey normal-font" radius="10px" onClick={() => {
                        navigate("/chat")
                    }}>
                        Назад
                    </ResizableRoundedButton>
                </Form>
            </Formik>
        </Container>
    )
}

export default CreateCharacter;