const chatBattle = {
    title: 'Battle with orks',
    numberOfPlayers: 4,
    players: [],
}

export function getChatBattle() {
    return chatBattle;
}

export function getChatOrganisation() {
    const chat = {
        title: 'Organisation',
        numberOfPlayers: 2,
        players: [],
    }

    return chat;
}

export function getChatTown() {
    const chat = {
        title: 'Neverwinter',
        numberOfPlayers: 3,
        players: [],
    }

    return chat;
}
