export const successRegister = (username, email, password) => {
    return new Promise(function(resolve, reject) {
        resolve({
            'data' : {
                'message' : 'success'
            }
        });
    });
}

export const failRegister = (username, email, password) => {
    return new Promise(function(resolve, reject) {
        reject({
            'message' : 'fail'
        });
    });
}

export const successLogin = (username, password) => {
    return new Promise(function(resolve, reject) {
        resolve({});
    });
}

export const failLogin = (username, password) => {
    return new Promise(function(resolve, reject) {
        reject({
            'message' : 'fail'
        });
    });
}