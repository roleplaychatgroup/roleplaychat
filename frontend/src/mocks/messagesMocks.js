const messages = [
    {
        senderCharacter: 'Rolaf',
        senderName: 'Petya Vaflev',
        text: 'Ha-ha i got you you nasty ork!',
        sentTime: '20:30',
        isOwn: true,
    },
    {
        senderCharacter: 'Ork',
        senderName: 'Lena Lozkina',
        text: 'Ouch! It hurts!',
        sentTime: '20:31',
        isOwn: false,
    }
]

export function sendMessageBattle(message) {
    messages.push({
        senderCharacter: message.senderCharacter,
        senderName: message.senderName,
        text: message.text,
        sentTime: message.sentTime,
        isOwn: message.isOwn,
    });
}

export function getMessagesBattle() {

    return messages;
}